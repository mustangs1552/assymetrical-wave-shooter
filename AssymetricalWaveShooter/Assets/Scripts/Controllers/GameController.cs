﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
    /// <summary>
    /// This controller controls the overall game.
    /// It is responsible for initial setup, closing, and managing main values.
    /// It manages other controllers that run the actual game.
    /// </summary>
    public class GameController : MonoBehaviour
    {
        #region Variables
        public static GameController SINGLETON = null;

        [Tooltip("The amount of shooter players.")]
        [SerializeField] [Range(1, 4)] private int playerCount = 1;
        [Tooltip("The prefab for the shooter players.")]
        [SerializeField] private GameObject shooterPlayerPrefab = null;
        [Tooltip("The spawn area for the shooter players.")]
        [SerializeField] private Collider shooterPlayerSpawnArea = null;

        private List<GameObject> spawnedShooterPlayers = new List<GameObject>();
        private int roundNum = 1;
        #endregion

        #region Functions
        /// <summary>
        /// Find and return the spawnpoints on the player spawn area.
        /// </summary>
        /// <param name="pointsRequested">Amount of spoints requested.</param>
        /// <returns>The requested spawnpoints.</returns>
        private List<Vector3> FindSpawnPoints(int pointsRequested)
        {
            List<Vector3> points = new List<Vector3>();

            if (pointsRequested > 0 && pointsRequested <= 4)
            {
                switch (pointsRequested)
                {
                    case 1:
                        points.Add(shooterPlayerSpawnArea.bounds.center);
                        break;
                    case 2:
                        points.Add(new Vector3(shooterPlayerSpawnArea.bounds.min.x, shooterPlayerSpawnArea.bounds.center.y, shooterPlayerSpawnArea.bounds.center.z));
                        points.Add(new Vector3(shooterPlayerSpawnArea.bounds.max.x, shooterPlayerSpawnArea.bounds.center.y, shooterPlayerSpawnArea.bounds.center.z));
                        break;
                    case 3:
                        points.Add(new Vector3(shooterPlayerSpawnArea.bounds.min.x, shooterPlayerSpawnArea.bounds.center.y, shooterPlayerSpawnArea.bounds.min.z));
                        points.Add(new Vector3(shooterPlayerSpawnArea.bounds.max.x, shooterPlayerSpawnArea.bounds.center.y, shooterPlayerSpawnArea.bounds.min.z));
                        points.Add(new Vector3(shooterPlayerSpawnArea.bounds.center.x, shooterPlayerSpawnArea.bounds.center.y, shooterPlayerSpawnArea.bounds.max.z));
                        break;
                    case 4:
                        points.Add(new Vector3(shooterPlayerSpawnArea.bounds.min.x, shooterPlayerSpawnArea.bounds.center.y, shooterPlayerSpawnArea.bounds.min.z));
                        points.Add(new Vector3(shooterPlayerSpawnArea.bounds.max.x, shooterPlayerSpawnArea.bounds.center.y, shooterPlayerSpawnArea.bounds.min.z));
                        points.Add(new Vector3(shooterPlayerSpawnArea.bounds.min.x, shooterPlayerSpawnArea.bounds.center.y, shooterPlayerSpawnArea.bounds.max.z));
                        points.Add(new Vector3(shooterPlayerSpawnArea.bounds.max.x, shooterPlayerSpawnArea.bounds.center.y, shooterPlayerSpawnArea.bounds.max.z));
                        break;
                }
            }

            return points;
        }
        /// <summary>
        /// Clear the list of shooter players to be re-used again.
        /// </summary>
        private void ClearShooterPlayers()
        {
            foreach (GameObject spawnedPlayer in spawnedShooterPlayers) ObjectPoolManagerBase.SINGLETON.Destroy(spawnedPlayer);
        }
        /// <summary>
        /// Spawn a shooter player at the given position.
        /// </summary>
        /// <param name="pos">The position to spawn the player.</param>
        private void SpawnPlayer(Vector3 pos)
        {
            spawnedShooterPlayers.Add(ObjectPoolManagerBase.SINGLETON.Instantiate(shooterPlayerPrefab, pos, shooterPlayerSpawnArea.transform.rotation, true, false));
        }
        /// <summary>
        /// Setup and spawn in the shooter players.
        /// </summary>
        /// <param name="numOfPlayers">Number of players to setup.</param>
        private void SetupShooterPlayers(int numOfPlayers)
        {
            if(shooterPlayerPrefab != null)
            {
                List<Vector3> spawnpoints = FindSpawnPoints(numOfPlayers);
                if(spawnpoints.Count > 0)
                {
                    ClearShooterPlayers();
                    foreach (Vector3 spawnpoint in spawnpoints) SpawnPlayer(spawnpoint);
                }
            }
        }

        /// <summary>
        /// Revive any players that have died.
        /// </summary>
        private void RevivePlayers()
        {
            IHasHealth currPlayerHealth = null;
            foreach (GameObject player in spawnedShooterPlayers)
            {
                currPlayerHealth = player.GetComponent<IHasHealth>();
                if (currPlayerHealth != null) currPlayerHealth.Revive(true);
                else Debug.LogError(player.name + " does not have an 'IHasHealth' component!");
            }
        }

        /// <summary>
        /// A round has finished.
        /// Handle tasks between rounds and start a new one.
        /// </summary>
        /// <param name="wasRoundSuccess">Was the finished round successful?</param>
        private void RoundFinished(bool wasRoundSuccess)
        {
            RevivePlayers();

            if (wasRoundSuccess)
            {
                roundNum++;
                RoundControllerBase.SINGLETON.StartRound(playerCount, roundNum);
            }
            else
            {
                roundNum = 1;
                RoundControllerBase.SINGLETON.StartRound(playerCount, roundNum);
            }
        }

        /// <summary>
        /// Initial setup.
        /// </summary>
        private void Setup()
        {
            if (GameController.SINGLETON == null) SINGLETON = this;
            else
            {
                Debug.LogError("More than one '" + GetType() + "' singletons detected!");
                Destroy(this);
            }

            if (shooterPlayerPrefab == null) Debug.LogError("No 'shooterPlayerPrefab' assigned!");
            if (shooterPlayerSpawnArea == null) Debug.LogError("No 'shooterPlayerSpawnArea' assigned!");

            UnityAnalyticsController.GameModeStart("1vsAI", playerCount, false);
        }
        /// <summary>
        /// Setup ran after normal Setup().
        /// </summary>
        private void PostSetup()
        {
            RoundControllerBase.SINGLETON.OnRoundEnd += RoundFinished;
            RoundControllerBase.SINGLETON.StartRound(playerCount, roundNum);

            SetupShooterPlayers(playerCount);
        }
        #endregion

        #region Unity Functions
        private void Awake()
        {
            Setup();
        }
        private void Start()
        {
            PostSetup();
        }

        private void OnApplicationQuit()
        {
            UnityAnalyticsController.GameModeEnded(true);
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Utilities;
using Assets.Scripts.Controllers;
public enum RoundState
{
    Idle,
    Preparing,
    Starting,
    Playing,
    PostPlay,
}

/// <summary>
/// This controller controls a single round from start to finish.
/// It does not start a new round and instead waits for another object to start a new round.
/// </summary>
public class RoundController : RoundControllerBase
{
    private enum SetupMapState
    {
        StartingSpawnSpheres,
        SpawnSpheres,
        StartingPillars,
        Pillars,
    }

    #region Variables
    #region Public
    [Tooltip("The wave master.")]
    [SerializeField] private GameObject waveMasterObj = null;
    [Tooltip("The pillar manager controlling the map.")]
    [SerializeField] private GameObject pillarManager = null;

    [Tooltip("The available enemy options with thier costs for the wave master.")]
    [SerializeField] private List<PublicEnemyOption> enemyOptions = new List<PublicEnemyOption>();
    [Tooltip("The available heightmaps with POIs for the wave master.")]
    [SerializeField] private List<PMHeightmap> heightmapOptions = new List<PMHeightmap>();

    [Tooltip("The door letting the shooter players into the main area.")]
    [SerializeField] private Door door = null;
    [Tooltip("The prefab that is placed on the map's starting area to detect when the players have reached it.")]
    [SerializeField] private ShooterPlayerStartingArea startingAreaTriggerPrefab = null;
    [Tooltip("The prefab that represents the spawn spheres.")]
    [SerializeField] private GameObject spawnSpherePrefab = null;
    [Tooltip("The countdown before the playing state begins after the players are moved into the starting areas.")]
    [SerializeField] private float prePlayingCountdown = 5.9f;

    [Tooltip("The trigger object in the prepare area that monitors thier presence there.")]
    [SerializeField] private GameObject prepareAreaTrigger = null;
    #endregion

    #region Properties
    /// <summary>
    /// Is the wave master ready?
    /// </summary>
    public override bool WaveMasterReady { get; set; } = false;
    
    /// <summary>
    /// Is the count of players reached for the current action.
    /// </summary>
    private bool IsPlayerCounterReached
    {
        get
        {
            return WaveMasterReady && currShooterPlayerCount >= shooterPlayerCount;
        }
    }

    public override Action<bool> OnRoundEnd { get; set; }
    public override Action OnPlayersReady { get; set; }

    /// <summary>
    /// The players gained currency.
    /// </summary>
    public override int EarnedCurrency
    {
        get
        {
            return CurrCurrency;
        }
    }
    private int CurrCurrency
    {
        get
        {
            return currCurrency;
        }
        set
        {
            currCurrency = (value >= 0) ? value : 0;
        }
    }
    #endregion

    #region Private
    private IWaveMaster waveMaster = null;
    private IRoundControllerUI ui = null;
    private IPillarManager pManager = null;

    private List<EnemyOption> privateEnemyOptions = new List<EnemyOption>();
    private int shooterPlayerCount = 0;
    private int currShooterPlayerCount = 0;
    private int currRoundNum = 1;
    private int currRoundPoints = 0;
    PMHeightmap currHeightmap = null;

    private List<ShooterPlayerStartingArea> startingAreasSpawned = new List<ShooterPlayerStartingArea>();
    private List<ISpawnSphere> spawnedSpawnSpheres = new List<ISpawnSphere>();
    private SetupMapState setupMapState = SetupMapState.StartingSpawnSpheres;
    private ITimer timer = null;

    private IObjectPresenceMonitor prepareAreaObjMonitor = null;

    private bool wasPrevRoundSuccess = false;

    private int currCurrency = 0;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Start the round in the preparing state from idle.
    /// </summary>
    /// <param name="playerCount">Number of shooter players.</param>
    /// <param name="startRoundNum">Round to start at. Round 1 is default.</param>
    /// <returns>True if successfully started round.</returns>
    public override bool StartRound(int playerCount, int startRoundNum = 1)
    {
        if (state == RoundState.Idle)
        {
            shooterPlayerCount = (playerCount > 0 && playerCount <= 4) ? playerCount : 1;
            if (startRoundNum > 0) currRoundNum = startRoundNum;
            StartPrepareState();
            return true;
        }

        return false;
    }

    /// <summary>
    /// Ready up one shooter player.
    /// </summary>
    public override void ShooterPlayerReady()
    {
        if (state == RoundState.Preparing)
        {
            currShooterPlayerCount++;
            ui.UpdatePlayerCounts(shooterPlayerCount, currShooterPlayerCount);
        }
    }
    /// <summary>
    /// Un-ready one shooter player.
    /// </summary>
    public override void ShooterPlayerUnReady()
    {
        if (currShooterPlayerCount > 0 && state == RoundState.Preparing)
        {
            currShooterPlayerCount--;
            ui.UpdatePlayerCounts(shooterPlayerCount, currShooterPlayerCount);
        }
    }

    /// <summary>
    /// A shooter player has reached the starting area and is awaiting the map to be set up and start.
    /// </summary>
    public override void ShooterPlayerInPosition()
    {
        if (state == RoundState.Starting) currShooterPlayerCount++;
    }

    /// <summary>
    /// Set the heightmap for the next round.
    /// Heightmap must have a POI map with "StartArea" and "EnemySpawn" POIs.
    /// </summary>
    /// <param name="heightmap">Heightmap to set.</param>
    /// <returns>True if successful.</returns>
    public override bool SetNextHeightmap(PMHeightmap heightmap)
    {
        if (heightmap != null && heightmap.Heightmap != null && heightmap.POIMap != null && heightmap.POIs != null && heightmap.POIs.Count >= 2)
        {
            bool hasStart = false;
            bool hasEnemySpawn = false;
            foreach (POIPair poi in heightmap.POIs)
            {
                if (poi.POIName == "StartArea") hasStart = true;
                else if (poi.POIName == "EnemySpawnpoint") hasEnemySpawn = true;

                if (hasStart && hasEnemySpawn) break;
            }

            if (hasStart && hasEnemySpawn)
            {
                pManager.ApplyHeightmap(heightmap);
                currHeightmap = heightmap;
                Dictionary<string, List<Vector3>> poiLocations = pManager.POILocations;
                if (poiLocations.ContainsKey("StartArea") && poiLocations.ContainsKey("EnemySpawnpoint"))
                {
                    UpdatePrepareMonitorHeightmap(heightmap);
                    return true;
                }
                else if (!poiLocations.ContainsKey("StartArea")) Debug.LogError("No 'StartArea' POI found!");
                else if (!poiLocations.ContainsKey("EnemySpawnpoint")) Debug.LogError("No 'EnemySpawnpoint' POI found!");
            }
            else
            {
                if (!hasStart) Debug.LogError("Provided heightmap does not have 'StartArea' POI!");
                if (!hasEnemySpawn) Debug.LogError("Provided heightmap does not have 'EnemySpawnpoint' POI!");
            }
        }
        else Debug.LogError("Invalid heightmap given!");

        return false;
    }

    /// <summary>
    /// A shooter player was defeated.
    /// </summary>
    public override void ShooterPlayerDefeated()
    {
        if (state == RoundState.Playing) currShooterPlayerCount++;
    }

    /// <summary>
    /// Remove the given amount of currency from the players' currency only if they have enough.
    /// </summary>
    /// <param name="amount">The amount to be removed.</param>
    /// <returns>True if successful.</returns>
    public override bool CheckRemoveCurrency(int amount)
    {
        if(amount <= CurrCurrency)
        {
            CurrCurrency -= amount;
            ui.UpdateEarnedCurrency(CurrCurrency);
            return true;
        }

        return false;
    }
    #endregion

    #region Private
    /// <summary>
    /// Start the preparing state.
    /// </summary>
    private void StartPrepareState()
    {
        state = RoundState.Preparing;

        currShooterPlayerCount = 0;

        privateEnemyOptions = new List<EnemyOption>();
        foreach (PublicEnemyOption publicEO in enemyOptions) privateEnemyOptions.Add(new EnemyOption(publicEO));

        currRoundPoints = CalcNextRoundPoints(currRoundNum);

        UpdatePrepareMonitorPreRound(currRoundPoints);
        ui.UpdatePlayerCounts(shooterPlayerCount);
        waveMaster.PrepareState(currRoundPoints, privateEnemyOptions, heightmapOptions);
    }
    /// <summary>
    /// Start the starting state.
    /// </summary>
    private void StartStartingState()
    {
        state = RoundState.Starting;

        if (OnPlayersReady != null) OnPlayersReady();

        SpawnStartingAreaTriggers();

        currShooterPlayerCount = 0;
        WaveMasterReady = true;
        ui.UpdatePrePlayingCountdown(0);
        door.Open();

        setupMapState = SetupMapState.StartingSpawnSpheres;

        waveMaster.StartingState();

        UnityAnalyticsController.RoundStart(currRoundNum, currHeightmap.Heightmap.name, false);
    }
    /// <summary>
    /// Start the playing state.
    /// </summary>
    private void StartPlayingState()
    {
        state = RoundState.Playing;

        FinishStartingState();

        currShooterPlayerCount = 0;
        WaveMasterReady = true;

        timer.StartCountingUp();

        foreach (IQueueSpawner spawnSphere in spawnedSpawnSpheres) spawnSphere.SpawningEnabled = true;
        waveMaster.PlayingState();
    }
    /// <summary>
    /// Start the post play state.
    /// </summary>
    private void StartPostPlayState()
    {
        state = RoundState.PostPlay;

        pManager.LowerPillars();
        foreach (ISpawnSphere spawnSphere in spawnedSpawnSpheres) spawnSphere.DespawnSpawnSphere();
        door.Open();

        UpdatePrepareMonitorRoundEnd((int)timer.CancelCounting(), waveMaster.PostPlayState());
    }
    /// <summary>
    /// Clean up and finish the current round.
    /// </summary>
    private void FinishRound()
    {
        state = RoundState.Idle;

        foreach (ISpawnSphere spawnSphere in spawnedSpawnSpheres) ObjectPoolManagerBase.SINGLETON.Destroy(((MonoBehaviour)spawnSphere).gameObject);

        if (wasPrevRoundSuccess) AddCurencyWinnings();
        else
        {
            CurrCurrency = 0;
            ui.UpdateEarnedCurrency(CurrCurrency);
        }

        UnityAnalyticsController.RoundOver(wasPrevRoundSuccess);
        if (OnRoundEnd != null) OnRoundEnd(wasPrevRoundSuccess);
    }

    /// <summary>
    /// Primary loop for performing actions overtime for states.
    /// </summary>
    private void StateLoop()
    {
        switch (state)
        {
            case RoundState.Preparing:
                if (IsPlayerCounterReached) StartStartingState();
                break;
            case RoundState.Starting:
                if (IsPlayerCounterReached) SetupMap();
                break;
            case RoundState.Playing:
                ui.UpdatePlayingTime((int)timer.TimePassed);
                if (IsPlayerCounterReached)
                {
                    wasPrevRoundSuccess = false;
                    ui.UpdateRoundSuccess(wasPrevRoundSuccess);
                    StartPostPlayState();
                }
                else if (CheckEnemyCounts())
                {
                    wasPrevRoundSuccess = true;
                    ui.UpdateRoundSuccess(wasPrevRoundSuccess);
                    StartPostPlayState();
                }
                break;
            case RoundState.PostPlay:
                if (door.IsClosed) FinishRound();
                else
                {
                    if (prepareAreaObjMonitor.ObjCount >= shooterPlayerCount) door.Close();
                    else if (prepareAreaObjMonitor.ObjCount < shooterPlayerCount) door.Open();
                }
                break;
        }
    }

    /// <summary>
    /// Check if all the enemies have been spawned and defeated.
    /// </summary>
    /// <returns>True if none are left.</returns>
    private bool CheckEnemyCounts()
    {
        bool enemiesActive = false;
        bool enemiesQueued = false;
        if (state == RoundState.Playing)
        {
            foreach (EnemyOption enemyOption in privateEnemyOptions)
            {
                if (ObjectPoolManagerBase.SINGLETON.GetActiveObjects(enemyOption.Enemy.gameObject).Count > 0)
                {
                    enemiesActive = true;
                    break;
                }
            }

            foreach (IQueueSpawner spawner in spawnedSpawnSpheres)
            {
                if (spawner.SpawnQueue.Count > 0)
                {
                    enemiesQueued = true;
                    break;
                }
            }
        }

        return !enemiesActive && !enemiesQueued && !waveMaster.EnemiesRemaining;
    }

    /// <summary>
    /// Finish the starting state and clean up the objects created by it.
    /// </summary>
    private void FinishStartingState()
    {
        foreach (ShooterPlayerStartingArea startingArea in startingAreasSpawned)
        {
            startingArea.ResumePlayers();
            ObjectPoolManagerBase.SINGLETON.Destroy(startingArea.gameObject);
        }
    }
    /// <summary>
    /// Spawn in the spawn spheres and start thier spawning animations.
    /// </summary>
    private void SpawnSpheres()
    {
        foreach (Pillar pillar in pManager.POIPillars["EnemySpawnpoint"])
        {
            GameObject spawnedSpawnSphere = ObjectPoolManagerBase.SINGLETON.Instantiate(spawnSpherePrefab, pillar.TopSurface, Quaternion.identity);
            if (spawnedSpawnSphere != null)
            {
                ISpawnSphere spawnedISpawnSphere = spawnedSpawnSphere.GetComponent<ISpawnSphere>();
                if (spawnedISpawnSphere != null)
                {
                    spawnedSpawnSpheres.Add(spawnedISpawnSphere);
                    spawnedISpawnSphere.SpawnSpawnSphere(pillar);
                }
                else ObjectPoolManagerBase.SINGLETON.Destroy(spawnedSpawnSphere);
            }
        }
    }
    /// <summary>
    /// Check to see if the spawn spheres are finished spawning.
    /// </summary>
    /// <returns>True if they are all done.</returns>
    private bool CheckSpheres()
    {
        foreach (ISpawnSphere sphere in spawnedSpawnSpheres)
        {
            if (sphere.IsMoving) return false;
        }

        return true;
    }
    /// <summary>
    /// The state loop for setting up the map.
    /// This includes spawning the spawn spheres and raising the pillars of the map.
    /// </summary>
    private void SetupMap()
    {
        switch (setupMapState)
        {
            case SetupMapState.StartingSpawnSpheres:
                door.Close();
                SpawnSpheres();
                setupMapState = SetupMapState.SpawnSpheres;
                break;
            case SetupMapState.SpawnSpheres:
                if (CheckSpheres()) setupMapState = SetupMapState.StartingPillars;
                break;
            case SetupMapState.StartingPillars:
                pManager.RaisePillars();
                setupMapState = SetupMapState.Pillars;
                break;
            case SetupMapState.Pillars:
                if (!pManager.ArePillarsMoving)
                {
                    timer.StartCountdown(prePlayingCountdown);
                    ui.UpdatePrePlayingCountdown((int)timer.TimeRemaining);
                }
                break;
        }
    }

    /// <summary>
    /// Calculate the total points for the next round given.
    /// </summary>
    /// <param name="roundNum">The next round to calculate for.</param>
    /// <returns>The total points calculated.</returns>
    private int CalcNextRoundPoints(int roundNum)
    {
        return roundNum * 5;
    }

    /// <summary>
    /// Give the UI the updated values to be updated.
    /// </summary>
    /// <param name="totalRoundPoints">Total points for the next round.</param>
    private void UpdatePrepareMonitorPreRound(int totalRoundPoints)
    {
        ui.UpdateEnemyOptions(privateEnemyOptions);
        ui.UpdateNextRoundNum(currRoundNum);
        ui.UpdateNextRoundPoints(totalRoundPoints);
    }
    /// <summary>
    /// Give the UI the updated heightmap with POIs for the next round to be updated.
    /// </summary>
    /// <param name="heightmap">The heightmap with POIs for the next round.</param>
    private void UpdatePrepareMonitorHeightmap(PMHeightmap heightmap)
    {
        ui.UpdateNextRoundHeightmap(heightmap);
    }
    /// <summary>
    /// Update the UI on the prepare monitor with info from the round just finished.
    /// </summary>
    /// <param name="roundSeconds">Time the round lasted in seconds.</param>
    /// <param name="endRoundInfo">End round info with enemy counts and chosen heightmap.</param>
    private void UpdatePrepareMonitorRoundEnd(int roundSeconds, EndRoundWaveMasterInfo endRoundInfo)
    {
        ui.UpdatePrevRoundNum(currRoundNum);
        ui.UpdatePrevRoundTime(roundSeconds);

        int selectedEnemiesTotalPoints = 0;
        foreach (EnemyCount enemyCount in endRoundInfo.SelectedEnemyCounts)
        {
            foreach (EnemyOption enemyOption in endRoundInfo.SelectedEnemyOptions)
            {
                if (GameObjectUtilities.GetActualName(enemyCount.Enemy) == GameObjectUtilities.GetActualName(enemyOption.Enemy.gameObject))
                {
                    selectedEnemiesTotalPoints += enemyOption.PointCost * enemyCount.Count;
                }
            }
        }
        ui.UpdatePrevRoundPoints(currRoundPoints, selectedEnemiesTotalPoints);
        ui.UpdatePrevRoundEnemies(endRoundInfo.SelectedEnemyOptions, endRoundInfo.SelectedEnemyCounts);

        ui.UpdatePrevRoundHeightmap(endRoundInfo.SelectedHeightmap);
    }

    /// <summary>
    /// Spawn the trigger objects on the starting areas that will detect when the player has reached a starting spot.
    /// </summary>
    private void SpawnStartingAreaTriggers()
    {
        List<Vector3> startingSpots = pManager.POILocations["StartArea"];
        foreach (Vector3 pos in startingSpots)
        {
            GameObject spawned = ObjectPoolManagerBase.SINGLETON.Instantiate(startingAreaTriggerPrefab.gameObject, pos, Quaternion.identity);
            if (spawned != null)
            {
                ShooterPlayerStartingArea spawnedStartingArea = spawned.GetComponent<ShooterPlayerStartingArea>();
                if (spawnedStartingArea != null) startingAreasSpawned.Add(spawnedStartingArea);
            }
        }
    }

    /// <summary>
    /// Check if the given object is one to be monitored by the prepare area trigger.
    /// </summary>
    /// <param name="obj">object to check.</param>
    /// <returns>True if it should be monitored.</returns>
    private bool PrepareAreaMonitoredObj(GameObject obj)
    {
        if (obj.GetComponentInChildren<IShooterControls>() != null) return true;

        return false;
    }

    /// <summary>
    /// Add the players' winnings to their currency count.
    /// </summary>
    private void AddCurencyWinnings()
    {
        CurrCurrency += currRoundPoints;
        ui.UpdateEarnedCurrency(CurrCurrency);
    }

    /// <summary>
    /// initial setup.
    /// </summary>
    private void Setup()
    {
        if (waveMasterObj != null)
        {
            waveMaster = waveMasterObj.GetComponent<IWaveMaster>();
            if (waveMaster == null) Debug.LogError("No 'IWaveMaster' found on 'waveMasterObj'!");
        }
        else Debug.LogError("No 'waveMasterObj' provided!");

        ui = GetComponent<IRoundControllerUI>();
        if (ui == null) Debug.LogError("No 'IRoundControllerUI' found on this object!");

        if (pillarManager == null) Debug.LogError("No 'PillarManager' found on this object!");
        else
        {
            pManager = pillarManager.GetComponent<IPillarManager>();
            if(pManager == null) Debug.LogError("No 'IPillarManager' found on the 'PillarManager' object!");
        }
        if (enemyOptions == null || enemyOptions.Count == 0) Debug.LogError("No 'enemyOptions' provided!");
        if (heightmapOptions == null || heightmapOptions.Count == 0) Debug.LogError("No 'heightmapOptions' provided!");
        if (door == null) Debug.LogError("No 'Door' set!");
        if (spawnSpherePrefab == null) Debug.LogError("No 'spawnSpherePrefab' set!");

        prepareAreaObjMonitor = prepareAreaTrigger.GetComponent<IObjectPresenceMonitor>();
        if (prepareAreaObjMonitor == null) Debug.LogError("No 'IObjectPresenceMonitor' found on 'prepareAreaTrigger'!");
        else prepareAreaObjMonitor.MonitoredObj = PrepareAreaMonitoredObj;

        timer = gameObject.AddComponent<Timer>();
        timer.TimeupCallback = StartPlayingState;

        ui.UpdateEarnedCurrency(CurrCurrency);
    }
    #endregion
    #endregion

    #region Unity Functions
    private new void Awake()
    {
        base.Awake();
        Setup();
    }
    private void Update()
    {
        StateLoop();
    }
    #endregion
}

/// <summary>
/// An enemy option with its values public.
/// </summary>
[Serializable]
public class PublicEnemyOption
{
    [Tooltip("The enemy prefab.")]
    public AIBrain enemy = null;
    [Tooltip("The cost of the enemy.")]
    public int pointCost = 1;
}
/// <summary>
/// An enemy option with its values private but, accessable through properties.
/// </summary>
public class EnemyOption
{
    private AIBrain enemy = null;
    private int pointCost = 1;

    public AIBrain Enemy
    {
        get
        {
            return enemy;
        }
    }
    public int PointCost
    {
        get
        {
            return pointCost;
        }
    }

    public EnemyOption()
    {

    }
    public EnemyOption(PublicEnemyOption publicEnemyOption)
    {
        enemy = publicEnemyOption.enemy;
        pointCost = publicEnemyOption.pointCost;
    }
}

/// <summary>
/// An enemy with a count of that enemy.
/// </summary>
public class EnemyCount
{
    public GameObject Enemy { get; set; }
    public int Count { get; set; }

    public EnemyCount(GameObject enemy, int count)
    {
        Enemy = enemy;
        Count = count;
    }
}

/// <summary>
/// The info needed at the end of each round.
/// </summary>
public class EndRoundWaveMasterInfo
{
    public List<EnemyOption> SelectedEnemyOptions { get; set; }
    public List<EnemyCount> SelectedEnemyCounts { get; set; }
    public PMHeightmap SelectedHeightmap { get; set; }

    public EndRoundWaveMasterInfo()
    {

    }
}
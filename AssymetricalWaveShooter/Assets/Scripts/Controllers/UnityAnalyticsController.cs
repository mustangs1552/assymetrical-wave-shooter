﻿using System.Collections.Generic;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Controllers
{
    /// <summary>
    /// This controller is where all the calls to Unity Analytics are made.
    /// </summary>
    public static class UnityAnalyticsController
    {
        private static bool generalValuesSet = false;
        private static string currGameMode = "";
        private static int currShooterPlayerCount = -1;
        private static bool isHumanWaveMaster = false;
        private static string currActiveScene = "";

        private static int currRoundNum = -1;
        private static string currMapName = "";
        private static bool currPlayerChoseMap = false;

        /// <summary>
        /// A game mode started.
        /// </summary>
        /// <param name="gameMode">The name of the mode.</param>
        /// <param name="shooterPlayerCount">The number of shooter players.</param>
        /// <param name="humanWaveMaster">Is there a human wave master?</param>
        public static void GameModeStart(string gameMode, int shooterPlayerCount, bool humanWaveMaster)
        {
            SetGeneralValues(gameMode, shooterPlayerCount, humanWaveMaster, SceneManager.GetActiveScene().name);
            AnalyticsEvent.GameStart(new Dictionary<string, object> { { "Scene", currActiveScene }, { "GameMode", currGameMode }, { "PlayerCount", currShooterPlayerCount }, { "IsHumanWaveMaster", isHumanWaveMaster } });
        }
        /// <summary>
        /// The current game mode was ended.
        /// </summary>
        /// <param name="quitGame">Was the application quit?</param>
        public static void GameModeEnded(bool quitGame)
        {
            if (generalValuesSet)
            {
                if (!quitGame) AnalyticsEvent.GameOver(currActiveScene, new Dictionary<string, object> { { "Scene", currActiveScene }, { "GameMode", currGameMode }, { "PlayerCount", currShooterPlayerCount }, { "IsHumanWaveMaster", isHumanWaveMaster } });
                else AnalyticsEvent.LevelQuit(currActiveScene, new Dictionary<string, object> { { "Scene", currActiveScene }, { "GameMode", currGameMode }, { "PlayerCount", currShooterPlayerCount }, { "IsHumanWaveMaster", isHumanWaveMaster } });

                ResetGeneralValues();
            }
        }

        /// <summary>
        /// The current game mode started a new round.
        /// </summary>
        /// <param name="roundNum">The new round number.</param>
        public static void RoundStart(int roundNum, string mapName, bool playerChoseMap)
        {
            if (generalValuesSet)
            {
                currRoundNum = roundNum;
                currMapName = mapName;
                currPlayerChoseMap = playerChoseMap;
                AnalyticsEvent.LevelStart(currRoundNum, new Dictionary<string, object> { { "Scene", currActiveScene }, { "GameMode", currGameMode }, { "PlayerCount", currShooterPlayerCount }, { "IsHumanWaveMaster", isHumanWaveMaster }, { "ChosenMapName", currMapName }, { "PlayerChoseMap", currPlayerChoseMap } });
            }
        }
        /// <summary>
        /// The current game mode ended a round.
        /// </summary>
        /// <param name="won">Did the round end in the shooter players being successful?</param>
        public static void RoundOver(bool won)
        {
            if (generalValuesSet)
            {
                if (won) AnalyticsEvent.LevelComplete(currRoundNum, new Dictionary<string, object> { { "Scene", currActiveScene }, { "GameMode", currGameMode }, { "PlayerCount", currShooterPlayerCount }, { "IsHumanWaveMaster", isHumanWaveMaster }, { "ChosenMapName", currMapName }, { "PlayerChoseMap", currPlayerChoseMap } });
                else AnalyticsEvent.LevelFail(currRoundNum, new Dictionary<string, object> { { "Scene", currActiveScene }, { "GameMode", currGameMode }, { "PlayerCount", currShooterPlayerCount }, { "IsHumanWaveMaster", isHumanWaveMaster }, { "ChosenMapName", currMapName }, { "PlayerChoseMap", currPlayerChoseMap } });

                currRoundNum = -1;
                currMapName = "";
                currPlayerChoseMap = false;
            }
        }

        /// <summary>
        /// The current game mode had an item purchased by a currency terminal.
        /// </summary>
        /// <param name="purchasedItemName">The name of the item.</param>
        public static void ItemPurchased(string purchasedItemName)
        {
            if (generalValuesSet)
            {
                AnalyticsEvent.Custom("ItemPurchased", new Dictionary<string, object> { { "PurchasedItem", purchasedItemName }, { "Scene", currActiveScene }, { "GameMode", currGameMode }, { "PlayerCount", currShooterPlayerCount }, { "IsHumanWaveMaster", isHumanWaveMaster } });
            }
        }
        /// <summary>
        /// The current game mode had an item that was used.
        /// </summary>
        /// <param name="usedItem">The name of the item.</param>
        public static void ItemUsed(string usedItem)
        {
            if(generalValuesSet)
            {
                AnalyticsEvent.Custom("ItemUsed", new Dictionary<string, object> { { "UsedItem", usedItem }, { "Scene", currActiveScene }, { "GameMode", currGameMode }, { "PlayerCount", currShooterPlayerCount }, { "IsHumanWaveMaster", isHumanWaveMaster } });
            }
        }
        /// <summary>
        /// The current game mode had an enemy die from an item.
        /// </summary>
        /// <param name="itemName">The name of the item.</param>
        /// <param name="enemyName">The name of the killed enemy.</param>
        public static void ItemKilledEnemy(string itemName, string enemyName)
        {
            if (generalValuesSet)
            {
                AnalyticsEvent.Custom("ItemKilledEnemy", new Dictionary<string, object> { { "ItemName", itemName }, { "EnemyName", enemyName }, { "Scene", currActiveScene }, { "GameMode", currGameMode }, { "PlayerCount", currShooterPlayerCount }, { "IsHumanWaveMaster", isHumanWaveMaster } });
            }
        }

        /// <summary>
        /// The current game mode had an enemy spawn.
        /// </summary>
        /// <param name="enemyName">The name of the enemy.</param>
        public static void EnemySpawned(string enemyName)
        {
            if (generalValuesSet)
            {
                AnalyticsEvent.Custom("EnemySpawned", new Dictionary<string, object> { { "EnemyName", enemyName }, { "Scene", currActiveScene }, { "GameMode", currGameMode }, { "PlayerCount", currShooterPlayerCount }, { "IsHumanWaveMaster", isHumanWaveMaster } });
            }
        }
        /// <summary>
        /// The current game mode had an enemy kill a player.
        /// </summary>
        /// <param name="enemyName">The name of the enemy.</param>
        public static void EnemyKilledPlayer(string enemyName)
        {
            if (generalValuesSet)
            {
                AnalyticsEvent.Custom("EnemyKilledPlayer", new Dictionary<string, object> { { "EnemyName", enemyName }, { "Scene", currActiveScene }, { "GameMode", currGameMode }, { "PlayerCount", currShooterPlayerCount }, { "IsHumanWaveMaster", isHumanWaveMaster } });
            }
        }

        /// <summary>
        /// Set general values posted by all messages.
        /// </summary>
        /// <param name="gameMode">The current game mode.</param>
        /// <param name="shooterPlayerCount">The current number of shooter players.</param>
        /// <param name="humanWaveMaster">Is the current wave master human controlled?</param>
        /// <param name="activeScene">The current active scene name.</param>
        private static void SetGeneralValues(string gameMode, int shooterPlayerCount, bool humanWaveMaster, string activeScene)
        {
            currGameMode = gameMode;
            currShooterPlayerCount = shooterPlayerCount;
            isHumanWaveMaster = humanWaveMaster;
            currActiveScene = activeScene;

            generalValuesSet = true;
        }
        /// <summary>
        /// Reset all the general values to default.
        /// </summary>
        private static void ResetGeneralValues()
        {
            currGameMode = "";
            currShooterPlayerCount = -1;
            isHumanWaveMaster = false;
            currActiveScene = "";

            generalValuesSet = false;
        }
    }
}

﻿using System;
using UnityEngine;

interface IObjectPresenceMonitor
{
    int ObjCount { get; }
    Func<GameObject, bool> MonitoredObj { get; set; }
}
﻿using System;

interface ITimer
{
    Action TimeupCallback { get; set; }
    float TimeRemaining { get; }
    float TimePassed { get; }

    bool StartCountdown(float time, bool eraseCallback = false);
    bool StartCountingUp();
    float CancelCounting();
}
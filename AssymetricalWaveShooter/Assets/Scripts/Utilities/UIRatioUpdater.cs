﻿using UnityEngine;

namespace Assets.Scripts.Utilities
{
    /// <summary>
    /// Updates the width or height of the attached rect transform to match given ratio.
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    public class UIRatioUpdater : MonoBehaviour
    {
        [Tooltip("The ratio desired.")]
        [SerializeField] private float ratio = 1;

        private RectTransform trans = null;

        /// <summary>
        /// Re-size the RectTransform to the desired ratio.
        /// </summary>
        /// <param name="updateWidth">Re-size by the width.</param>
        private void ResizeToRatio(bool updateWidth)
        {
            if(!updateWidth) trans.sizeDelta = new Vector2(0, (trans.rect.width / ratio - trans.rect.height) / 2);
            else trans.sizeDelta = new Vector2((trans.rect.height / ratio - trans.rect.width) / 2, 0);
        }
        /// <summary>
        /// Check to see to the current ratio is the same as the desired one.
        /// </summary>
        private void CheckRatio()
        {
            bool widthLarger = trans.rect.width > trans.rect.height;
            float currRatio = (widthLarger) ? trans.rect.width / trans.rect.height : trans.rect.height / trans.rect.width;

            if (currRatio != ratio) ResizeToRatio(widthLarger);
        }

        /// <summary>
        /// Initial setup.
        /// </summary>
        private void Setup()
        {
            trans = GetComponent<RectTransform>();
        }

        private void Awake()
        {
            Setup();
            CheckRatio();
        }
    }
}

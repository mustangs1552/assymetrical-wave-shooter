﻿using System;
using UnityEngine;

namespace MattRGeorge.Utilities
{
    public static class TransformUtility
    {
        public static Transform GetClosest(Vector3 startingPoint, Collider[] colliders)
        {
            float currClosestDist = 0;
            float currDist = 0;
            Transform currClosestTrans = null;
            foreach(Collider col in colliders)
            {
                currDist = Vector3.Distance(startingPoint, col.transform.position);

                if (currClosestTrans == null || currDist < currClosestDist)
                {
                    currClosestTrans = col.transform;
                    currClosestDist = currDist;
                }
            }

            return currClosestTrans;
        }
    }
}

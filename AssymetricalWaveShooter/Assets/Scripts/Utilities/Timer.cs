﻿using System;
using UnityEngine;

/// <summary>
/// A simple timer that supports both counting down and counting up.
/// </summary>
public class Timer : MonoBehaviour, ITimer
{
    #region Variables
    #region Public

    #endregion

    #region Properties
    /// <summary>
    /// A callback for when the count down timer is up.
    /// </summary>
    public Action TimeupCallback { get; set; }

    /// <summary>
    /// The time remaining in the count down.
    /// </summary>
    public float TimeRemaining
    {
        get
        {
            return (isCounting) ? currTimeLength - (Time.time - startTime) : 0;
        }
    }
    /// <summary>
    /// The amount of time that has passed since timer started counting up/down.
    /// </summary>
    public float TimePassed
    {
        get
        {
            return (isCounting) ? Time.time - startTime : 0;
        }
    }
    #endregion

    #region Private
    private bool isCounting = false;
    private float currTimeLength = 0;
    private float startTime = 0;
    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// Start the timer counting down.
    /// </summary>
    /// <param name="time">The time to count down from.</param>
    /// <param name="eraseCallback">Erase the callback(s) stored?</param>
    /// <returns>return true if successfully started.</returns>
    public bool StartCountdown(float time, bool eraseCallback = false)
    {
        if (!isCounting)
        {
            currTimeLength = time;
            startTime = Time.time;
            isCounting = true;
            if (eraseCallback) TimeupCallback = null;
            return true;
        }

        return false;
    }
    /// <summary>
    /// Start the timer counting up.
    /// </summary>
    /// <returns>True if successfully started.</returns>
    public bool StartCountingUp()
    {
        if(!isCounting)
        {
            startTime = Time.time;
            isCounting = true;
            return true;
        }

        return false;
    }

    /// <summary>
    /// Stop the timer counting up/down.
    /// </summary>
    /// <returns>The time that has passed since it started.</returns>
    public float CancelCounting()
    {
        float timePassed = TimePassed;
        isCounting = false;
        currTimeLength = 0;
        return timePassed;
    }

    /// <summary>
    /// Check if the timer has finished counting down.
    /// </summary>
    private void CheckTime()
    {
        if(isCounting && currTimeLength != 0 && Time.time - startTime >= currTimeLength)
        {
            currTimeLength = 0;
            isCounting = false;

            if (TimeupCallback != null) TimeupCallback();
        }
    }
    #endregion

    #region Unity Functions
    private void Update()
    {
        CheckTime();
    }
    #endregion
}

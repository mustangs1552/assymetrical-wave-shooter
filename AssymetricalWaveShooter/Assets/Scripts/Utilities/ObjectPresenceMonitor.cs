﻿using System;
using UnityEngine;

/// <summary>
/// An object that is placed on a trigger and monitors how many objects are currently within the bounds.
/// </summary>
public class ObjectPresenceMonitor : MonoBehaviour, IObjectPresenceMonitor
{
    #region Variables
    #region Public

    #endregion

    #region Properties
    /// <summary>
    /// The amount of objects currently within the bounds.
    /// </summary>
    public int ObjCount
    {
        get
        {
            return objCount;
        }
    }

    /// <summary>
    /// Check if the object that entered this trigger should be monitored.
    /// </summary>
    /// <param name="GameObject">object to check.</param>
    /// <returns>True if it should be monitored.</returns>
    public Func<GameObject, bool> MonitoredObj { get; set; }
    #endregion

    #region Private
    private int objCount = 0;
    #endregion
    #endregion

    #region Functions

    #endregion

    #region Unity Functions
    private void OnTriggerEnter(Collider other)
    {
        if(MonitoredObj == null || MonitoredObj(other.gameObject)) objCount++;
    }
    private void OnTriggerExit(Collider other)
    {
        if(MonitoredObj == null || MonitoredObj(other.gameObject)) objCount--;
    }
    #endregion
}

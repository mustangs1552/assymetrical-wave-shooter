﻿namespace MattRGeorge.Utilities
{
    public enum HorizontalDirections
    {
        None,

        Left,
        Right,

        Both
    }
}

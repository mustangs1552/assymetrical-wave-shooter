﻿using System.Collections.Generic;
using UnityEngine;

interface IObjectPool
{
    List<GameObject> AllGameObjects { get; }
    List<Transform> AllTransforms { get; }

    bool AddObject(GameObject obj);
    GameObject RemoveObject(GameObject obj);

    List<GameObject> GetPool(GameObject gameObjectType);
    bool CheckIfExist(GameObject obj);
    List<T> GetObjectComponents<T>();
    List<GameObject> GetObjectsWithTag(string tag);
    List<Transform> GetTransformsWithTag(string tag);

    void SetObjectsActive(bool active, string poolName = null);
}
﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public abstract class ObjectPoolManagerBase : MonoBehaviour
{
    public static ObjectPoolManagerBase SINGLETON = null;

    public abstract List<GameObject> AllGameObjects { get; }
    public abstract List<Transform> AllTransforms { get; }

    public abstract GameObject Instantiate(GameObject obj, bool asActive = true, bool poolManaged = true);
    public abstract GameObject Instantiate(GameObject obj, Transform parent, bool asActive = true, bool poolManaged = true);
    //public abstract GameObject InstantiateObject(GameObject obj, Transform parent, bool instantiateInWorldSpace, bool asActive = true, bool poolManaged = true);
    public abstract GameObject Instantiate(GameObject obj, Vector3 position, Quaternion rotation, bool asActive = true, bool poolManaged = true);
    public abstract GameObject Instantiate(GameObject obj, Vector3? position, Quaternion? rotation, Transform parent, bool asActive = true, bool poolManaged = true);

    public abstract void Destroy(GameObject obj);

    public abstract bool AddActiveObject(GameObject obj);
    public abstract bool AddInactiveObject(GameObject obj);

    public abstract bool CheckIfExists(GameObject obj);

    public abstract List<GameObject> GetActiveObjects(GameObject obj);
    public abstract List<T> GetActiveObjectsComponents<T>();
    public abstract List<GameObject> GetActiveObjectsWithTag(string tag);
    public abstract List<Transform> GetActiveTransformsWithTag(string tag);

    public abstract string ActivePoolToString();
    public abstract string InactivePoolToString();

    protected void Awake()
    {
        if (ObjectPoolManagerBase.SINGLETON == null) SINGLETON = this;
        else
        {
            Debug.LogError("More than one 'ObjectPoolManagerBase' singletons found!");
            Destroy(this);
        }
    }
}

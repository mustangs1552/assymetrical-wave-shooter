﻿namespace Assets.Scripts.Utilities.ObjectPooling.Interfaces
{
    public interface IObjectPoolEvents
    {
        void OnInstantiatedByObjectPool();
        void OnDestroyedByObjectPool();
    }
}

﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.Utilities.ObjectPooling.Interfaces;

/// <summary>
/// This class manages and provides access to both pools of objects: Active and Inactive objects.
/// </summary>
public class ObjectPoolManager : ObjectPoolManagerBase
{
    #region Variables
    #region Public
    
    #endregion

    #region Properties
    /// <summary>
    /// All the active gameobjects that are pooled.
    /// </summary>
    public override List<GameObject> AllGameObjects
    {
        get
        {
            return activePool.AllGameObjects;
        }
    }
    /// <summary>
    /// All the active gameobjects' transforms that are pooled.
    /// </summary>
    public override List<Transform> AllTransforms
    {
        get
        {
            return activePool.AllTransforms;
        }
    }
    #endregion

    #region Private
    private ObjectPool activePool = null;
    private ObjectPool inactivePool = null;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Get an in-active object from the pool or instantiate a new one to be used as an active object.
    /// Then enable it.
    /// </summary>
    /// <param name="obj">The kind of object desired.</param>
    /// <param name="asActive">Instantiate object as in-active instead?</param>
    /// <param name="poolManaged">Have the pool manager manage this object and parent it to the pool objects under this manager.</param>
    /// <returns>The object that has been grabbed/created.</returns>
    public override GameObject Instantiate(GameObject obj, bool asActive = true, bool poolManaged = true)
    {
        return Instantiate(obj, null, null, null, asActive, poolManaged);
    }
    /// <summary>
    /// Get an in-active object from the pool or instantiate a new one to be used as an active object.
    /// Then enable it.
    /// </summary>
    /// <param name="obj">The kind of object desired.</param>
    /// <param name="parent">The for the returned object.</param>
    /// <param name="asActive">Instantiate object as in-active instead?</param>
    /// <param name="poolManaged">Have the pool manager manage this object and parent it to the pool objects under this manager.</param>
    /// <returns>The object that has been grabbed/created.</returns>
    public override GameObject Instantiate(GameObject obj, Transform parent, bool asActive = true, bool poolManaged = true)
    {
        return Instantiate(obj, null, null, parent, asActive, poolManaged);
    }
    //public override GameObject Instantiate(GameObject obj, Transform parent, bool instantiateInWorldSpace, bool asActive = true, bool poolManaged = true)
    /// <summary>
    /// Get an in-active object from the pool or instantiate a new one to be used as an active object.
    /// Then enable it.
    /// </summary>
    /// <param name="obj">The kind of object desired.</param>
    /// <param name="position">The position of the returned object.</param>
    /// <param name="rotation">The rotation of the returned object.</param>
    /// <param name="asActive">Instantiate object as in-active instead?</param>
    /// <param name="poolManaged">Have the pool manager manage this object and parent it to the pool objects under this manager.</param>
    /// <returns>The object that has been grabbed/created.</returns>
    public override GameObject Instantiate(GameObject obj, Vector3 position, Quaternion rotation, bool asActive = true, bool poolManaged = true)
    {
        return Instantiate(obj, position, rotation, null, asActive, poolManaged);
    }
    /// <summary>
    /// Get an in-active object from the pool or instantiate a new one to be used as an active object.
    /// Then enable it.
    /// </summary>
    /// <param name="obj">The kind of object desired.</param>
    /// <param name="position">The position of the returned object.</param>
    /// <param name="rotation">The rotation of the returned object.</param>
    /// <param name="parent">The for the returned object.</param>
    /// <param name="asActive">Instantiate object as in-active instead?</param>
    /// <param name="poolManaged">Have the pool manager manage this object and parent it to the pool objects under this manager.</param>
    /// <returns>The object that has been grabbed/created.</returns>
    public override GameObject Instantiate(GameObject obj, Vector3? position, Quaternion? rotation, Transform parent, bool asActive = true, bool poolManaged = true)
    {
        if (obj == null) return null;

        GameObject instantiatedObj = inactivePool.RemoveObject(obj);
        if (instantiatedObj == null)
        {
            if (position == null && rotation == null && parent == null) instantiatedObj = Object.Instantiate(obj);
            else if (position == null && rotation == null && parent != null) instantiatedObj = Object.Instantiate(obj, parent);
            else if (position != null && rotation != null && parent == null) instantiatedObj = Object.Instantiate(obj, (Vector3)position, (Quaternion)rotation);
            else if (position != null && rotation != null && parent != null) instantiatedObj = Object.Instantiate(obj, (Vector3)position, (Quaternion)rotation, parent);
        }
        else
        {
            if (position != null) instantiatedObj.transform.position = (Vector3)position;
            if (rotation != null) instantiatedObj.transform.rotation = (Quaternion)rotation;
            if (parent != null) instantiatedObj.transform.SetParent(parent);
        }

        if (poolManaged)
        {
            if (asActive) activePool.AddObject(instantiatedObj);
            else inactivePool.AddObject(instantiatedObj);
        }
        else instantiatedObj.SetActive(asActive);

        IObjectPoolEvents events = instantiatedObj.GetComponent<IObjectPoolEvents>();
        if (events != null) events.OnInstantiatedByObjectPool();
        UpdateObjectActiveState();
        return instantiatedObj;
    }

    /// <summary>
    /// Remove the given object from the active pool and add it to the in-active pool.
    /// Then disable it.
    /// </summary>
    /// <param name="obj">The object to destroy.</param>
    public override void Destroy(GameObject obj)
    {
        if (obj == null) return;

        activePool.RemoveObject(obj);
        inactivePool.AddObject(obj);

        IObjectPoolEvents events = obj.GetComponent<IObjectPoolEvents>();
        if (events != null) events.OnDestroyedByObjectPool();
        UpdateObjectActiveState();
    }

    /// <summary>
    /// Add the already existing given object to the pool of active objects.
    /// Will move the object from the pool of inactive objects to active objects if already exists there.
    /// </summary>
    /// <param name="obj">The existing object to add.</param>
    /// <returns>True if successful.</returns>
    public override bool AddActiveObject(GameObject obj)
    {
        if (activePool.CheckIfExist(obj)) return false;
        else if(inactivePool.CheckIfExist(obj))
        {
            inactivePool.RemoveObject(obj);
            activePool.AddObject(obj);
            return true;
        }
        activePool.AddObject(obj);

        UpdateObjectActiveState();
        return true;
    }
    /// <summary>
    /// Add the already existing given object to the pool of inactive objects.
    /// Will move the object from the pool of active objects to inactive objects if already exists there.
    /// </summary>
    /// <param name="obj">The existing object to add.</param>
    /// <returns>True if successful.</returns>
    public override bool AddInactiveObject(GameObject obj)
    {
        if (inactivePool.CheckIfExist(obj)) return false;
        else if (activePool.CheckIfExist(obj))
        {
            activePool.RemoveObject(obj);
            inactivePool.AddObject(obj);
            return true;
        }
        inactivePool.AddObject(obj);

        UpdateObjectActiveState();
        return true;
    }

    /// <summary>
    /// Check if the given object exists in either pool.
    /// </summary>
    /// <param name="obj">The object to check.</param>
    /// <returns>True if exists.</returns>
    public override bool CheckIfExists(GameObject obj)
    {
        return activePool.CheckIfExist(obj) || inactivePool.CheckIfExist(obj);
    }

    public override List<GameObject> GetActiveObjects(GameObject obj)
    {
        return activePool.GetPool(obj);
    }
    /// <summary>
    /// Gets all of the given components off all the objects that are in the active objects pool.
    /// </summary>
    /// <typeparam name="T">The component to get from objects.</typeparam>
    /// <returns>A list of all the components found.</returns>
    public override List<T> GetActiveObjectsComponents<T>()
    {
        return activePool.GetObjectComponents<T>();
    }
    /// <summary>
    /// Get all the active gamobjects with the given tag.
    /// </summary>
    /// <param name="tag">The tag to search.</param>
    /// <returns>All active gameobjects with the given tag.</returns>
    public override List<GameObject> GetActiveObjectsWithTag(string tag)
    {
        return activePool.GetObjectsWithTag(tag);
    }
    /// <summary>
    /// Get all the active gamobjects' transforms with the given tag.
    /// </summary>
    /// <param name="tag">The tag to search.</param>
    /// <returns>All active gameobjects' transforms with the given tag.</returns>
    public override List<Transform> GetActiveTransformsWithTag(string tag)
    {
        return activePool.GetTransformsWithTag(tag);
    }

    /// <summary>
    /// Returns all the pools with their names and object counts from both active and in-active object pools.
    /// </summary>
    /// <returns>All the pools with their names and object counts from both active and in-active object pools.</returns>
    public override string ToString()
    {
        string returnStr = "Active Pool: " + ActivePoolToString();
        returnStr += "\nIn-active Pool: " + InactivePoolToString();
        return returnStr;
    }
    /// <summary>
    /// Returns all the pools with their names and object counts from the active object pool.
    /// </summary>
    /// <returns>All the pools with their names and object counts from the active object pool.</returns>
    public override string ActivePoolToString()
    {
        return activePool.ToString();
    }
    /// <summary>
    /// Returns all the pools with their names and object counts from the in-active object pool.
    /// </summary>
    /// <returns>All the pools with their names and object counts from the in-active object pool.</returns>
    public override string InactivePoolToString()
    {
        return inactivePool.ToString();
    }
    #endregion

    #region Private
    /// <summary>
    /// Create two pools, one for active objects and another for inactive objects.
    /// </summary>
    private void Setup()
    {
        GameObject activePoolGO = new GameObject("ACTIVE_OBJECTS_POOL");
        activePool = activePoolGO.AddComponent<ObjectPool>();
        activePool.transform.SetParent(transform);

        GameObject inactivePoolGO = new GameObject("INACTIVE_OBJECTS_POOL");
        inactivePool = inactivePoolGO.AddComponent<ObjectPool>();
        inactivePool.transform.SetParent(transform);
    }

    /// <summary>
    /// Makes sure that all the active objects are enabled and inactive objects are disabled.
    /// </summary>
    private void UpdateObjectActiveState()
    {
        activePool.SetObjectsActive(true);
        inactivePool.SetObjectsActive(false);
    }
    #endregion
    #endregion

    #region Unity Functions
    private new void Awake()
    {
        base.Awake();
        Setup();
    }
    #endregion
}

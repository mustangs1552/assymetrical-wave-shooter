﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Utilities
{
    /// <summary>
    /// Updates all the Text components in the children of this object font size to the smallest best fit font size.
    /// Can also size up or down texts to support larger texts as headers.
    /// </summary>
    public class UITextMatcher : MonoBehaviour
    {
        #region Variables
        [Tooltip("The maximum number of attempts before giving up updating fonts.")]
        [SerializeField] private int maxAttempts = 5;
        [Tooltip("Update the font sizes again when re-enabled?")]
        [SerializeField] private bool updateFontsOnEnable = false;

        [Header("Header Settings")]
        [Tooltip("The header text objects.")]
        [SerializeField] private List<Text> headerTexts = new List<Text>();
        [Tooltip("The parent of the header text objects.")]
        [SerializeField] private GameObject headerTextParent = null;
        [Tooltip("The size difference desired for the header text objects vs the rest of the text objects.")]
        [SerializeField] private int headerFontSizeDifference = 5;
        [Tooltip("Size up the headers from the rest of the text objects or size down the non-header text objects.")]
        [SerializeField] private bool sizeUpHeader = false;

        /// <summary>
        /// Finished updating the font sizes of the texts.
        /// </summary>
        public Action OnFinishFontUpdate { get; set; }
        /// <summary>
        /// The maximum attempts alloweed before giving up updating the text.
        /// </summary>
        public int MaxAttempts
        {
            get
            {
                return maxAttempts;
            }
            set
            {
                if (value > 0) maxAttempts = value;
            }
        }
        /// <summary>
        /// The parent of the header text objects.
        /// </summary>
        public GameObject HeaderTextParent
        {
            get
            {
                return headerTextParent;
            }
            set
            {
                headerTextParent = value;
            }
        }
        /// <summary>
        /// The size difference desired for the header text objects vs the rest of the text objects.
        /// </summary>
        public int HeaderFontSizeDifference
        {
            get
            {
                return headerFontSizeDifference;
            }
            set
            {
                if (value > 0) headerFontSizeDifference = value;
            }
        }
        /// <summary>
        /// Size up the headers from the rest of the text objects or size down the non-header text objects.
        /// </summary>
        public bool SizeUpHeader
        {
            get
            {
                return sizeUpHeader;
            }
            set
            {
                sizeUpHeader = value;
            }
        }

        private Text[] texts = null;
        private int smallestFont = 0;
        private int findFontAttemptCount = 0;
        private int setFontAttemptCount = 0;
        #endregion

        #region Methods
        /// <summary>
        /// Update the font sizes of all the texts.
        /// </summary>
        public void StartUpdateFontSizes(bool findNewTexts = false)
        {
            if (findNewTexts) FindTexts();
            foreach (Text text in texts) text.resizeTextForBestFit = true;

            findFontAttemptCount = 0;
            setFontAttemptCount = 0;
            StartCoroutine(UpdateFontSizes());
        }

        /// <summary>
        /// Find the smallest font size from the best fit texts.
        /// </summary>
        private void FindSmallestFontSize()
        {
            foreach(Text text in texts)
            {
                if (text.resizeTextForBestFit)
                {
                    if (smallestFont == 0 || smallestFont > text.cachedTextGenerator.fontSizeUsedForBestFit)
                    {
                        smallestFont = text.cachedTextGenerator.fontSizeUsedForBestFit;
                        if(smallestFont != 0) text.resizeTextForBestFit = false;
                    }
                }
            }
        }

        /// <summary>
        /// Set the current smallest font size to all the non-best fit texts.
        /// </summary>
        private void SetFontSizes()
        {
            foreach (Text text in texts) text.fontSize = smallestFont;
        }

        /// <summary>
        /// Disable the best fit option on all texts.
        /// </summary>
        private void DisableBestFit()
        {
            foreach (Text text in texts) text.resizeTextForBestFit = false;
        }

        /// <summary>
        /// Update the size of the headers to be larger than the rest of the text objects.
        /// </summary>
        private void UpdateHeaders()
        {
            if (headerTexts != null && headerTexts.Count > 0)
            {
                foreach (Text text in texts)
                {
                    bool isHeader = false;
                    foreach (Text header in headerTexts)
                    {
                        if (text == header)
                        {
                            isHeader = true;
                            break;
                        }
                    }

                    if (!sizeUpHeader && !isHeader) text.fontSize = (text.fontSize - headerFontSizeDifference <= 0) ? 1 : text.fontSize - headerFontSizeDifference;
                    else if(sizeUpHeader && isHeader) text.fontSize = text.fontSize + headerFontSizeDifference;
                }
            }
        }
        /// <summary>
        /// Update the font sizes using the best fit sizes.
        /// Wait 'til the end of the frame to move on and try again if the best fit size was not updated yet.
        /// </summary>
        private IEnumerator UpdateFontSizes()
        {
            yield return 0;

            while (smallestFont == 0 && findFontAttemptCount < maxAttempts)
            {
                FindSmallestFontSize();
                findFontAttemptCount++;

                yield return 0;
            }

            while (!CheckFonts() && setFontAttemptCount < maxAttempts)
            {
                setFontAttemptCount++;
                SetFontSizes();

                yield return 0;
            }

            DisableBestFit();
            UpdateHeaders();

            if (OnFinishFontUpdate != null) OnFinishFontUpdate();
        }

        /// <summary>
        /// Check to make sure that the texts all have the correct font size.
        /// </summary>
        /// <returns>True if all texts are the correct size.</returns>
        private bool CheckFonts()
        {
            if (smallestFont == 0) return false;

            foreach(Text text in texts)
            {
                if(text.fontSize != smallestFont) return false;
            }

            return true;
        }

        /// <summary>
        /// Find all the Text components in the children of this object.
        /// </summary>
        private void FindTexts()
        {
            texts = GetComponentsInChildren<Text>();

            if(headerTextParent != null)
            {
                Text[] headerParentTexts = headerTextParent.GetComponentsInChildren<Text>();
                foreach(Text parentText in headerParentTexts) headerTexts.Add(parentText);
            }
        }
        #endregion

        #region Unity Methods
        private void Awake()
        {
            StartUpdateFontSizes(true);
        }

        private void OnEnable()
        {
            if(updateFontsOnEnable) StartUpdateFontSizes(false);
        }
        #endregion
    }
}

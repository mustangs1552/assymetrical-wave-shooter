﻿public interface IHealthPiece
{
    void Setup(ITakesDamage mainHealth);

    void ResetCurrDamageMultiplier();
    void ResetStats();
}

﻿using System.Collections.Generic;
using UnityEngine;

public interface IQueueSpawner
{
    bool SpawningEnabled { get; set; }
    List<GameObject> SpawnQueue { get; }

    void AddToQueue(GameObject spawnObj);

    void RemoveFromFrontOfQueue();
    void RemoveFromBackOfQueue();
    void RemoveFromQueue(int index);
}
﻿namespace Assets.Scripts.Interfaces
{
    public interface IHasMenuLogic
    {
        void OnOpened();
        void OnClosed();
    }
}

﻿using UnityEngine;

interface IBullitt
{
    GameObject Shooter { get; }
    GameObject Gun { get; }
    float Life { get; }
    float Damage { get; }

    void OnSpawn(GameObject shooter, GameObject g);
    void OnSpawn(GameObject shooter, GameObject g, float life, float damage);
}
﻿using UnityEngine;

public interface IWeapon
{
    string WeaponName { get; }
    WeaponTypes WType { get; }
    float Range { get; }
    FiringMode WeaponFiringMode { get; }
    bool IsEmpty { get; }
    bool IsNotFull { get; }
    GameObject User { get; }

    void FireStart();
    void Firing();
    void FireEnd();

    void Reload();
    void CancelReload();

    void PickedUp(GameObject newUser);
    void Drop();
}

public enum WeaponTypes
{
    Primary,
    Secondary,
}
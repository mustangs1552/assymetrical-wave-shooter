﻿using System.Collections.Generic;

public interface IRoundControllerUI
{
    void UpdateEnemyOptions(List<EnemyOption> enemies);
    void UpdateNextRoundNum(int roundNum);
    void UpdateNextRoundPoints(int totalPoints);
    void UpdateNextRoundHeightmap(PMHeightmap heightmap);
    void UpdatePlayerCounts(int totalPlayers, int readyPlayers = 0);

    void UpdatePrePlayingCountdown(int secondsRemaining);
    void UpdatePlayingTime(int secondsPassed);

    void UpdateRoundSuccess(bool roundWon);
    void UpdatePrevRoundTime(int seconds);
    void UpdatePrevRoundNum(int roundNum);
    void UpdatePrevRoundPoints(int totalPoints, int totalPointsEnemies);
    void UpdatePrevRoundEnemies(List<EnemyOption> selectedEnemiesCosts, List<EnemyCount> selectedEnemiesCounts);
    void UpdatePrevRoundHeightmap(PMHeightmap heightmap);

    void UpdateEarnedCurrency(int amount);
}
﻿using System.Collections.Generic;

public interface IWaveMaster
{
    bool EnemiesRemaining { get; }

    void PrepareState(int totalPoints, List<EnemyOption> enemyOptions, List<PMHeightmap> heightmapOptions);
    void StartingState();
    void PlayingState();
    EndRoundWaveMasterInfo PostPlayState();
}
﻿public interface ISpawnSphere
{
    bool IsMoving { get; }

    void SpawnSpawnSphere(Pillar pillar);
    void DespawnSpawnSphere();
}
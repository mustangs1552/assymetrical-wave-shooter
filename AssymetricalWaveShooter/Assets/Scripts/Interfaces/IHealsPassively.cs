﻿interface IHealsPassively
{
    void ResetCurrPassiveHealAmount();
    void ResetCurrPassiveHealRate();
    void ResetStats();
}
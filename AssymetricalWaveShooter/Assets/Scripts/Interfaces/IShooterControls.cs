﻿public interface IShooterControls
{
    void PauseMovement();
    void ResumeMovement();
}
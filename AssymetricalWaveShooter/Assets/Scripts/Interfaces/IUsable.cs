﻿using UnityEngine;

public interface IUsable
{
    bool Use(GameObject playerUsing);
}
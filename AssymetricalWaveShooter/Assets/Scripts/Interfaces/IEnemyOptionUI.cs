﻿interface IEnemyOptionUI
{
    string Name { get; }

    void UpdateNameCost(string name, int cost);
    void UpdateCount(int count);
}
﻿using System;

public interface IWeaponManager
{
    Action ActiveWeaponFireStart { get; }
    Action ActiveWeaponFiring { get; }
    Action ActiveWeaponFireEnd { get; }
    Action ActiveWeaponReload { get; }

    bool PickUpWeapon(IWeapon weapon);
    void DropWeapon();
    void DropWeapon(WeaponTypes slot);

    void ChangeWeaponUp();
    void ChangeWeaponDown();
    void ToggleLastWeapon();
    void ChangeWeapon(WeaponTypes slot);
    void ChangeWeapon(int slotNum);
}
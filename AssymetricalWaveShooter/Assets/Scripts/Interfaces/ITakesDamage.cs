﻿using UnityEngine;

public interface ITakesDamage
{
    bool TakeDamage(float amount, GameObject source);
    bool Heal(float amount, GameObject source);

    void ResetCurrDamageMultiplier();
}

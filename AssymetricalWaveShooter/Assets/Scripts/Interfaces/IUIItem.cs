﻿using UnityEngine;

namespace Assets.Scripts.Interfaces
{
    public interface IUIItem
    {
        string Name { get; }
        string Description { get; }
        Texture Icon { get; }
        int Cost { get; }
    }
}

﻿public interface IHasHealth
{
    float CurrHealth { get; }
    float CurrHealthPerc { get; }

    bool AdjustHealth(float amount);

    void ResetCurrHealth();
    void ResetCurrMaxHealth();
    void ResetStats();

    void Revive(bool resetAllStats);
    void Kill();
}
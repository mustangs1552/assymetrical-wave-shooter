﻿using System;
using UnityEngine;

/// <summary>
/// The base class for the round controller.
/// </summary>
public abstract class RoundControllerBase : MonoBehaviour
{
    public static RoundControllerBase SINGLETON = null;

    /// <summary>
    /// The current state of the round controller.
    /// </summary>
    public RoundState State
    {
        get
        {
            return state;
        }
    }
    protected RoundState state = RoundState.Idle;
    /// <summary>
    /// Is the wave master ready?
    /// </summary>
    public abstract bool WaveMasterReady { get; set; }
    public abstract Action<bool> OnRoundEnd { get; set; }
    public abstract Action OnPlayersReady { get; set; }
    /// <summary>
    /// The players gained currency.
    /// </summary>
    public abstract int EarnedCurrency { get; }

    /// <summary>
    /// Start the next round.
    /// </summary>
    /// <param name="playerCount">The amount of shooter players.</param>
    /// <param name="startRoundNum">The round number of this round.</param>
    /// <returns>True if successfully started round.</returns>
    public abstract bool StartRound(int playerCount, int startRoundNum = 1);
    /// <summary>
    /// A shooter player is ready.
    /// </summary>
    public abstract void ShooterPlayerReady();
    /// <summary>
    /// A shooter player has un-readied.
    /// </summary>
    public abstract void ShooterPlayerUnReady();
    /// <summary>
    /// A shooter player is in position.
    /// </summary>
    public abstract void ShooterPlayerInPosition();
    /// <summary>
    /// A shooter player has been defeated.
    /// </summary>
    public abstract void ShooterPlayerDefeated();
    /// <summary>
    /// Set the heightmap for the round.
    /// </summary>
    /// <param name="heightmap">The heightmap to be set.</param>
    /// <returns>True if successfully set.</returns>
    public abstract bool SetNextHeightmap(PMHeightmap heightmap);

    public abstract bool CheckRemoveCurrency(int amount);

    protected void Awake()
    {
        if (RoundControllerBase.SINGLETON == null) SINGLETON = this;
        else
        {
            Debug.LogError("More than one 'RoundControllerBase' singletons found!");
            Destroy(this);
        }
    }
}

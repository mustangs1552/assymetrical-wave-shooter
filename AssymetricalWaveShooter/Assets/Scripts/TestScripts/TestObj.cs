﻿using UnityEngine;

/// <summary>
/// 
/// </summary>
public class TestObj : MonoBehaviour, IUsable
{
    #region Variables
    #region Public

    #endregion

    #region Properties

    #endregion

    #region Private

    #endregion
    #endregion

    #region Functions
    public bool Use(GameObject playerUsing)
    {
        Debug.Log(gameObject.name + " used by " + playerUsing.name + "!");
        return true;
    }
    #endregion

    #region Unity Functions
    private void Start()
    {
        ObjectPoolManagerBase.SINGLETON.AddActiveObject(gameObject);
    }
    #endregion
}

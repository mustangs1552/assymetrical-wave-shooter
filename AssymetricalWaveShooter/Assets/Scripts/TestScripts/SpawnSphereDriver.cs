﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class SpawnSphereDriver : MonoBehaviour
{
    #region Variables
    #region Public
    [SerializeField] private List<GameObject> spawnSpheres = new List<GameObject>();
    [SerializeField] private GameObject obj = null;
    [SerializeField] private GameObject[] weaponOptions = null;
    #endregion

    #region Properties

    #endregion

    #region Private
    
    #endregion
    #endregion

    #region Functions
    private GameObject SetAIWeapon()
    {
        GameObject spawnedObj = ObjectPoolManagerBase.SINGLETON.Instantiate(obj, false);
        AIBrain spawnedObjAIBrain = spawnedObj.GetComponent<AIBrain>();
        if (spawnedObjAIBrain != null)
        {
            spawnedObjAIBrain.RunSetup();

            AttackTrait atkTrait = (AttackTrait)spawnedObjAIBrain.GetTrait(typeof(AttackTrait));
            if (atkTrait != null)
            {
                int randI = Random.Range(0, weaponOptions.Length);
                IWeapon chosenWeapon = weaponOptions[randI].GetComponent<IWeapon>();
                atkTrait.PickUpWeapon(chosenWeapon);
            }
        }

        return spawnedObj;
    }

    private void CheckInput()
    {
        if (Input.GetMouseButtonUp(0))
        {
            foreach (GameObject sphere in spawnSpheres)
            {
                IQueueSpawner spawner = sphere.GetComponent<IQueueSpawner>();
                spawner.AddToQueue(SetAIWeapon());
                spawner.SpawningEnabled = true;
            }
        }

        if (Input.GetMouseButtonUp(1))
        {
            foreach (GameObject sphere in spawnSpheres)
            {
                IQueueSpawner spawner = sphere.GetComponent<IQueueSpawner>();
                spawner.RemoveFromBackOfQueue();
            }
        }
    }
    #endregion

    #region Unity Functions
    private void Update()
    {
        CheckInput();
    }
    #endregion
}

﻿using UnityEngine;

/// <summary>
/// 
/// </summary>
public class ClickSpawn : MonoBehaviour
{
    #region Variables
    #region Public
    [SerializeField] private GameObject spawnObj = null;
    [SerializeField] private float heightOffset = 5;
    [SerializeField] private GameObject[] weaponOptions = null;
    #endregion

    #region Properties

    #endregion

    #region Private
    private GameObject lastSpawnedObj = null;
    #endregion
    #endregion

    #region Functions
    private void CheckInput()
    {
        if(Input.GetMouseButtonUp(0))
        {
            RaycastHit hit = new RaycastHit();
            Camera mainCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
            if(Physics.Raycast(mainCam.ScreenPointToRay(Input.mousePosition), out hit))
            {
                lastSpawnedObj = ObjectPoolManagerBase.SINGLETON.Instantiate(spawnObj, hit.transform.position + (Vector3.up * heightOffset), Quaternion.identity);
                Invoke("ManageLastSpawnedObj", 1f);
            }
        }
    }

    private void ManageLastSpawnedObj()
    {
        if (lastSpawnedObj != null && weaponOptions != null && weaponOptions.Length > 0 && lastSpawnedObj.GetComponent<AIBrain>() != null)
        {
            AttackTrait atkTrait = (AttackTrait)lastSpawnedObj.GetComponent<AIBrain>().GetTrait(typeof(AttackTrait));
            int rand = Random.Range(0, weaponOptions.Length - 1);
            IWeapon chosenWeapon = weaponOptions[rand].GetComponent<IWeapon>();
            atkTrait.PickUpWeapon(chosenWeapon);
        }
    }
    #endregion

    #region Unity Functions
    private void Update()
    {
        CheckInput();
    }
    #endregion
}

﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class MapTestDriver : MonoBehaviour
{
    #region Variables
    #region Public
    [SerializeField] private GameObject pillarManager = null;
    [SerializeField] private PMHeightmap heightmap = null;
    #endregion

    #region Properties

    #endregion

    #region Private
    private IPillarManager pManager = null;
    private Dictionary<string, List<Vector3>> pois = new Dictionary<string, List<Vector3>>();
    #endregion
    #endregion

    #region Functions
    private void RaisePillars()
    {
        pManager.RaisePillars();
    }
    private void LowerPillars()
    {
        pManager.LowerPillars();
    }

    private void CheckPOIs()
    {
        pois = pManager.POILocations;
        foreach (KeyValuePair<string, List<Vector3>> poi in pois)
        {
            foreach (Vector3 pos in poi.Value)
            {
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.transform.position = pos;
            }
        }

    }
    #endregion

    #region Unity Functions
    private void Start()
    {
        pManager = pillarManager.GetComponent<IPillarManager>();

        pManager.ApplyHeightmap(heightmap);
        CheckPOIs();

        Invoke("RaisePillars", 1);
        //Invoke("LowerPillars", 5);
    }
    #endregion
}

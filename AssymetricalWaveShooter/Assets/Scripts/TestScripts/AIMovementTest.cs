﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class AIMovementTest : MonoBehaviour
{
    #region Variables
    #region Public

    #endregion

    #region Properties

    #endregion

    #region Private

    #endregion
    #endregion

    #region Functions
    private void DestinationTest()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100)) ObjectPoolManagerBase.SINGLETON.GetActiveObjectsComponents<IAIMovementTrait>().ForEach(x => x.AssignTarget(hit.point));
        }
    }
    #endregion

    #region Unity Functions
    private void Update()
    {
        DestinationTest();
    }
    #endregion
}

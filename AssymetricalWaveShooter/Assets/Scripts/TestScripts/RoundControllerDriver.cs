﻿using UnityEngine;

/// <summary>
/// 
/// </summary>
public class RoundControllerDriver : MonoBehaviour
{
    #region Variables
    #region Public

    #endregion

    #region Properties

    #endregion

    #region Private

    #endregion
    #endregion

    #region Functions
    private void StartRound(int startRoundNum = 0)
    {
        if (startRoundNum > 0) RoundControllerBase.SINGLETON.StartRound(1, startRoundNum);
        else RoundControllerBase.SINGLETON.StartRound(1);
    }

    private void CheckInput()
    {
        if (Input.GetKeyUp(KeyCode.O)) StartRound();
        else if (Input.GetKeyUp(KeyCode.Alpha5)) StartRound(5);
        else if (Input.GetKeyUp(KeyCode.Alpha7)) StartRound(7);
        //else if (Input.GetKeyUp(KeyCode.Space)) RoundControllerBase.SINGLETON.ShooterPlayerReady();
        else if (Input.GetKeyUp(KeyCode.DownArrow)) RoundControllerBase.SINGLETON.ShooterPlayerUnReady();
    }
    #endregion

    #region Unity Functions
    private void Update()
    {
        CheckInput();
    }
    #endregion
}

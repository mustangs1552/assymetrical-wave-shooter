﻿using UnityEngine;
using Assets.Scripts.AI.Enemy;

namespace Assets.Scripts.TestScripts
{
    public class CommandTraitDriver : MonoBehaviour
    {
        [SerializeField] private bool attackMoveTo = false;
        [SerializeField] private Transform[] moveTargets = null;
        [SerializeField] private Transform[] attackTargets = null;

        private void CheckInput()
        {
            if(Input.GetKey(KeyCode.M))
            {
                if(Input.GetKeyUp(KeyCode.Alpha1))
                {
                    ObjectPoolManagerBase.SINGLETON.GetActiveObjectsComponents<CommandsTrait>().ForEach(x => x.MoveTo(moveTargets[0].position, attackMoveTo));
                }
                if (Input.GetKeyUp(KeyCode.Alpha2))
                {
                    ObjectPoolManagerBase.SINGLETON.GetActiveObjectsComponents<CommandsTrait>().ForEach(x => x.MoveTo(moveTargets[1].position, attackMoveTo));
                }
                if (Input.GetKeyUp(KeyCode.Alpha3))
                {
                    ObjectPoolManagerBase.SINGLETON.GetActiveObjectsComponents<CommandsTrait>().ForEach(x => x.MoveTo(moveTargets[2].position, attackMoveTo));
                }
                if (Input.GetKeyUp(KeyCode.Alpha4))
                {
                    ObjectPoolManagerBase.SINGLETON.GetActiveObjectsComponents<CommandsTrait>().ForEach(x => x.MoveTo(moveTargets[3].position, attackMoveTo));
                }
            }
            if(Input.GetKey(KeyCode.N))
            {
                if (Input.GetKeyUp(KeyCode.Alpha1))
                {
                    ObjectPoolManagerBase.SINGLETON.GetActiveObjectsComponents<CommandsTrait>().ForEach(x => x.AttackTarget(attackTargets[0]));
                }
                if (Input.GetKeyUp(KeyCode.Alpha2))
                {
                    ObjectPoolManagerBase.SINGLETON.GetActiveObjectsComponents<CommandsTrait>().ForEach(x => x.AttackTarget(attackTargets[1]));
                }
            }
        }

        private void Update()
        {
            CheckInput();
        }
    }
}

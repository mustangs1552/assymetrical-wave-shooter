﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
public class RoundControllerTestUI : MonoBehaviour, IRoundControllerUI
{
    #region Variables
    #region Public
    [SerializeField] private Text uiText = null;
    [SerializeField] private RawImage nextRoundHeightmapTex = null;
    [SerializeField] private RawImage nextRoundPOIMapTex = null;
    [SerializeField] private RawImage prevRoundHeightmapTex = null;
    [SerializeField] private RawImage prevRoundPOIMapTex = null;
    #endregion

    #region Properties

    #endregion

    #region Private
    private List<EnemyOption> enemyOptions = new List<EnemyOption>();
    private PMHeightmap nextHeightmap = null;
    private PMHeightmap prevHeightmap = null;
    private int nextRoundNum = 0;
    private int nextRoundTotalPoints = 0;

    private List<EnemyOption> prevRoundSelectEnemiesOptions = new List<EnemyOption>();
    private List<EnemyCount> prevRoundSelectedEnemyCounts = new List<EnemyCount>();
    private int prePlayingSecsRemaining = 0;
    private int playingTimePassed = 0;
    private int prevRoundNum = 0;
    private int prevRoundTotalPoints = 0;
    private int prevRoundtotalEnemyPoints = 0;
    private int prevRoundSeconds = 0;
    #endregion
    #endregion

    #region Functions
    public void UpdateEnemyOptions(List<EnemyOption> enemies)
    {
        enemyOptions = enemies;
        UpdateUI();
    }
    public void UpdateNextRoundHeightmap(PMHeightmap heightmap)
    {
        nextHeightmap = heightmap;
        UpdateUI();
    }
    public void UpdateNextRoundNum(int roundNum)
    {
        nextRoundNum = roundNum;
        UpdateUI();
    }
    public void UpdateNextRoundPoints(int totalPoints)
    {
        nextRoundTotalPoints = totalPoints;
        UpdateUI();
    }
    public void UpdatePlayerCounts(int totalPlayers, int readyPlayers = 0)
    {
        throw new System.NotImplementedException();
    }

    public void UpdatePrePlayingCountdown(int secondsRemaining)
    {
        prePlayingSecsRemaining = secondsRemaining;
        UpdateUI();
    }
    public void UpdatePlayingTime(int secondsPassed)
    {
        playingTimePassed = secondsPassed;
        UpdateUI();
    }

    public void UpdateRoundSuccess(bool roundWon)
    {
        throw new System.NotImplementedException();
    }
    public void UpdatePrevRoundEnemies(List<EnemyOption> selectedEnemiesCosts, List<EnemyCount> selectedEnemiesCounts)
    {
        prevRoundSelectEnemiesOptions = selectedEnemiesCosts;
        prevRoundSelectedEnemyCounts = selectedEnemiesCounts;

        UpdateUI();
    }
    public void UpdatePrevRoundHeightmap(PMHeightmap heightmap)
    {
        prevHeightmap = heightmap;
        UpdateUI();
    }
    public void UpdatePrevRoundNum(int roundNum)
    {
        prevRoundNum = roundNum;
        UpdateUI();
    }
    public void UpdatePrevRoundPoints(int totalPoints, int totalPointsEnemies)
    {
        prevRoundTotalPoints = totalPoints;
        prevRoundtotalEnemyPoints = totalPointsEnemies;

        UpdateUI();
    }
    public void UpdatePrevRoundTime(int seconds)
    {
        prevRoundSeconds = seconds;
        UpdateUI();
    }

    private void UpdateUI()
    {
        if(uiText != null)
        {
            string text = "";
            uiText.text = "";

            if (enemyOptions != null && enemyOptions.Count > 0)
            {
                text += "Enemy Costs: ";
                foreach (EnemyOption enemy in enemyOptions) text += enemy.Enemy.name + " (" + enemy.PointCost + ")" + ", ";
            }
            else text += "No enemy costs!";

            text += "\nNEXT ROUND:";
            text += "\nRound Number: " + nextRoundNum;
            text += "\nTotal Points Available: " + nextRoundTotalPoints;
            text += "\n\nPre-playing Countdown: " + prePlayingSecsRemaining;
            text += "\n\nPlaying Time Passed: " + (int)playingTimePassed / 60 + ":" + (playingTimePassed % 60).ToString("D2");

            text += "\n\nPREVIOUS ROUND:";
            text += "\nRound Number: " + prevRoundNum;
            text += "\nTotal Points Available: " + prevRoundTotalPoints;
            text += "\nTotal Points of Enemies: " + prevRoundtotalEnemyPoints;
            text += "\nRound Time: " + (int)prevRoundSeconds / 60 + ":" + (prevRoundSeconds % 60).ToString("D2");
            if (prevRoundSelectEnemiesOptions != null && prevRoundSelectEnemiesOptions.Count > 0)
            {
                text += "\nEnemies Chosen Cost: ";
                foreach (EnemyOption enemy in prevRoundSelectEnemiesOptions) text += "\n\t" + enemy.Enemy.name + " (" + enemy.PointCost + ")" + ", ";
            }
            else text += "\nNo enemies chosen!";
            if (prevRoundSelectedEnemyCounts != null && prevRoundSelectedEnemyCounts.Count > 0)
            {
                text += "\nEnemies Chosen Counts: ";
                foreach (EnemyCount enemy in prevRoundSelectedEnemyCounts) text += "\n\t" + enemy.Enemy.name + " (" + enemy.Count + ")" + ", ";
            }
            else text += "\nNo enemies chosen!";

            uiText.text = text;
        }

        if(nextRoundHeightmapTex != null && nextRoundPOIMapTex != null && nextHeightmap != null)
        {
            nextRoundHeightmapTex.texture = nextHeightmap.Heightmap;
            nextRoundPOIMapTex.texture = nextHeightmap.POIMap;
        }
        if (prevRoundHeightmapTex != null && prevRoundPOIMapTex != null && prevHeightmap != null)
        {
            prevRoundHeightmapTex.texture = prevHeightmap.Heightmap;
            prevRoundPOIMapTex.texture = prevHeightmap.POIMap;
        }
    }

    public void UpdateEarnedCurrency(int amount)
    {
        
    }
    #endregion

    #region Unity Functions

    #endregion
}

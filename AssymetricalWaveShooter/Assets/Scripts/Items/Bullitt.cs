﻿using UnityEngine;

/// <summary>
/// A bullitt that can be fired from a weapon.
/// Holds information about the shooter.
/// </summary>
public class Bullitt : MonoBehaviour, IBullitt
{
    #region Variables
    #region Public
    [Tooltip("The speed that the bullitt moves.")]
    [SerializeField] private float speed = 1;
    [Tooltip("The time that the bullitt stays alive when not hitting anything.")]
    [SerializeField] private float life = 10;
    [Tooltip("The damage that this bullitt deals.")]
    [SerializeField] private float damage = 1;
    #endregion

    #region Properties
    /// <summary>
    /// The shooter that shot this bullitt.
    /// </summary>
    public GameObject Shooter
    {
        get
        {
            return shooter;
        }
    }
    /// <summary>
    /// The gameobject that spawned this bullit.
    /// </summary>
    public GameObject Gun
    {
        get
        {
            return gun;
        }
    }
    /// <summary>
    /// The bullitt's current life.
    /// </summary>
    public float Life
    {
        get
        {
            return currLife;
        }
    }
    /// <summary>
    /// The bullitt's current damage.
    /// </summary>
    public float Damage
    {
        get
        {
            return currDamage;
        }
    }
    #endregion

    #region Private
    private GameObject shooter = null;
    private GameObject gun = null;
    private float startMove = 0;
    private float currLife = 0;
    private float currDamage = 0;
    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// Setup for this bullitt when it is spawned.
    /// </summary>
    /// <param name="s">The shooter that shot this bullitt.</param>
    public void OnSpawn(GameObject s, GameObject g)
    {
        shooter = s;
        gun = g;
        currLife = life;
        currDamage = damage;
    }
    /// <summary>
    /// Setup for this bullitt when it is spawned.
    /// </summary>
    /// <param name="s">The shooter that shot this bullitt.</param>
    /// <param name="l">The life of this bullitt.</param>
    /// <param name="d">The damage of this bullitt.</param>
    public void OnSpawn(GameObject s, GameObject g, float l, float d)
    {
        shooter = s;
        gun = g;
        currLife = l;
        currDamage = d;
    }

    /// <summary>
    /// Move the bullitt down its path.
    /// </summary>
    private void Move()
    {
        if (startMove == 0) startMove = Time.time;

        transform.Translate(transform.forward * speed * Time.deltaTime, Space.World);
        if (Time.time - startMove > currLife) DestroySelf();
    }

    /// <summary>
    /// Destroy this bullitt.
    /// </summary>
    private void DestroySelf()
    {
        startMove = 0;
        shooter = null;
        gun = null;
        currLife = life;
        currDamage = damage;
        ObjectPoolManagerBase.SINGLETON.Destroy(gameObject);
    }

    /// <summary>
    /// Deal damage to whatever this bullitt hit if it can be damaged.
    /// </summary>
    /// <param name="target">The object to take damage.</param>
    private void DealDamage(GameObject target)
    {
        if (RoundControllerBase.SINGLETON.State == RoundState.Playing)
        {
            ITakesDamage damageObj = target.GetComponent<ITakesDamage>();
            if (damageObj != null) damageObj.TakeDamage(damage, gun);
        }
    }
    #endregion

    #region Unity Functions
    private void Update()
    {
        Move();
    }

    private void OnCollisionEnter(Collision collision)
    {
        DealDamage(collision.gameObject);
        DestroySelf();
    }
    #endregion
}

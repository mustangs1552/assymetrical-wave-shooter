﻿using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Interfaces;
using Assets.Scripts.UI;
using Assets.Scripts.Controllers;
using MattRGeorge.Utilities;

namespace Assets.Scripts.Items
{
    /// <summary>
    /// The main script for the Currency Terminal.
    /// Primary purpose is to handle the process of purchases, spawned items in the world, and controls the state of the UI display.
    /// </summary>
    public class CurrencyTerminal : MonoBehaviour
    {
        #region Variables
        [Tooltip("The spawn point for purchased items.")]
        [SerializeField] private Transform itemSpawn = null;
        [Tooltip("The UI display object.")]
        [SerializeField] private CurrencyTerminalUI ui = null;

        private List<GameObject> spawnedItems = new List<GameObject>();
        #endregion

        #region Methods
        /// <summary>
        /// Spawn the given item after checking and deducting the required cost.
        /// </summary>
        /// <param name="item">The desired item.</param>
        public void MakePurchase(IUIItem item)
        {
            if (ui.gameObject.activeInHierarchy && RoundControllerBase.SINGLETON.CheckRemoveCurrency(item.Cost))
            {
                GameObject spawnedObj = ((MonoBehaviour)item).gameObject;
                spawnedItems.Add(ObjectPoolManagerBase.SINGLETON.Instantiate(spawnedObj, itemSpawn.position, itemSpawn.rotation, true, false));

                UnityAnalyticsController.ItemPurchased(GameObjectUtilities.GetActualName(((MonoBehaviour)item).gameObject));
            }
        }

        /// <summary>
        /// Turn off the UI display.
        /// </summary>
        public void TurnOff()
        {
            ui.TurnOff();
        }
        /// <summary>
        /// Turn on the UI display.
        /// </summary>
        public void TurnOn()
        {
            ui.TurnOn();
        }

        /// <summary>
        /// Destroy all the spawned items that this terminal spawned.
        /// </summary>
        public void DestroySpawnedItems()
        {
            foreach (GameObject item in spawnedItems) ObjectPoolManagerBase.SINGLETON.Destroy(item);
            spawnedItems = new List<GameObject>();
        }

        /// <summary>
        /// All players are ready for the next round.
        /// Tunr off the terminal, they can't buy items anymore.
        /// </summary>
        private void OnPlayersReady()
        {
            TurnOff();
        }
        /// <summary>
        /// The round just ended.
        /// Turn the terminal back on and if they loss then destroy all purchased items.
        /// </summary>
        /// <param name="roundSuccess"></param>
        private void OnRoundEnded(bool roundSuccess)
        {
            if (!roundSuccess) DestroySpawnedItems();

            TurnOn();
        }

        /// <summary>
        /// Initial setup.
        /// </summary>
        private void Setup()
        {
            if (itemSpawn == null) Debug.LogError("No 'itemSpawn' assigned!");
            if (ui == null) Debug.LogError("No 'ui' assigned!");
        }
        #endregion

        #region Unity Methods
        private void Awake()
        {
            Setup();
            TurnOn();
        }
        private void Start()
        {
            RoundControllerBase.SINGLETON.OnPlayersReady += OnPlayersReady;
            RoundControllerBase.SINGLETON.OnRoundEnd += OnRoundEnded;
        }
        #endregion
    }
}

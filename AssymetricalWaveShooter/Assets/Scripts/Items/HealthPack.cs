﻿using UnityEngine;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.Items
{
    /// <summary>
    /// A health pack that heals an amount of health when used.
    /// </summary>
    public class HealthPack : MonoBehaviour, IUsable, IUIItem
    {
        [Tooltip("The name of the item used in-game.")]
        [SerializeField] private string itemName = "";
        [Tooltip("The amount of health to heal.")]
        [SerializeField] private float healAmount = 10;
        [Tooltip("A description about this item.")]
        [SerializeField] private string description = "";
        [Tooltip("An icon of this item.")]
        [SerializeField] private Texture icon = null;
        [Tooltip("The cost of this weapon in a terminal.")]
        [SerializeField] private int cost = 10;

        /// <summary>
        /// The name of the item used in-game.
        /// </summary>
        public string Name => itemName;
        /// <summary>
        /// A description about this item.
        /// </summary>
        public string Description => description;
        /// <summary>
        /// An icon of this item.
        /// </summary>
        public Texture Icon => icon;
        /// <summary>
        /// The cost of this weapon in a terminal.
        /// </summary>
        public int Cost => cost;

        /// <summary>
        /// Heal the user who picked this up.
        /// </summary>
        /// <param name="playerUsing">The user to heal.</param>
        /// <returns>true if successfully used.</returns>
        public bool Use(GameObject playerUsing)
        {
            ITakesDamage playerHealth = playerUsing.GetComponent<ITakesDamage>();
            if (playerHealth == null) playerHealth = playerUsing.transform.parent.GetComponent<ITakesDamage>();

            if (playerHealth != null && playerHealth.Heal(healAmount, gameObject))
            {
                ObjectPoolManagerBase.SINGLETON.Destroy(gameObject);
                return true;
            }

            return false;
        }
    }
}

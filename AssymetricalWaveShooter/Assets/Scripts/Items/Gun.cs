﻿using UnityEngine;
using UnityEngine.UI;
using MattRGeorge.Utilities;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Utilities.ObjectPooling.Interfaces;

public enum FiringMode
{
    Single,
    Burst,
    Auto,
}

[RequireComponent(typeof(Rigidbody))]
/// <summary>
/// A general gun that can be picked up and dropped.
/// Supports different firing modes.
/// </summary>
public class Gun : MonoBehaviour, IWeapon, IUsable, IUIItem, IObjectPoolEvents
{
    #region Variables
    #region Public
    [Tooltip("The name of this weapon.")]
    [SerializeField] private string weaponName = "";
    [Tooltip("The type of the slot this weapon goes in.")]
    [SerializeField] private WeaponTypes wType = WeaponTypes.Primary;

    [Header("Firing Settings")]
    [Tooltip("The bullitt that this weapon fires.")]
    [SerializeField] private GameObject bullitt = null;
    [Tooltip("The firing mode this weapon fires at.")]
    [SerializeField] private FiringMode firingMode = FiringMode.Single;
    [Tooltip("The amount of ammo each clip holds.")]
    [SerializeField] private int clipSize = 10;
    [Tooltip("The firing rate this weapon fires at.")]
    [SerializeField] private float fireingRate = 1;
    [Tooltip("The delay between burst fire shots where the weapon can fire another group of shots.")]
    [SerializeField] private float burstFireDelay = 1;
    [Tooltip("Amount of bullitts in each group of shots for a burst fire weapon.")]
    [SerializeField] private int burstShotCount = 3;
    [Tooltip("The range(life) of the fired bullits.")]
    [SerializeField] private float range = 10;
    [Tooltip("The damage of the fired bullitts.")]
    [SerializeField] private float damage = 1;
    [Tooltip("The time it takes to reload the gun before it can be fired again.")]
    [SerializeField] private float reloadTime = 3;

    [Header("UI Settings")]
    [Tooltip("The UI text that shows the ammo in the curent clip.")]
    [SerializeField] private Text ammoText = null;
    [Tooltip("The UI text that shows the max ammo in each clip.")]
    [SerializeField] private Text maxAmmoText = null;
    [Tooltip("Show the ammo as a percentage remaining?")]
    [SerializeField] private bool showAmmoAsPercentage = false;
    [Tooltip("The icon representing this item to show for UIs.")]
    [SerializeField] private Texture icon = null;
    [Tooltip("The cost of the item when buying it.")]
    [SerializeField] private int cost = 1;
    #endregion

    #region Properties
    /// <summary>
    /// The name of this weapon.
    /// </summary>
    public string WeaponName
    {
        get
        {
            return weaponName;
        }
    }
    /// <summary>
    /// The slot type.
    /// </summary>
    public WeaponTypes WType
    {
        get
        {
            return wType;
        }
    }
    /// <summary>
    /// The range of the bullitts fired.
    /// </summary>
    public float Range
    {
        get
        {
            return range;
        }
    }
    /// <summary>
    /// The firing mode.
    /// </summary>
    public FiringMode WeaponFiringMode
    {
        get
        {
            return firingMode;
        }
    }
    /// <summary>
    /// Is this weapon's clip empty?
    /// </summary>
    public bool IsEmpty
    {
        get
        {
            return currClipCount == 0;
        }
    }
    /// <summary>
    /// Is this weapon's clip not full?
    /// </summary>
    public bool IsNotFull
    {
        get
        {
            return currClipCount < clipSize;
        }
    }
    /// <summary>
    /// The user of this gun.
    /// </summary>
    public GameObject User
    {
        get
        {
            return user;
        }
    }

    /// <summary>
    /// The name of this gun.
    /// </summary>
    public string Name
    {
        get
        {
            return WeaponName;
        }
    }
    /// <summary>
    /// The description of this gun.
    /// </summary>
    public string Description
    {
        get
        {
            string desc = "";

            desc += "Weapon Type: " + WType.ToString();
            desc += "\nFiring Mode: " + firingMode.ToString();
            desc += "\nClip Size: " + clipSize;
            desc += "\nFiring Rate: " + fireingRate;
            desc += "\nRange: " + range;
            desc += "\nDamage: " + damage;

            return desc;
        }
    }
    /// <summary>
    /// An icon for this gun.
    /// </summary>
    public Texture Icon
    {
        get
        {
            return icon;
        }
    }
    /// <summary>
    /// Ths cost of this gun.
    /// </summary>
    public int Cost
    {
        get
        {
            return cost;
        }
    }
    #endregion

    #region Private
    private GameObject user = null;
    private Rigidbody rigidbody = null;
    private Collider collider = null;

    private Transform bullittSpawnpoint = null;
    private float lastShot = 0;
    private int groupShotCount = 0;
    private bool isBurstFiring = false;
    private bool isReloading = false;

    private int currClipCount = 0;
    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// Trigger was just pulled from not pulled.
    /// </summary>
    public void FireStart()
    {
        if (!isReloading && currClipCount > 0 && !isBurstFiring)
        {
            if (firingMode == FiringMode.Burst) isBurstFiring = true;
            groupShotCount = 0;
            SingleShot();
        }
    }
    /// <summary>
    /// The trigger is being held down and not been released yet.
    /// </summary>
    public void Firing()
    {
        if (firingMode == FiringMode.Auto) SingleShot();
    }
    /// <summary>
    /// The trigger was released.
    /// </summary>
    public void FireEnd()
    {
        
    }

    /// <summary>
    /// Start the process of reloading this weapon's ammo.
    /// </summary>
    public void Reload()
    {
        if (!isReloading && currClipCount < clipSize)
        {
            isReloading = true;
            Invoke("FinishReload", reloadTime);
        }
    }
    /// <summary>
    /// Cancel the current reload if one is in progress.
    /// </summary>
    public void CancelReload()
    {
        if (isReloading)
        {
            StopBurstFire();
            CancelInvoke("FinishReload");
            isReloading = false;
        }
    }

    /// <summary>
    /// This gun was picked up.
    /// </summary>
    public void PickedUp(GameObject newUser)
    {
        collider.enabled = false;
        rigidbody.isKinematic = true;
        user = newUser;
    }
    /// <summary>
    /// This gun is being dropped.
    /// </summary>
    public void Drop()
    {
        CancelReload();

        collider.enabled = true;
        rigidbody.isKinematic = false;
        user = null;
    }

    /// <summary>
    /// The use action for this gun is to pick it up.
    /// </summary>
    /// <param name="playerUsing">The player that is picking this weapon up.</param>
    /// <returns>true if successfully used.</returns>
    public bool Use(GameObject playerUsing)
    {
        IWeaponManager playerWeaponManger = playerUsing.GetComponent<IWeaponManager>();
        if (playerWeaponManger != null)
        {
            if (playerWeaponManger.PickUpWeapon(this))
            {
                PickedUp(playerUsing);
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Was just instantiated by the object pool manager.
    /// </summary>
    public void OnInstantiatedByObjectPool()
    {
        
    }
    /// <summary>
    /// Was just destroyed by the object pool manager.
    /// Get the user of this weapon to drop it if there is one.
    /// </summary>
    public void OnDestroyedByObjectPool()
    {
        user.GetComponent<IWeaponManager>().DropWeapon(WType);

        if (user != null) Drop();
    }

    #region Private
    /// <summary>
    /// Stop burst firing.
    /// </summary>
    private void StopBurstFire()
    {
        if(isBurstFiring) isBurstFiring = false;
    }
    /// <summary>
    /// Fires one shot if one is available in clip.
    /// If burst firing mode is set then it will call itself until finished all shots.
    /// </summary>
    private void SingleShot()
    {
        if (!isReloading && currClipCount > 0 && (firingMode == FiringMode.Burst || Time.time - lastShot >= fireingRate))
        {
            IBullitt spawnedBullitt = ObjectPoolManagerBase.SINGLETON.Instantiate(bullitt.gameObject, bullittSpawnpoint.position, bullittSpawnpoint.rotation).GetComponent<IBullitt>();
            spawnedBullitt.OnSpawn(user, gameObject, range, damage);

            if (currClipCount > 0)
            {
                currClipCount--;
                UpdateAmmoUI();
            }

            groupShotCount++;
            if (firingMode == FiringMode.Burst && isBurstFiring)
            {
                if (groupShotCount < burstShotCount) Invoke("SingleShot", fireingRate);
                else if (!IsInvoking("StopBurstFire")) Invoke("StopBurstFire", burstFireDelay);
            }
            else if(isBurstFiring) StopBurstFire();

            lastShot = Time.time;
        }
    }

    /// <summary>
    /// Reload the clip and make the gun fireable again.
    /// </summary>
    private void FinishReload()
    {
        if (isReloading && currClipCount < clipSize)
        {
            currClipCount = clipSize;
            isReloading = false;
        }

        UpdateAmmoUI();
    }

    /// <summary>
    /// Update the text for the UI with the current ammo.
    /// </summary>
    private void UpdateAmmoUI()
    {
        if(showAmmoAsPercentage)
        {
            if (ammoText != null) ammoText.text = ((float)currClipCount / (float)clipSize * 100f).ToString("F0");
            if (maxAmmoText != null) maxAmmoText.text = "100";
        }
        else
        {
            if (ammoText != null) ammoText.text = currClipCount.ToString();
            if (maxAmmoText != null) maxAmmoText.text = clipSize.ToString();
        }
    }

    /// <summary>
    /// Initial Setup.
    /// </summary>
    private void Setup()
    {
        rigidbody = GetComponent<Rigidbody>();
        if (rigidbody == null) Debug.LogError("No '" + rigidbody.GetType() + "' found!");
        collider = GetComponent<Collider>();
        if (rigidbody == null) Debug.LogError("No '" + collider.GetType() + "' found!");

        if (bullitt == null) Debug.LogError("No 'bullitt' set!");

        bullittSpawnpoint = ParentChildUtility.FindChildByName(transform, "BullittSpawnpoint");
        if (bullittSpawnpoint == null) Debug.LogError("No 'BullittSpawnpoint' found!");

        currClipCount = clipSize;
        UpdateAmmoUI();
        if (burstShotCount < 2) burstShotCount = 2;
    }
    #endregion
    #endregion

    #region Unity Functions
    private void Awake()
    {
        Setup();
    }
    #endregion
}

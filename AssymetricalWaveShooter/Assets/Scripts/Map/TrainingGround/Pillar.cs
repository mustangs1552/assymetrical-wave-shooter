﻿using UnityEngine;

/// <summary>
/// A pillar controlled by the PillarManager.
/// Can get information about this pillar.
/// </summary>
public class Pillar : MonoBehaviour
{
    #region Variables
    #region Public

    #endregion

    #region Properties
    /// <summary>
    /// The movement speed of the pillar when raising and lowering.
    /// </summary>
    public float Speed
    {
        get
        {
            return speed;
        }
        set
        {
            if (value > 0) speed = value;
        }
    }
    /// <summary>
    /// The height index of the pillar.
    /// Will not take affect until pillars are risen or lowered.
    /// </summary>
    public byte HeightIndex { get; set; } = 0;
    /// <summary>
    /// Units on the y-axis for each height index level.
    /// </summary>
    public float HeightPerIndex
    {
        get
        {
            return heightPerIndex;
        }
        set
        {
            if (value > 0)
            {

                heightPerIndex = value;
                UpdateTransform();
            }
        }
    }
    /// <summary>
    /// The x and y of the pillar.
    /// </summary>
    public Vector2 LengthWidth
    {
        get
        {
            return lengthWidth;
        }
        set
        {
            if (value.x > 0 && value.y > 0)
            {
                lengthWidth = value;
                UpdateTransform();
            }
        }
    }

    /// <summary>
    /// Is this pillar representing a POI on the POI map?
    /// </summary>
    public bool IsPOI { get; set; } = false;
    /// <summary>
    /// Information about the POI this pillar is representing.
    /// </summary>
    public POIPair POIInfo
    {
        get
        {
            return poiInfo;
        }
        set
        {
            poiInfo = value;
        }
    }
    /// <summary>
    /// The position of the top surface of this pillar.
    /// </summary>
    public Vector3 TopSurface
    {
        get
        {
            return transform.position + (Vector3.up * transform.localScale.y / 2);
        }
    }

    /// <summary>
    /// Is this pillar raising or lowering?
    /// </summary>
    public bool IsMoving
    {
        get
        {
            return isMovingUp || isMovingDown;
        }
    }
    #endregion

    #region Private
    private POIPair poiInfo = null;
    private float speed = 1;
    private float heightPerIndex = 1;
    private Vector2 lengthWidth = new Vector2(1, 1);

    private float floorHeight = 0;

    private float targetHeight = 0;
    private float startHeight = 0;
    private bool isMovingUp = false;
    private bool isMovingDown = false;
    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// Raise this pillar to the pre-set height index with animation.
    /// </summary>
    /// <param name="animate">Animate the pillar's movement?</param>
    public void StartRaising(bool animate = true)
    {
        startHeight = transform.position.y;
        targetHeight = floorHeight + HeightIndex * heightPerIndex;

        if (animate)
        {
            isMovingUp = true;
            isMovingDown = false;
        }
        else transform.position = new Vector3(transform.position.x, targetHeight, transform.position.z);
    }
    /// <summary>
    /// Lower this pillar to the floor height index with animation.
    /// </summary>
    /// <param name="animate">Animate the pillar's movement?</param>
    public void StartLowering(bool animate = true)
    {
        startHeight = transform.position.y;
        targetHeight = floorHeight;

        if (animate)
        {
            isMovingUp = false;
            isMovingDown = true;
        }
        else transform.position = new Vector3(transform.position.x, floorHeight, transform.position.z);
    }

    /// <summary>
    /// Update the transform of this pillar.
    /// </summary>
    public void UpdateTransform()
    {
        // Stretch our full height and make sure we are the proper x and z
        transform.localScale = new Vector3(lengthWidth.x, heightPerIndex * 255, lengthWidth.y);
        // Set the lowest position
        floorHeight = transform.position.y - transform.localScale.y / 2;
    }

    /// <summary>
    /// Move the pillar in the required direction via isMoving flags.
    /// </summary>
    private void Move()
    {
        if (isMovingUp)
        {
            transform.Translate(transform.up * speed * Time.deltaTime);
            if (transform.position.y >= targetHeight)
            {
                transform.position = new Vector3(transform.position.x, targetHeight, transform.position.z);
                isMovingUp = false;
            }
        }
        else if (isMovingDown)
        {
            transform.Translate(-transform.up * speed * Time.deltaTime);
            if (transform.position.y <= targetHeight)
            {
                transform.position = new Vector3(transform.position.x, targetHeight, transform.position.z);
                isMovingDown = false;
            }
        }
    }
    #endregion

    #region Unity Functions
    private void Update()
    {
        Move();
    }
    #endregion
}

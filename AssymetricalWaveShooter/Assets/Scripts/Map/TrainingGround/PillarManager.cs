﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The class that manages all the pillars in the room.
/// Applies a heightmap with an optional POI map to each of the pillars.
/// </summary>
public class PillarManager : MonoBehaviour, IPillarManager
{
    #region Variables
    #region Public
    [Tooltip("The pillar prefab.")]
    [SerializeField] private GameObject pillarPrefab = null;

    [Tooltip("The movement speed of the pillars.")]
    [SerializeField] private float pillarSpeed = 1;
    [Tooltip("The amount along the y-axis for each height index (0-255) by the grayscale pixel they represent.")] // Fixed: (Must also currently be set to pillar prefab manually (pillarHeightIndex * 255 = pillarHeight.y))
    [SerializeField] private float pillarHeightPerIndex = .05f;
    [Tooltip("The length and width of the the pillars.")]
    [SerializeField] private Vector2 pillarLengthWidth = new Vector2(1, 1);

    [Tooltip("Apply the POI's color to the corresponding pillars.")]
    [SerializeField] private bool applyPOIColor = false;
    #endregion

    #region Properties
    /// <summary>
    /// Gets all the POI locations.
    /// </summary>
    public Dictionary<string, List<Vector3>> POILocations
    {
        get
        {
            Dictionary<string, List<Vector3>> spawnpointsList = new Dictionary<string, List<Vector3>>();

            foreach (Pillar pillar in pillars)
            {
                if (pillar != null)
                {
                    if (pillar.IsPOI)
                    {
                        if (!spawnpointsList.ContainsKey(pillar.POIInfo.POIName)) spawnpointsList.Add(pillar.POIInfo.POIName, new List<Vector3>());
                        spawnpointsList[pillar.POIInfo.POIName].Add(pillar.TopSurface);
                    }
                }
            }

            return spawnpointsList;
        }
    }
    /// <summary>
    /// Gets all the POI pillars.
    /// </summary>
    public Dictionary<string, List<Pillar>> POIPillars
    {
        get
        {
            Dictionary<string, List<Pillar>> spawnpointsList = new Dictionary<string, List<Pillar>>();

            foreach (Pillar pillar in pillars)
            {
                if (pillar != null)
                {
                    if (pillar.IsPOI)
                    {
                        if (!spawnpointsList.ContainsKey(pillar.POIInfo.POIName)) spawnpointsList.Add(pillar.POIInfo.POIName, new List<Pillar>());
                        spawnpointsList[pillar.POIInfo.POIName].Add(pillar);
                    }
                }
            }

            return spawnpointsList;
        }
    }
    /// <summary>
    /// Checks to see if pillars are moving.
    /// </summary>
    public bool ArePillarsMoving
    {
        get
        {
            foreach (Pillar pillar in pillars)
            {
                if (pillar != null)
                {
                    if (pillar.IsMoving) return true;
                }
            }

            return false;
        }
    }
    #endregion

    #region Private
    private Transform pillarAnchor = null;
    private List<Pillar> pillars = new List<Pillar>();
    private Transform unusedPillarAnchor = null;
    private List<Pillar> unUsedPillars = new List<Pillar>();

    private PMHeightmap currHeightmap = null;

    private Color pillarsStartingColor = new Color();
    private bool startingColorSet = false;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Apply the given heightmap with POI map to the pillars.
    /// </summary>
    /// <param name="heightmap">The object that has the heightmap and spawnmap.</param>
    /// <param name="updateSize">Allow update of the floor resolution by respawning the pillars if given heightmap resolution doesn't match current floor resolution.</param>
    public void ApplyHeightmap(PMHeightmap heightmap, bool updateSize = false)
    {
        if (heightmap != null && heightmap.Heightmap != null)
        {
            if (currHeightmap == null || (updateSize && (heightmap.Heightmap.width != currHeightmap.Heightmap.width || heightmap.Heightmap.height != currHeightmap.Heightmap.height))) SpawnPillars(heightmap.Heightmap.width, heightmap.Heightmap.height);
            else if (heightmap.Heightmap.width != currHeightmap.Heightmap.width || heightmap.Heightmap.height != currHeightmap.Heightmap.height) return;

            currHeightmap = heightmap;
            Texture2D heightmap2D = (Texture2D)currHeightmap.Heightmap;

            // Pillars are created by width(x) then rows(y), incrementing both positive directions
            float currPixelGrayscale = 0;
            int currPillarI = 0;
            for (int i = 0; i < currHeightmap.Heightmap.height; i++)
            {
                for (int ii = 0; ii < currHeightmap.Heightmap.width; ii++)
                {
                    // Convert pixels to grayscale
                    currPixelGrayscale = heightmap2D.GetPixel(ii, i).grayscale;

                    // Grayscale is a percentage from 0-1, need inverse of 255 (255 pillar height index represents black)
                    Pillar currPillarComp = pillars[currPillarI].GetComponent<Pillar>();
                    currPillarComp.HeightIndex = (byte)Mathf.Abs((currPixelGrayscale * 255f) - 255f);

                    // Mark this pillar as a POI and save the POI information if it is on the POI map
                    Texture2D poiMap2D = (currHeightmap.POIMap != null) ? (Texture2D)currHeightmap.POIMap : null;
                    if (poiMap2D != null && poiMap2D.width == heightmap2D.width && poiMap2D.height == heightmap2D.height && poiMap2D.GetPixel(ii, i).a != 0)
                    {
                        currPillarComp.IsPOI = true;
                        foreach (POIPair poi in heightmap.POIs)
                        {
                            if(poi.POIColor == poiMap2D.GetPixel(ii, i)) currPillarComp.POIInfo = new POIPair(poi.POIName, poiMap2D.GetPixel(ii, i));
                        }

                        if (applyPOIColor)
                        {
                            if (!startingColorSet)
                            {
                                pillarsStartingColor = currPillarComp.GetComponent<MeshRenderer>().material.color;
                                startingColorSet = true;
                            }
                            currPillarComp.GetComponent<MeshRenderer>().material.color = poiMap2D.GetPixel(ii, i);
                        }
                    }
                    else
                    {
                        currPillarComp.IsPOI = false;
                        if (applyPOIColor && startingColorSet) currPillarComp.GetComponent<MeshRenderer>().material.color = pillarsStartingColor;
                    }

                    currPillarI++;
                }
            }
        }
    }

    /// <summary>
    /// Raise all the pillars to thier height index.
    /// </summary>
    /// <param name="animate">Animate the pillars' movement?</param>
    public void RaisePillars(bool animate = true)
    {
        foreach (Pillar pillar in pillars) pillar.StartRaising(animate);
    }
    /// <summary>
    /// Lower all the pillars to 0 height index.
    /// </summary>
    /// <param name="animate">Animate the pillars' movement?</param>
    public void LowerPillars(bool animate = true)
    {
        foreach (Pillar pillar in pillars) pillar.StartLowering(animate);
    }
    #endregion

    #region Private
    /// <summary>
    /// Initial setup.
    /// </summary>
    private void SetUp()
    {
        if (pillarPrefab == null) Debug.LogError("No 'pillarPrefab' set!");
    }

    /// <summary>
    /// Spawns a group of pillars with the given length and width being pillar count.
    /// Creates width then moves to next row(length), both positive directions.
    /// </summary>
    /// <param name="length">How many pillars along z.</param>
    /// <param name="width">How many pillars along x. </param>
    private void SpawnPillars(int length, int width)
    {
        if (pillarPrefab != null)
        {
            if (length < 1 || width < 1) return;

            // Remove previesly used pillars.
            if (pillars.Count > 0) RemovePillars();

            if (pillarAnchor == null)
            {
                pillarAnchor = new GameObject("PILLAR_ANCHOR").transform;
                pillarAnchor.SetParent(transform);
            }

            // Add pillars starting along the x and then move on to the next row along z
            Vector3 floorPos = new Vector3(transform.position.x + pillarPrefab.transform.localScale.x / 2, transform.position.y, transform.position.z + pillarPrefab.transform.localScale.z / 2);
            Transform currRowAnchor = null;
            Transform currPillar = null;
            Pillar currPillarComp = null;
            for (int i = 0; i < length; i++)
            {
                // Re-use same row anchors from last time if any
                if (pillarAnchor.childCount > 0 && pillarAnchor.childCount > i) currRowAnchor = pillarAnchor.GetChild(i);
                else
                {
                    currRowAnchor = new GameObject("ROW_" + i).transform;
                    currRowAnchor.SetParent(pillarAnchor);
                }

                for (int ii = 0; ii < width; ii++)
                {
                    // Re-use pillars if any
                    if (unUsedPillars.Count > 0)
                    {
                        currPillar = ReUsePillar();
                        currPillar.transform.position = floorPos;
                    }
                    else currPillar = Instantiate(pillarPrefab, floorPos, Quaternion.identity).transform;

                    // Position pillar and append " row-pillar" to the name
                    if (currPillar.GetComponent<Pillar>() == null) currPillarComp = currPillar.gameObject.AddComponent<Pillar>();
                    else currPillarComp = currPillar.GetComponent<Pillar>();

                    currPillarComp.Speed = pillarSpeed;
                    currPillarComp.HeightPerIndex = pillarHeightPerIndex;
                    currPillarComp.LengthWidth = pillarLengthWidth;
                    currPillarComp.HeightIndex = 0;
                    float pillarPosX = currPillar.localScale.x * ii;
                    float pillarPosZ = currPillar.localScale.z * i;
                    currPillar.Translate(new Vector3(pillarPosX, currPillar.position.y, pillarPosZ));
                    currPillar.name += " " + i + "-" + ii;
                    currPillar.SetParent(currRowAnchor);
                    pillars.Add(currPillarComp);
                }
            }

            LowerPillars(false);
        }
    }

    /// <summary>
    /// Re-use a pillar that is not in use.
    /// </summary>
    /// <returns>The pillar to be re-used.</returns>
    private Transform ReUsePillar()
    {
        // Re-use last pillar
        Pillar pillar = unUsedPillars[unUsedPillars.Count - 1];
        unUsedPillars.Remove(pillar);
        pillar.gameObject.SetActive(true);

        // Reset name
        string[] currName = pillar.name.Split(' ');
        pillar.name = "";
        for (int i = 0; i < currName.Length - 1; i++) pillar.name += currName[i];

        return pillar.transform;
    }
    /// <summary>
    /// Remove all pillars that are currently in use and adds them to the un-used pillars list.
    /// </summary>
    private void RemovePillars()
    {
        if (unusedPillarAnchor == null)
        {
            unusedPillarAnchor = new GameObject("UNUSED_PILLAR_ANCHOR").transform;
            unusedPillarAnchor.SetParent(transform);
        }

        // Add the pillars to the un-used pillars list and clear the currently used pillars list
        foreach (Pillar pillar in pillars)
        {
            unUsedPillars.Add(pillar);
            pillar.transform.SetParent(unusedPillarAnchor);
            pillar.gameObject.SetActive(false);
        }
        pillars = new List<Pillar>();
    }
    #endregion
    #endregion

    #region Unity Functions
    private void Awake()
    {
        SetUp();
    }
    #endregion
}

/// <summary>
/// Contains the information of a POI on the coresponding POI map.
/// </summary>
[Serializable]
public class POIPair
{
    [Tooltip("The name of the POI.")]
    [SerializeField] private string poiName = "";
    [Tooltip("The exact color of the pixel in the 'poiMap' that represent this POI.")]
    [SerializeField] private Color poiColor = Color.white;

    public string POIName
    {
        get
        {
            return poiName;
        }
    }
    public Color POIColor
    {
        get
        {
            return poiColor;
        }
    }

    public POIPair(string name, Color color)
    {
        poiName = name;
        poiColor = color;
    }
}
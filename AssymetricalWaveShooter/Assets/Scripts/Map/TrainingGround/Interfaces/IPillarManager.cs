﻿using System;
using System.Collections.Generic;
using UnityEngine;

interface IPillarManager
{
    Dictionary<string, List<Vector3>> POILocations { get; }
    Dictionary<string, List<Pillar>> POIPillars { get; }
    bool ArePillarsMoving { get; }

    void ApplyHeightmap(PMHeightmap heightmap, bool updateSize = false);
    void RaisePillars(bool animate = true);
    void LowerPillars(bool animate = true);
}

/// <summary>
/// A helper class intended to store the information about a PillarManager Heightmap.
/// </summary>
[Serializable]
public class PMHeightmap
{
    [Tooltip("The heightmap of the pillars. Each pixel represents one pillar.")]
    [SerializeField] private Texture heightmap = null;
    [Tooltip("The a transparent image with colored pixels representing POI's for the 'heightmap'. POI pixels' alphas must be greater than 0.")]
    [SerializeField] private Texture poiMap = null;
    [Tooltip("The POIs that exist in the 'poiMap'.")]
    [SerializeField] private List<POIPair> pois = new List<POIPair>();

    public Texture Heightmap
    {
        get
        {
            return heightmap;
        }
        set
        {
            heightmap = (heightmap.width == value.width && heightmap.height == value.height) ? value : heightmap;
        }
    }
    public Texture POIMap
    {
        get
        {
            return poiMap;
        }
        set
        {
            poiMap = (poiMap.width == value.width && poiMap.height == value.height) ? value : poiMap;
        }
    }
    public List<POIPair> POIs
    {
        get
        {
            return pois;
        }
    }
}
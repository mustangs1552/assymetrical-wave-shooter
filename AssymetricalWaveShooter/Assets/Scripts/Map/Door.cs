﻿using UnityEngine;

/// <summary>
/// A simple double door.
/// Can open horizontally with both sides moving away from eachother to open or vertically with both sides moving down to open.
/// </summary>
public class Door : MonoBehaviour
{
    #region Variables
    #region Public
    [Tooltip("Open vertically or horizontally?")]
    [SerializeField] private bool opensVertically = false;
    [Tooltip("The left door looking down the positive z-axis.")]
    [SerializeField] private Transform leftDoor = null;
    [Tooltip("The right door looking down the positive z-axis.")]
    [SerializeField] private Transform rightDoor = null;
    [Tooltip("The speed the door moves.")]
    [SerializeField] private float speed = 1;
    #endregion

    #region Properties
    /// <summary>
    /// Is the door open?
    /// </summary>
    public bool IsOpen
    {
        get
        {
            return isOpen;
        }
    }
    /// <summary>
    /// Is the door closed?
    /// </summary>
    public bool IsClosed
    {
        get
        {
            return isClosed;
        }
    }

    /// <summary>
    /// Is the door opening?
    /// </summary>
    public bool IsOpening
    {
        get
        {
            return isOpening;
        }
    }
    /// <summary>
    /// Is the door closing?
    /// </summary>
    public bool IsClosing
    {
        get
        {
            return isClosing;
        }
    }
    /// <summary>
    /// Is the doors moving?
    /// </summary>
    public bool IsMoving
    {
        get
        {
            return isOpening || isClosing;
        }
    }
    #endregion

    #region Private
    private bool isOpening = false;
    private bool isClosing = false;
    private Vector3 leftDoorStartPos = Vector3.zero;
    private Vector3 rightDoorStartPos = Vector3.zero;
    private Vector3 leftDoorEndPos = Vector3.zero;
    private Vector3 rightDoorEndPos = Vector3.zero;

    private bool isOpen = false;
    private bool isClosed = false;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Start opening the door.
    /// </summary>
    public void Open()
    {
        if (!isOpening)
        {
            isOpening = true;
            isClosing = false;

            isOpen = false;
            isClosed = false;
        }
    }
    /// <summary>
    /// Start closing the door.
    /// </summary>
    public void Close()
    {
        if (!isClosing)
        {
            isClosing = true;
            isOpening = false;

            isOpen = false;
            isClosed = false;
        }
    }
    #endregion

    #region Private
    /// <summary>
    /// Continue animating the door opening.
    /// </summary>
    private void ContinueOpening()
    {
        if (leftDoor != null)
        {
            if (opensVertically)
            {
                leftDoor.Translate(Vector3.down * speed * Time.deltaTime);
                if (leftDoor.position.y < leftDoorEndPos.y)
                {
                    leftDoor.position = leftDoorEndPos;
                    isOpening = false;
                    isOpen = true;
                }
            }
            else
            {
                leftDoor.Translate(Vector3.left * speed * Time.deltaTime);
                if (leftDoor.position.x < leftDoorEndPos.x)
                {
                    leftDoor.position = leftDoorEndPos;
                    isOpening = false;
                    isOpen = true;
                }
            }
        }
        if (rightDoor != null)
        {
            if (opensVertically)
            {
                rightDoor.Translate(Vector3.down * speed * Time.deltaTime);
                if (rightDoor.position.y < rightDoorEndPos.y)
                {
                    rightDoor.position = rightDoorEndPos;
                    isOpening = false;
                    isOpen = true;
                }
            }
            else
            {
                rightDoor.Translate(Vector3.right * speed * Time.deltaTime);
                if (rightDoor.position.x > rightDoorEndPos.x)
                {
                    rightDoor.position = rightDoorEndPos;
                    isOpening = false;
                    isOpen = true;
                }
            }
        }
    }
    /// <summary>
    /// Continue animating the door closing.
    /// </summary>
    private void ContinueClosing()
    {
        if (leftDoor != null)
        {
            if (opensVertically)
            {
                leftDoor.Translate(Vector3.up * speed * Time.deltaTime);
                if (leftDoor.position.y > leftDoorStartPos.y)
                {
                    leftDoor.position = leftDoorStartPos;
                    isClosing = false;
                    isClosed = true;
                }
            }
            else
            {
                leftDoor.Translate(Vector3.right * speed * Time.deltaTime);
                if (leftDoor.position.x > leftDoorStartPos.x)
                {
                    leftDoor.position = leftDoorStartPos;
                    isClosing = false;
                    isClosed = true;
                }
            }
        }
        if (rightDoor != null)
        {
            if (opensVertically)
            {
                rightDoor.Translate(Vector3.up * speed * Time.deltaTime);
                if (rightDoor.position.y > rightDoorStartPos.y)
                {
                    rightDoor.position = rightDoorStartPos;
                    isClosing = false;
                    isClosed = true;
                }
            }
            else
            {
                rightDoor.Translate(Vector3.left * speed * Time.deltaTime);
                if (rightDoor.position.x < rightDoorStartPos.x)
                {
                    rightDoor.position = rightDoorStartPos;
                    isClosing = false;
                    isClosed = true;
                }
            }
        }
    }
    /// <summary>
    /// Continue opeing or closing the door if either is happening.
    /// </summary>
    private void ContinueMoving()
    {
        if (isOpening) ContinueOpening();
        else if (isClosing) ContinueClosing();
    }

    /// <summary>
    /// Initial setup.
    /// </summary>
    private void Setup()
    {
        if (leftDoor == null && rightDoor == null) Debug.LogError("No doors set!");
        else
        {
            if (leftDoor != null)
            {
                leftDoorStartPos = leftDoor.position;
                leftDoorEndPos = (opensVertically) ? leftDoorStartPos + Vector3.down * leftDoor.localScale.y : leftDoorStartPos + Vector3.left * leftDoor.localScale.x;
            }
            if (rightDoor != null)
            {
                rightDoorStartPos = rightDoor.position;
                rightDoorEndPos = (opensVertically) ? rightDoorStartPos + Vector3.down * rightDoor.localScale.y : rightDoorStartPos + Vector3.right * rightDoor.localScale.x;
            }
        }

        Close();
    }
    #endregion
    #endregion

    #region Unity Functions
    private void Awake()
    {
        Setup();
    }
    private void Update()
    {
        ContinueMoving();
    }
    #endregion
}

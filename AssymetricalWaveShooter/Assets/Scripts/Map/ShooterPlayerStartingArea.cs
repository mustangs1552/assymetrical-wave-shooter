﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is a trigger object that detects when a player has reached a starting spot.
/// </summary>
public class ShooterPlayerStartingArea : MonoBehaviour
{
    #region Variables
    #region Public

    #endregion

    #region Properties

    #endregion

    #region Private
    private List<IShooterControls> playersPaused = new List<IShooterControls>();
    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// Resume teh players' movement that was paused by this trigger.
    /// </summary>
    public void ResumePlayers()
    {
        foreach (IShooterControls player in playersPaused) player.ResumeMovement();
        playersPaused = new List<IShooterControls>();
    }
    #endregion

    #region Unity Functions
    private void OnTriggerEnter(Collider other)
    {
        IShooterControls shooterPlayer = (other.gameObject.GetComponent<AIBrain>() == null) ? other.gameObject.GetComponentInChildren<IShooterControls>() : null;
        if (shooterPlayer != null)
        {
            RoundControllerBase.SINGLETON.ShooterPlayerInPosition();
            shooterPlayer.PauseMovement();
            playersPaused.Add(shooterPlayer);
        }
    }
    #endregion
}

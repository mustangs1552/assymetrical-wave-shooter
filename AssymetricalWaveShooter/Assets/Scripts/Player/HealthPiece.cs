﻿using UnityEngine;

/// <summary>
/// Works with a Health class as a seperate point where damage can be taken and adjusted in addition.
/// This class has no health.
/// </summary>
public class HealthPiece : MonoBehaviour, IHealthPiece, ITakesDamage
{
    #region Variables
    #region Public
    [Tooltip("Starting multiplier applied to all incoming damage.")]
    [SerializeField] private float damageMultiplier = 1;
    #endregion

    #region Properties

    #endregion

    #region Private
    private ITakesDamage health = null;
    private float currDamageMultiplier = 0;
    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// Modifies and passes the damage onto the user's main health.
    /// </summary>
    /// <param name="amount">Amount to be dealt.</param>
    /// <param name="source">The object that represents the source of the damage.</param>
    /// <returns>True if successful.</returns>
    public bool TakeDamage(float amount, GameObject source)
    {
        if (amount > 0)
        {
            health.TakeDamage(amount * currDamageMultiplier, source);
            return true;
        }

        return false;
    }
    /// <summary>
    /// Passes the amount of health to be healed to the user's main health.
    /// </summary>
    /// <param name="amount">The amount of health to be healed.</param>
    /// <param name="source">The object that represents the source of the health.</param>
    /// <returns>True if successful.</returns>
    public bool Heal(float amount, GameObject source)
    {
        if (amount > 0)
        {
            health.Heal(amount, source);
            return true;
        }

        return false;
    }

    /// <summary>
    /// Reset all the stats.
    /// </summary>
    public void ResetStats()
    {
        ResetCurrDamageMultiplier();
    }
    /// <summary>
    /// Reset the current damage multiplier.
    /// </summary>
    public void ResetCurrDamageMultiplier()
    {
        currDamageMultiplier = damageMultiplier;
    }

    /// <summary>
    /// Initial setup.
    /// </summary>
    /// <param name="mainHealth">The user's main health.</param>
    public void Setup(ITakesDamage mainHealth)
    {
        health = mainHealth;
        ResetStats();
    }
    #endregion

    #region Unity Functions

    #endregion
}

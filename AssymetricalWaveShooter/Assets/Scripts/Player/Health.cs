﻿using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Controllers;
using MattRGeorge.Utilities;

/// <summary>
/// The main health stat for an object or character that has health.
/// Can use seperate health pieces that are on other parts of the object/character that can have thier own added affect on damage taken.
/// </summary>
public class Health : MonoBehaviour, IHasHealth, ITakesDamage, IHealsPassively
{
    #region Variables
    #region Public
    [Tooltip("Starting max health.")]
    [SerializeField] private float maxHealth = 100;
    [Tooltip("Starting heal amount for passive heal ability (0 essentially disables).")]
    [SerializeField] private float passiveHealAmount = 1;
    [Tooltip("Starting rate at which passive heal triggers.")]
    [SerializeField] private float passiveHealRate = 1;
    [Tooltip("Starting multiplier applied to all incoming damage.")]
    [SerializeField] private float damageMultiplier = 1;
    [SerializeField] private Text healthText = null;

    [Tooltip("Time between health updates when dealing damage and healing.")]
    [SerializeField] private float healthUpdateCooldown = .1f;
    #endregion

    #region Properties
    /// <summary>
    /// Current health amount.
    /// </summary>
    public float CurrHealth
    {
        get
        {
            return currHealth;
        }
    }
    /// <summary>
    /// Current percentage of health.
    /// </summary>
    public float CurrHealthPerc
    {
        get
        {
            return CurrHealth / currMaxHealth;
        }
    }

    /// <summary>
    /// Is this object still alive?
    /// </summary>
    public bool IsAlive
    {
        get
        {
            return currHealth > 0;
        }
    }
    #endregion

    #region Private
    private float currMaxHealth = 0;
    private float currHealth = 0;
    private float currPassiveHealAmount = 0;
    private float currPassiveHealRate = 0;
    private float lastPassiveHeal = 0;
    private float currDamageMultiplier = 0;
    private float lastHealthUpdate = 0;

    private IHealthPiece[] healthPieces = null;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Applies the given amount of damage.
    /// </summary>
    /// <param name="amount">The amount of damage to be dealt.</param>
    /// <param name="source">The object that represents the source of the damage.</param>
    /// <returns>True if successful.</returns>
    public bool TakeDamage(float amount, GameObject source)
    {
        if(amount > 0 && Time.time - lastHealthUpdate >= healthUpdateCooldown && currHealth > 0)
        {
            bool successful = AdjustHealth(-amount * currDamageMultiplier);
            if (successful)
            {
                lastHealthUpdate = Time.time;
                if(source != null && !IsAlive)
                {
                    IWeapon sourceWeapon = source.GetComponent<IWeapon>();
                    if (sourceWeapon != null) UnityAnalyticsController.EnemyKilledPlayer(GameObjectUtilities.GetActualName(sourceWeapon.User));
                }
                return true;
            }
        }

        return false;
    }
    /// <summary>
    /// Applies the given amount of health to heal.
    /// </summary>
    /// <param name="amount">The amount of health to be healed.</param>
    /// <param name="source">The object that represents the source of the health.</param>
    /// <returns>True if successful.</returns>
    public bool Heal(float amount, GameObject source)
    {
        if (amount > 0 && currHealth < currMaxHealth && Time.time - lastHealthUpdate >= healthUpdateCooldown && currHealth < currMaxHealth)
        {
            bool successful = AdjustHealth(amount);
            if (successful)
            {
                lastHealthUpdate = Time.time;
                return true;
            }
        }

        return false;
    }
    /// <summary>
    /// Adjusts the health directly.
    /// </summary>
    /// <param name="amount">The amount to adjust by.</param>
    /// <returns>True if successful.</returns>
    public bool AdjustHealth(float amount)
    {
        if(amount != 0)
        {
            currHealth += amount;
            CheckValueBoundries();
            return true;
        }

        return false;
    }

    /// <summary>
    /// Resets current health to current max health.
    /// </summary>
    public void ResetCurrHealth()
    {
        currHealth = currMaxHealth;
        if (healthText != null) healthText.text = currHealth.ToString("F0");
    }
    /// <summary>
    /// Resets curr max health back to starting value.
    /// </summary>
    public void ResetCurrMaxHealth()
    {
        currMaxHealth = maxHealth;
        CheckValueBoundries();
    }
    /// <summary>
    /// Resets current passive heal amount to starting value.
    /// </summary>
    public void ResetCurrPassiveHealAmount()
    {
        currPassiveHealAmount = passiveHealAmount;
    }
    /// <summary>
    /// Resets current passive heal rate to starting value.
    /// </summary>
    public void ResetCurrPassiveHealRate()
    {
        currPassiveHealRate = passiveHealRate;
    }
    /// <summary>
    /// Resets current damage multiplier to starting value.
    /// </summary>
    public void ResetCurrDamageMultiplier()
    {
        currDamageMultiplier = damageMultiplier;
        foreach (HealthPiece piece in healthPieces) piece.ResetCurrDamageMultiplier();
    }
    /// <summary>
    /// Resets all health stats.
    /// </summary>
    public void ResetStats()
    {
        ResetCurrMaxHealth();
        ResetCurrHealth();
        ResetCurrPassiveHealAmount();
        ResetCurrPassiveHealRate();
        ResetCurrDamageMultiplier();

        foreach (HealthPiece hp in healthPieces) hp.ResetStats();
    }

    /// <summary>
    /// Reset the health and optionaly the rest of the stats to full.
    /// </summary>
    /// <param name="resetAllStats">Reset all stats other than just health.</param>
    public void Revive(bool resetAllStats)
    {
        if(currHealth == 0)
        {
            if (resetAllStats) ResetStats();
            else ResetCurrHealth();
        }
    }
    /// <summary>
    /// Set the health to 0.
    /// </summary>
    public void Kill()
    {
        currHealth = 0;
        CheckValueBoundries();
    }
    #endregion

    #region Private
    /// <summary>
    /// A pass of the passive heal ability.
    /// </summary>
    private void PassiveHeal()
    {
        if(currPassiveHealAmount > 0 && Time.time - lastPassiveHeal >= currPassiveHealRate)
        {
            if(currHealth < currMaxHealth)
            {
                currHealth += currPassiveHealAmount;
                CheckValueBoundries();
            }

            lastPassiveHeal = Time.time;
        }
    }

    /// <summary>
    /// Check the bounds of stats values.
    /// </summary>
    private void CheckValueBoundries()
    {
        if (currPassiveHealRate < 0) currPassiveHealRate = 0;

        if (currHealth <= 0)
        {
            currHealth = 0;
            Die();
        }
        else if (currHealth > currMaxHealth) currHealth = currMaxHealth;
        if(healthText != null) healthText.text = currHealth.ToString("F0");
    }

    /// <summary>
    /// Health reached 0.
    /// </summary>
    private void Die()
    {
        SendMessage("Died", SendMessageOptions.DontRequireReceiver);

        if(RoundControllerBase.SINGLETON != null) RoundControllerBase.SINGLETON.ShooterPlayerDefeated();
    }

    /// <summary>
    /// Initial setup.
    /// </summary>
    private void Setup()
    {
        healthPieces = GetComponentsInChildren<IHealthPiece>();
        foreach (HealthPiece hp in healthPieces) hp.Setup(this);
        ResetStats();
    }
    #endregion
    #endregion

    #region Unity Functions
    private void Awake()
    {
        Setup();
    }
    private void Update()
    {
        PassiveHeal();
    }
    #endregion
}

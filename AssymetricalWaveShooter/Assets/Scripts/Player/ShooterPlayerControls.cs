﻿using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using Assets.Scripts.Controllers;
using MattRGeorge.Utilities;

[RequireComponent(typeof(IWeaponManager))]
/// <summary>
/// Handles and manages all the input for the shooter player.
/// </summary>
public class ShooterPlayerControls : MonoBehaviour, IShooterControls
{
    #region Variables
    #region Public
    [Tooltip("The distance of the use action raycast.")]
    [SerializeField] private float useMaxRange = 1;
    [Tooltip("Draw gizmos for use action?")]
    [SerializeField] private bool drawUseGizmos = false;
    [Tooltip("The UI text for the various messages the player sees.")]
    [SerializeField] private Text messageText = null;

    [Tooltip("The controller for the player's movement.")]
    [SerializeField] private FirstPersonController controller = null;
    #endregion

    #region Properties
    
    #endregion

    #region Private
    private IWeaponManager weaponManager = null;

    private bool usePressed = false;
    private bool firePressed = false;
    private bool reloadPressed = false;
    private bool switchWeaponOnePressed = false;
    private bool switchWeaponTwoPressed = false;
    private bool switchWeaponUpPressed = false;
    private bool switchWeaponDownPressed = false;
    private bool switchWeaponLastPressed = false;

    private bool uiInRange = false;
    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// Pause the movement for this player.
    /// </summary>
    public void PauseMovement()
    {
        controller.IsMovementDisabled = true;
    }
    /// <summary>
    /// Resume the movement for this player.
    /// </summary>
    public void ResumeMovement()
    {
        controller.IsMovementDisabled = false;
    }
    
    /// <summary>
    /// Draws a raycast and calls Use() on the first object with IUsable.
    /// </summary>
    private void Use()
    {
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(new Ray(transform.position, transform.forward), out hit, useMaxRange, (int)LayerMask.GetMask("Usable")))
        {
            IUsable usableObj = hit.transform.GetComponent<IUsable>();
            if (usableObj != null)
            {
                if(usableObj.Use(gameObject)) UnityAnalyticsController.ItemUsed(GameObjectUtilities.GetActualName(hit.transform.gameObject));
            }
        }
    }

    /// <summary>
    /// Check the various requirements for showing a message to the user and show it as necessary.
    /// </summary>
    private void CheckMessageRequirements()
    {
        bool objInRange = Physics.Raycast(new Ray(transform.position, transform.forward), useMaxRange, (int)LayerMask.GetMask("Usable"));
        uiInRange = Physics.Raycast(new Ray(transform.position, transform.forward), useMaxRange, (int)LayerMask.GetMask("UI"));

        if (messageText != null)
        {
            if (!messageText.gameObject.activeInHierarchy)
            {
                if (objInRange) ShowMessage("Use the 'E' key to use.");
                else if (uiInRange)
                {
                    ShowMessage("Use 'Left Click' to interact.");
                    CheckUIControlsHover();
                }
            }
            else if (messageText.gameObject.activeInHierarchy && !objInRange && !uiInRange) messageText.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Set the desired message and show it.
    /// </summary>
    /// <param name="message">The desired message.</param>
    private void ShowMessage(string message)
    {
        messageText.text = message;
        messageText.gameObject.SetActive(true);
    }

    /// <summary>
    /// User is interacting with a UI element like a button.
    /// </summary>
    private void UIInteraction()
    {
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(new Ray(transform.position, transform.forward), out hit, useMaxRange, (int)LayerMask.GetMask("UI")))
        {
            Button button = hit.transform.GetComponent<Button>();
            if(button != null) button.onClick.Invoke();
        }
    }
    /// <summary>
    /// User is hovering over a UI element like a button.
    /// </summary>
    private void CheckUIControlsHover()
    {
        
    }

    /// <summary>
    /// Checks for and handles player input.
    /// </summary>
    private void CheckInput()
    {
        if (Input.GetAxis("Use") > 0) usePressed = true;
        else if (usePressed)
        {
            Use();
            usePressed = false;
        }

        if (Input.GetAxis("Fire1") > 0)
        {
            if (!firePressed)
            {
                if (!uiInRange && weaponManager.ActiveWeaponFireStart != null) weaponManager.ActiveWeaponFireStart();
                firePressed = true;
            }
            if (!uiInRange && weaponManager.ActiveWeaponFiring != null) weaponManager.ActiveWeaponFiring();
        }
        else if(firePressed)
        {
            if (!uiInRange && weaponManager.ActiveWeaponFireEnd != null) weaponManager.ActiveWeaponFireEnd();
            else if (uiInRange) UIInteraction();

            firePressed = false;
        }

        if (Input.GetAxis("Reload") > 0) reloadPressed = true;
        else if(reloadPressed)
        {
            if(weaponManager.ActiveWeaponReload != null) weaponManager.ActiveWeaponReload();
            reloadPressed = false;
        }

        if (Input.GetAxis("SwitchWeaponOne") > 0) weaponManager.ChangeWeapon(0);
        if (Input.GetAxis("SwitchWeaponTwo") > 0) weaponManager.ChangeWeapon(1);
        if (Input.GetAxis("SwitchWeaponWheel") > 0) weaponManager.ChangeWeaponUp();
        if (Input.GetAxis("SwitchWeaponWheel") < 0) weaponManager.ChangeWeaponDown();
    }

    /// <summary>
    /// Initial setup.
    /// </summary>
    private void Setup()
    {
        weaponManager = GetComponent<IWeaponManager>();

        if (controller == null) Debug.LogError("No 'FirstPersonController' set!");
    }
    #endregion

    #region Unity Functions
    private void Awake()
    {
        Setup();
    }
    private void Update()
    {
        CheckInput();
        CheckMessageRequirements();
    }

    private void OnDrawGizmos()
    {
        if (drawUseGizmos)
        {
            // Draw a ray showing range and origin of use raycast.
            Gizmos.color = Color.green;
            Gizmos.DrawRay(transform.position, transform.forward * useMaxRange);
        }
    }
    #endregion
}

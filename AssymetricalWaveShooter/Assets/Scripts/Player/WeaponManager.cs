﻿using System;
using UnityEngine;

/// <summary>
/// This manager handles the weapons.
/// It sits between the player controls and the weapons themselves.
/// </summary>
public class WeaponManager : MonoBehaviour, IWeaponManager
{
    #region Variables
    #region Public
    [Tooltip("The position the weapons go to when equiped.")]
    [SerializeField] private Transform weaponPos = null;
    #endregion

    #region Properties
    /// <summary>
    /// The currently selected weapon's fire start method.
    /// </summary>
    public Action ActiveWeaponFireStart
    {
        get
        {
            return StartFiringWeapon;
        }
    }
    /// <summary>
    /// The currently selected weapon's firing method.
    /// </summary>
    public Action ActiveWeaponFiring
    {
        get
        {
            return FiringWeapon;
        }
    }
    /// <summary>
    /// The currently selected weapon's fire end method.
    /// </summary>
    public Action ActiveWeaponFireEnd
    {
        get
        {
            return StopFiringWeapon;
        }
    }
    /// <summary>
    /// The currently selected weapon's reload method.
    /// </summary>
    public Action ActiveWeaponReload
    {
        get
        {
            return ReloadWeapon;
        }
    }
    #endregion

    #region Private
    private IWeapon activeWeapon = null;
    private IWeapon primaryWeapon = null;
    private IWeapon secondaryWeapon = null;

    private Action StartFiringWeapon = null;
    private Action FiringWeapon = null;
    private Action StopFiringWeapon = null;
    private Action ReloadWeapon = null;
    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// Pickup and place the given in the proper slot.
    /// Replace and drop any weapon that is already in the slot. 
    /// </summary>
    /// <param name="weapon">The weapon being picked up.</param>
    /// <returns>True if the weapon was successfully picked up.</returns>
    public bool PickUpWeapon(IWeapon weapon)
    {
        if (weapon != null)
        {
            ReplaceWeapon(weapon);
            ChangeWeapon(weapon.WType);

            return true;
        }

        return false;
    }
    /// <summary>
    /// Drop the currently active weapon.
    /// </summary>
    public void DropWeapon()
    {
        if(activeWeapon != null) DropWeapon(activeWeapon.WType);
    }
    /// <summary>
    /// Drop the weapon in the given slot.
    /// </summary>
    /// <param name="slot">The slot to drop the weapon from.</param>
    public void DropWeapon(WeaponTypes slot)
    {
        StartFiringWeapon = null;
        FiringWeapon = null;
        StopFiringWeapon = null;
        ReloadWeapon = null;
        if (slot == WeaponTypes.Primary && primaryWeapon != null)
        {
            primaryWeapon.Drop();
            primaryWeapon = null;
        }
        else if(secondaryWeapon != null)
        {
            secondaryWeapon.Drop();
            secondaryWeapon = null;
        }

        ToggleLastWeapon();
    }

    /// <summary>
    /// Change to the weapon in the next slot up.
    /// </summary>
    public void ChangeWeaponUp()
    {
        ToggleLastWeapon();
    }
    /// <summary>
    /// Change to the weapon in the next slot down.
    /// </summary>
    public void ChangeWeaponDown()
    {
        ToggleLastWeapon();
    }
    /// <summary>
    /// Change to the weapon that was last selected.
    /// </summary>
    public void ToggleLastWeapon()
    {
        if (activeWeapon != null)
        {
            if (activeWeapon.WType == WeaponTypes.Primary && secondaryWeapon != null) SetActiveWeapon(WeaponTypes.Secondary);
            else if (primaryWeapon != null) SetActiveWeapon(WeaponTypes.Primary);
        }
    }
    /// <summary>
    /// Change to the weapon slot given.
    /// </summary>
    public void ChangeWeapon(WeaponTypes slot)
    {
        SetActiveWeapon(slot);
    }
    /// <summary>
    /// Change to the weapon slot by the given number.
    /// </summary>
    public void ChangeWeapon(int slotNum)
    {
        if (slotNum == 0) SetActiveWeapon(WeaponTypes.Primary);
        else if (slotNum == 1) SetActiveWeapon(WeaponTypes.Secondary);
    }

    #region Private
    /// <summary>
    /// Replace the given weapon with whatever is in the coresponding slot.
    /// </summary>
    /// <param name="newWeapon">The new weapon.</param>
    private void ReplaceWeapon(IWeapon newWeapon)
    {
        if (newWeapon.WType == WeaponTypes.Primary) ReplacePrimaryWeapon(newWeapon);
        else ReplaceSecondaryWeapon(newWeapon);
    }
    /// <summary>
    /// Replace the weapon in the primary slot.
    /// </summary>
    /// <param name="newWeapon">The new primary weapon.</param>
    private void ReplacePrimaryWeapon(IWeapon newWeapon)
    {
        if (newWeapon != null && newWeapon.WType == WeaponTypes.Primary)
        {
            MonoBehaviour weaponMB = null;

            if (primaryWeapon == null || primaryWeapon.Equals(null)) primaryWeapon = newWeapon;
            else
            {
                weaponMB = (MonoBehaviour)primaryWeapon;
                weaponMB.transform.SetParent(null);
                primaryWeapon.Drop();
                MonoBehaviour newWeaponMB = (MonoBehaviour)newWeapon;
                weaponMB.transform.position = newWeaponMB.transform.position;
                weaponMB.transform.rotation = newWeaponMB.transform.rotation;
                weaponMB.gameObject.SetActive(true);
            }

            primaryWeapon = newWeapon;
            weaponMB = (MonoBehaviour)primaryWeapon;
            weaponMB.transform.position = weaponPos.position;
            weaponMB.transform.rotation = weaponPos.rotation;
            weaponMB.transform.SetParent(weaponPos);
            weaponMB.gameObject.SetActive(false);
        }
    }
    /// <summary>
    /// Replace the weapon in the secondary slot.
    /// </summary>
    /// <param name="newWeapon">The new secondary weapon.</param>
    private void ReplaceSecondaryWeapon(IWeapon newWeapon)
    {
        if (newWeapon != null && newWeapon.WType == WeaponTypes.Secondary)
        {
            MonoBehaviour weaponMB = null;

            if (secondaryWeapon == null || secondaryWeapon.Equals(null)) secondaryWeapon = newWeapon;
            else
            {
                weaponMB = (MonoBehaviour)secondaryWeapon;
                weaponMB.transform.SetParent(null);
                secondaryWeapon.Drop();
                MonoBehaviour newWeaponMB = (MonoBehaviour)newWeapon;
                weaponMB.transform.position = newWeaponMB.transform.position;
                weaponMB.transform.rotation = newWeaponMB.transform.rotation;
                weaponMB.gameObject.SetActive(true);
            }

            secondaryWeapon = newWeapon;
            weaponMB = (MonoBehaviour)secondaryWeapon;
            weaponMB.transform.position = weaponPos.position;
            weaponMB.transform.rotation = weaponPos.rotation;
            weaponMB.transform.SetParent(weaponPos);
            weaponMB.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Make the weapon in the selected slot the active weapon the player is using.
    /// </summary>
    /// <param name="slot">The slot with the desired weapon.</param>
    private void SetActiveWeapon(WeaponTypes slot)
    {
        IWeapon weapon = null;
        MonoBehaviour weaponMB = null;
        if (primaryWeapon != null && !primaryWeapon.Equals(null) && slot == WeaponTypes.Primary)
        {
            weapon = primaryWeapon;

            if (weapon != null && secondaryWeapon != null)
            {
                weaponMB = (MonoBehaviour)secondaryWeapon;
                if(weaponMB != null) weaponMB.gameObject.SetActive(false);
            }
        }
        else if(secondaryWeapon != null && !secondaryWeapon.Equals(null))
        {
            weapon = secondaryWeapon;

            if (weapon != null && primaryWeapon != null)
            {
                weaponMB = (MonoBehaviour)primaryWeapon;
                if (weaponMB != null) weaponMB.gameObject.SetActive(false);
            }
        }

        if (weapon != null)
        {
            if (activeWeapon != null) activeWeapon.CancelReload();

            weaponMB = (MonoBehaviour)weapon;
            if (weaponMB != null)
            {
                activeWeapon = weapon;

                weaponMB.transform.position = weaponPos.position;
                weaponMB.transform.rotation = weaponPos.rotation;
                weaponMB.transform.SetParent(weaponPos);
                StartFiringWeapon = activeWeapon.FireStart;
                FiringWeapon = activeWeapon.Firing;
                StopFiringWeapon = activeWeapon.FireEnd;
                ReloadWeapon = activeWeapon.Reload;
                weaponMB.gameObject.SetActive(true);
            }
        }
    }

    /// <summary>
    /// Initial setup.
    /// </summary>
    private void Setup()
    {
        if (weaponPos == null) Debug.LogError("No 'weaponPos' set!");
    }
    #endregion
    #endregion

    #region Unity Functions
    private void Awake()
    {
        Setup();
    }
    #endregion
}

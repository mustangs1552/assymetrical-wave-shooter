﻿using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.UI
{
    public class OptionsMenu : MonoBehaviour, IHasMenuLogic
    {
        [SerializeField] private Text versionText = null;

        private bool setVersion = false;

        public void OnClosed()
        {
            
        }
        public void OnOpened()
        {
            if (!setVersion && versionText != null)
            {
                versionText.text = "Version: " + Application.version;
                setVersion = true;
            }
        }
    }
}

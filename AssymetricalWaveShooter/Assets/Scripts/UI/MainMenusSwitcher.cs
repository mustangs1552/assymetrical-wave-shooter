﻿using UnityEngine;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// This controller switches between the different screens in the main menus.
    /// </summary>
    public class MainMenusSwitcher : MonoBehaviour
    {
        [SerializeField] private GameObject mainMenuUI = null;
        [SerializeField] private GameObject playLobbyUI = null;
        [SerializeField] private GameObject optionsUI = null;

        /// <summary>
        /// Show the main menu UI.
        /// </summary>
        public void ShowMainMenuUI()
        {
            HideMenus();

            if (mainMenuUI != null)
            {
                mainMenuUI.SetActive(true);

                IHasMenuLogic hasLogic = mainMenuUI.GetComponent<IHasMenuLogic>();
                if (hasLogic != null) hasLogic.OnOpened();
            }
            else Debug.LogError("No 'MainMenuUI' assigned!");
        }
        /// <summary>
        /// Show the play lobby UI.
        /// </summary>
        public void ShowPlayLobbyUI()
        {
            HideMenus();

            if (playLobbyUI != null)
            {
                playLobbyUI.SetActive(true);

                IHasMenuLogic hasLogic = playLobbyUI.GetComponent<IHasMenuLogic>();
                if (hasLogic != null) hasLogic.OnOpened();
            }
            else Debug.LogError("No 'PlayLobbyUI' assigned!");
        }
        /// <summary>
        /// Show the options UI.
        /// </summary>
        public void ShowOptionsUI()
        {
            HideMenus();

            if (optionsUI != null)
            {
                optionsUI.SetActive(true);

                IHasMenuLogic hasLogic = optionsUI.GetComponent<IHasMenuLogic>();
                if (hasLogic != null) hasLogic.OnOpened();
            }
            else Debug.LogError("No 'OptionsUI' assigned!");
        }

        /// <summary>
        /// Hide all the UI manues.
        /// </summary>
        public void HideMenus()
        {
            if (mainMenuUI != null)
            {
                if(mainMenuUI.activeInHierarchy)
                {
                    IHasMenuLogic hasLogic = mainMenuUI.GetComponent<IHasMenuLogic>();
                    if (hasLogic != null) hasLogic.OnClosed();
                }
                mainMenuUI.SetActive(false);
            }
            if (playLobbyUI != null)
            {
                if (playLobbyUI.activeInHierarchy)
                {
                    IHasMenuLogic hasLogic = playLobbyUI.GetComponent<IHasMenuLogic>();
                    if (hasLogic != null) hasLogic.OnClosed();
                }
                playLobbyUI.SetActive(false);
            }
            if (optionsUI != null)
            {
                if (optionsUI.activeInHierarchy)
                {
                    IHasMenuLogic hasLogic = optionsUI.GetComponent<IHasMenuLogic>();
                    if (hasLogic != null) hasLogic.OnClosed();
                }
                optionsUI.SetActive(false);
            }

            if (LoadingIcon.SINGLETON != null) LoadingIcon.SINGLETON.StopLoading();
        }

        /// <summary>
        /// Quite the game.
        /// </summary>
        public void Quit()
        {
            Application.Quit();
        }

        private void Awake()
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

            if (LoadingIcon.SINGLETON != null) LoadingIcon.SINGLETON.StopLoading();
            ShowMainMenuUI();
        }
    }
}

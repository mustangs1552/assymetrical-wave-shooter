﻿using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Utilities;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// The terminal that the players will use to ready up and un-ready up.
    /// It will also show the number of players ready and total players playing.
    /// </summary>
    public class ReadyUpTerminal : InteractiveTerminal
    {
        [Tooltip("The ready up button.")]
        [SerializeField] private Button readyButton = null;
        [Tooltip("The un-ready up button.")]
        [SerializeField] private Button unReadyButton = null;
        [Tooltip("The 'UITextMacther' for the buttons.")]
        [SerializeField] private UITextMatcher buttonUITextMatcher = null;

        #region Methods
        /// <summary>
        /// Ready up the player and show the un-ready button.
        /// </summary>
        public void ReadyUp()
        {
            RoundControllerBase.SINGLETON.ShooterPlayerReady();
            ShowUnReadyButton();
        }
        /// <summary>
        /// Un-ready up the player and show the ready button.
        /// </summary>
        public void UnReady()
        {
            RoundControllerBase.SINGLETON.ShooterPlayerUnReady();
            ShowReadyButton();
        }

        /// <summary>
        /// Turn on the UI display.
        /// </summary>
        /// <param name="wasRoundSuccess">Was the round successful (Not used, only here to satisfy "RoundController.OnRoundEnd(bool)" requirement).</param>
        private void TurnOn(bool wasRoundSuccess)
        {
            TurnOn();
            ShowReadyButton();
        }

        /// <summary>
        /// Show the ready button and hide the un-ready button.
        /// </summary>
        private void ShowReadyButton()
        {
            readyButton.gameObject.SetActive(true);
            unReadyButton.gameObject.SetActive(false);
        }
        /// <summary>
        /// Show the un-ready button and hide the ready button.
        /// </summary>
        private void ShowUnReadyButton()
        {
            readyButton.gameObject.SetActive(false);
            unReadyButton.gameObject.SetActive(true);
        }

        /// <summary>
        /// Initial setup.
        /// </summary>
        private void Setup()
        {
            if (readyButton == null) Debug.LogError("No 'readyButton' assigned!");
            if (unReadyButton == null) Debug.LogError("No 'unReadyButton' assigned!");
            if (buttonUITextMatcher == null) Debug.LogError("No 'buttonUITextMatcher' assigned!");

            buttonUITextMatcher.OnFinishFontUpdate += ShowReadyButton;
        }
        #endregion

        private new void Awake()
        {
            base.Awake();
            Setup();
        }
        private void Start()
        {
            RoundControllerBase.SINGLETON.OnPlayersReady += TurnOff;
            RoundControllerBase.SINGLETON.OnRoundEnd += TurnOn;
        }
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// The logic for the play lobby screen.
    /// </summary>
    public class PlayLobbyMenu : MonoBehaviour, IHasMenuLogic
    {
        [Tooltip("The traditional shooter players vs AI wave master scene.")]
        [SerializeField] private string traditionalVSAIScene = "";
        [Tooltip("The slider to select number of shooter players.")]
        [SerializeField] private Slider playerCountSlider = null;
        [Tooltip("The text to show current selection of number of shooter players.")]
        [SerializeField] private Text playerCountText = null;
        [Tooltip("Wave master is AI controlled?")]
        [SerializeField] private Toggle aiWaveMasterToggle = null;
        [Tooltip("All other controls that aren't above.")]
        [SerializeField] private Selectable[] otherControls = null;
        [Tooltip("The position and scale of the loading icon when loading the scene.")]
        [SerializeField] private Transform loadingIconPos = null;

        /// <summary>
        /// Update the slider text UI to match the slider value.
        /// </summary>
        public void OnPlayerCountSliderChanged()
        {
            playerCountText.text = playerCountSlider.value.ToString();
        }

        /// <summary>
        /// Laod the game using the settings the player chose.
        /// </summary>
        public void LoadGame()
        {
            if(aiWaveMasterToggle.isOn)
            {
                PlayerPrefs.SetInt("shooterPlayerCount", (int)playerCountSlider.value);
                ToggleAllUIControls(false);
                SceneManager.LoadSceneAsync(traditionalVSAIScene);

                LoadingIcon.SINGLETON.StartLoading(loadingIconPos.position, loadingIconPos.GetComponent<RectTransform>().rect.width);
            }
        }

        /// <summary>
        /// This UI was closed by the MainMenusSwitcher.
        /// </summary>
        public void OnClosed()
        {
            
        }
        /// <summary>
        /// This UI was opened by the MainMenusSwitcher.
        /// </summary>
        public void OnOpened()
        {
            ToggleOtherUIControls(true);
        }

        /// <summary>
        /// Toggle all the UI controls with the interactable state provided.
        /// </summary>
        /// <param name="interactable">Make the UIs interactable?</param>
        private void ToggleAllUIControls(bool interactable)
        {
            playerCountSlider.interactable = interactable;
            aiWaveMasterToggle.interactable = interactable;
            ToggleOtherUIControls(interactable);
        }
        /// <summary>
        /// Toggle all the otherControls UI with the interactable state provided.
        /// </summary>
        /// <param name="interactable">Make the UIs interactable?</param>
        private void ToggleOtherUIControls(bool interactable)
        {
            if (otherControls.Length > 0)
            {
                foreach (Selectable control in otherControls) control.interactable = interactable;
            }
        }

        private void Awake()
        {
            if (traditionalVSAIScene == null || traditionalVSAIScene.Length == 0) Debug.LogError("No 'traditionalVSAIScene' assigned!");

            if (playerCountSlider == null) Debug.LogError("No 'playerCountSlider' assigned!");
            if (playerCountText == null) Debug.LogError("No 'playerCountText' assigned!");
            if (aiWaveMasterToggle == null) Debug.LogError("No 'aiWaveMasterToggle' assigned!");
        }
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Contains simple information on an enemy option like the name, cost, and an optional count.
/// </summary>
public class EnemyOptionUI : MonoBehaviour, IEnemyOptionUI
{
    #region Variables
    #region Public
    [Tooltip("The UI text showing the name of the enemy.")]
    [SerializeField] private Text enemyName = null;
    [Tooltip("The UI text showing the cost of the enemy.")]
    [SerializeField] private Text enemyCost = null;
    [Tooltip("The UI text showing the amount of the enemy (optional).")]
    [SerializeField] private Text enemyCount = null;
    #endregion

    #region Properties
    /// <summary>
    /// The name of the enemy.
    /// </summary>
    public string Name
    {
        get
        {
            return enemyName.text;
        }
    }
    #endregion

    #region Private

    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// Update the name and the cost of the enemy.
    /// </summary>
    /// <param name="name">The name of the enemy.</param>
    /// <param name="cost">The cost of the enemy.</param>
    public void UpdateNameCost(string name, int cost)
    {
        enemyName.text = name;
        enemyCost.text = cost.ToString();
    }
    /// <summary>
    /// Update the count of the enemy if available.
    /// </summary>
    /// <param name="count">The count of the enemy.</param>
    public void UpdateCount(int count)
    {
        if (enemyCount != null) enemyCount.text = count.ToString();
    }

    /// <summary>
    /// Initial setup.
    /// </summary>
    private void Setup()
    {
        if (enemyName == null) Debug.LogError("No 'enemyName' set!");
        if (enemyCost == null) Debug.LogError("No 'enemyCost' set!");
    }
    #endregion

    #region Unity Functions
    private void Awake()
    {
        Setup();
    }
    #endregion
}

﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// The UI controller that handles all the UI elements used by the round controller.
/// </summary>
public class RoundControllerUI : MonoBehaviour, IRoundControllerUI
{
    #region Variables
    #region Public
    [Header("Next Round")]
    [Tooltip("The UI text showing the next round's number.")]
    [SerializeField] private Text nextRoundNum = null;
    [Tooltip("The UI text showing the next round's points.")]
    [SerializeField] private Text nextRoundPoints = null;
    [Tooltip("The transform where the list enemy options are placed.")]
    [SerializeField] private Transform availableEnemiesListArea = null;
    [Tooltip("The enemy options with cost objects to use.")]
    [SerializeField] private GameObject enemyOptionCost = null;
    [Tooltip("The UI raw image showing the next round's heightmap.")]
    [SerializeField] private RawImage nextRoundHeightmapImage = null;
    [Tooltip("The UI raw image showing the next round's POI map.")]
    [SerializeField] private RawImage nextRoundPOIMapImage = null;
    [Tooltip("The UI text showing the total shooter players playing.")]
    [SerializeField] private Text totalPlayersText = null;
    [Tooltip("The UI text showing the total shooter players ready.")]
    [SerializeField] private Text readyPlayerText = null;

    [Header("Play Area")]
    [Tooltip("All the UI texts that will show the timers.")]
    [SerializeField] private Text[] timerTexts = null;

    [Header("Round Results")]
    [Tooltip("The UI that shows the info on the previous round.")]
    [SerializeField] private GameObject resultsUI = null;
    [Tooltip("The UI text showing the previous round's success.")]
    [SerializeField] private Text roundSuccessText = null;
    [Tooltip("The string to put in the success text when the players won.")]
    [SerializeField] private string roundSuccessMessage = "Won!";
    [Tooltip("The string to put in the success text when the players lost.")]
    [SerializeField] private string roundFailedMessage = "Lost!";
    [Tooltip("The UI text showing the previous round's number.")]
    [SerializeField] private Text prevRoundNum = null;
    [Tooltip("The UI text showing the previous round's time.")]
    [SerializeField] private Text prevRoundTime = null;
    [Tooltip("The UI text showing the previous round's points.")]
    [SerializeField] private Text prevRoundPoints = null;
    [Tooltip("The UI text showing the previous round's points that were used by enemies.")]
    [SerializeField] private Text prevRoundPointsUsed = null;
    [Tooltip("The transform where the list enemy counts are placed from the previous round.")]
    [SerializeField] private Transform prevRoundEnemiesListArea = null;
    [Tooltip("The enemy options with cost and count objects to use.")]
    [SerializeField] private GameObject enemyOptionCostCount = null;
    [Tooltip("The UI raw image showing the previous round's heightmap.")]
    [SerializeField] private RawImage prevRoundHeightmapImage = null;
    [Tooltip("The UI raw image showing the previous round's POI map.")]
    [SerializeField] private RawImage prevRoundPOIMapImage = null;

    [Header("Currency")]
    [Tooltip("The text that will be added before the earned currency amount in the UI.")]
    [SerializeField] private string earnedCurrencyPrefix = "Earned Currency: ";
    [Tooltip("The text objects that will show the currently earned currency.")]
    [SerializeField] private Text[] earnedCurrencyTexts = null;
    #endregion

    #region Properties

    #endregion

    #region Private
    private List<GameObject> spawnedAvailableEnemiesList = new List<GameObject>();
    private List<GameObject> spawnedPrevRoundEnemiesList = new List<GameObject>();
    private List<GameObject> inactiveOptionsCosts = new List<GameObject>();
    private List<GameObject> inactiveOptionsCostCounts = new List<GameObject>();

    private Transform inactiveUIObjs = null;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Update the list of enemy options UI.
    /// </summary>
    /// <param name="enemies">The list of enemy options.</param>
    public void UpdateEnemyOptions(List<EnemyOption> enemies)
    {
        // Clear list
        foreach (GameObject obj in spawnedAvailableEnemiesList) ObjectPoolManagerBase.SINGLETON.Destroy(obj);
        spawnedAvailableEnemiesList = new List<GameObject>();

        // Populate list
        GameObject spawnedEnemyOption = null;
        IEnemyOptionUI spawnedEnemyOptionUI = null;
        foreach (EnemyOption enemy in enemies)
        {
            spawnedEnemyOption = ObjectPoolManagerBase.SINGLETON.Instantiate(enemyOptionCost, availableEnemiesListArea, true, false);

            // Setup UI object
            spawnedEnemyOptionUI = spawnedEnemyOption.GetComponent<IEnemyOptionUI>();
            if (spawnedEnemyOptionUI != null)
            {
                spawnedEnemyOptionUI.UpdateNameCost(enemy.Enemy.name, enemy.PointCost);
                spawnedAvailableEnemiesList.Add(spawnedEnemyOption);
            }
            else ObjectPoolManagerBase.SINGLETON.Destroy(spawnedEnemyOption);
        }
    }

    /// <summary>
    /// Update the next round number UI.
    /// </summary>
    /// <param name="roundNum">The round number.</param>
    public void UpdateNextRoundNum(int roundNum)
    {
        nextRoundNum.text = roundNum.ToString();
    }
    /// <summary>
    /// Update the next round available points UI.
    /// </summary>
    /// <param name="totalPoints">The next round's available points.</param>
    public void UpdateNextRoundPoints(int totalPoints)
    {
        nextRoundPoints.text = totalPoints.ToString();
    }
    /// <summary>
    /// Update the UI for the heightmap for the next round.
    /// </summary>
    /// <param name="heightmap">The next round's heightmap.</param>
    public void UpdateNextRoundHeightmap(PMHeightmap heightmap)
    {
        if (heightmap != null && heightmap.Heightmap != null && heightmap.POIMap != null)
        {
            nextRoundHeightmapImage.texture = heightmap.Heightmap;
            nextRoundPOIMapImage.texture = heightmap.POIMap;
        }
    }
    /// <summary>
    /// Update the UI for the count of shooter players and the amount that are readied up.
    /// </summary>
    /// <param name="totalPlayers">The total shooter players</param>
    /// <param name="readyPlayers">The total shooter players readied up.</param>
    public void UpdatePlayerCounts(int totalPlayers, int readyPlayers = 0)
    {
        totalPlayersText.text = totalPlayers.ToString();
        readyPlayerText.text = readyPlayers.ToString();
    }

    /// <summary>
    /// Update the pre-playing countdown timer UI.
    /// </summary>
    /// <param name="secondsRemaining">Time remaining in seconds.</param>
    public void UpdatePrePlayingCountdown(int secondsRemaining)
    {
        UpdateTimer(secondsRemaining);
    }
    /// <summary>
    /// Update the timer UI while playing.
    /// </summary>
    /// <param name="secondsPassed">The time passed in seconds.</param>
    public void UpdatePlayingTime(int secondsPassed)
    {
        UpdateTimer(secondsPassed);
    }

    /// <summary>
    /// Update the round success UI.
    /// </summary>
    /// <param name="roundWon">Was the round won?</param>
    public void UpdateRoundSuccess(bool roundWon)
    {
        if (roundWon) roundSuccessText.text = roundSuccessMessage;
        else roundSuccessText.text = roundFailedMessage;
    }
    /// <summary>
    /// Update the finished round's number UI.
    /// </summary>
    /// <param name="roundNum">The finished round's number.</param>
    public void UpdatePrevRoundNum(int roundNum)
    {
        prevRoundNum.text = roundNum.ToString();

        ShowResultsUI();
    }
    /// <summary>
    /// Update the UI for the points from the finished round.
    /// </summary>
    /// <param name="totalPoints">The total points that was available.</param>
    /// <param name="totalPointsEnemies">The total points used by enemies.</param>
    public void UpdatePrevRoundPoints(int totalPoints, int totalPointsEnemies)
    {
        prevRoundPoints.text = totalPoints.ToString();
        prevRoundPointsUsed.text = totalPointsEnemies.ToString();

        ShowResultsUI();
    }
    /// <summary>
    /// Update the finished round's time UI.
    /// </summary>
    /// <param name="seconds">The duration of the finished round.</param>
    public void UpdatePrevRoundTime(int seconds)
    {
        SetTimeFormat(prevRoundTime, seconds);

        ShowResultsUI();
    }
    /// <summary>
    /// Update the UI for the list of enemies that were used in the finished round.
    /// </summary>
    /// <param name="selectedEnemiesCosts">Selected enemies with their costs.</param>
    /// <param name="selectedEnemiesCounts">Selected enemies with their counts.</param>
    public void UpdatePrevRoundEnemies(List<EnemyOption> selectedEnemiesCosts, List<EnemyCount> selectedEnemiesCounts)
    {
        // Clear list
        foreach (GameObject obj in spawnedPrevRoundEnemiesList) ObjectPoolManagerBase.SINGLETON.Destroy(obj);
        spawnedPrevRoundEnemiesList = new List<GameObject>();

        // Populate list
        GameObject spawnedEnemyOption = null;
        IEnemyOptionUI spawnedEnemyOptionUI = null;
        foreach (EnemyCount enemy in selectedEnemiesCounts)
        {
            spawnedEnemyOption = ObjectPoolManagerBase.SINGLETON.Instantiate(enemyOptionCostCount, prevRoundEnemiesListArea, true, false);

            // Setup UI object
            spawnedEnemyOptionUI = spawnedEnemyOption.GetComponent<IEnemyOptionUI>();
            if (spawnedEnemyOptionUI != null)
            {
                foreach (EnemyOption selectedEnemy in selectedEnemiesCosts)
                {
                    if (selectedEnemy.Enemy.name == enemy.Enemy.name)
                    {
                        spawnedEnemyOptionUI.UpdateCount(enemy.Count);
                        spawnedEnemyOptionUI.UpdateNameCost(selectedEnemy.Enemy.name, selectedEnemy.PointCost);
                        break;
                    }
                }

                spawnedPrevRoundEnemiesList.Add(spawnedEnemyOption);
            }
            else ObjectPoolManagerBase.SINGLETON.Destroy(spawnedEnemyOption);
        }

        ShowResultsUI();
    }
    /// <summary>
    /// Update the UI for the heightmap used in the finished round.
    /// </summary>
    /// <param name="heightmap">The heightmap used in the finished round.</param>
    public void UpdatePrevRoundHeightmap(PMHeightmap heightmap)
    {
        if (heightmap != null && heightmap.Heightmap != null && heightmap.POIMap != null)
        {
            prevRoundHeightmapImage.texture = heightmap.Heightmap;
            prevRoundPOIMapImage.texture = heightmap.POIMap;

            ShowResultsUI();
        }
    }

    /// <summary>
    /// Update all the assigned earned currency texts with the amount provided and prefix.
    /// </summary>
    /// <param name="amount">New amount of currency.</param>
    public void UpdateEarnedCurrency(int amount)
    {
        foreach (Text text in earnedCurrencyTexts) text.text = earnedCurrencyPrefix + amount;
    }
    #endregion

    #region Private
    /// <summary>
    /// Update all the timers that are set in the 'timerTexts' variable.
    /// </summary>
    /// <param name="seconds">The time to be shown.</param>
    private void UpdateTimer(int seconds)
    {
        foreach (Text timerText in timerTexts) SetTimeFormat(timerText, seconds);
    }
    /// <summary>
    /// Set the given time in the desired format for the given text UI.
    /// </summary>
    /// <param name="uiText">The text UI to set.</param>
    /// <param name="seconds">The time to set.</param>
    private void SetTimeFormat(Text uiText, int seconds)
    {
        uiText.text = (seconds / 60).ToString("D2") + ":" + (seconds % 60).ToString("D2");
    }

    /// <summary>
    /// Show the results UI screen.
    /// </summary>
    private void ShowResultsUI()
    {
        resultsUI.SetActive(true);
    }
    /// <summary>
    /// Hide the results UI screen.
    /// </summary>
    private void HideResultsUI()
    {
        resultsUI.SetActive(false);
    }

    /// <summary>
    /// Initial setup.
    /// </summary>
    private void Setup()
    {
        if (nextRoundNum == null) Debug.LogError("No 'nextRoundNum' set!");
        if (nextRoundPoints == null) Debug.LogError("No 'nextRoundPoints' set!");
        if (availableEnemiesListArea == null) Debug.LogError("No 'availableEnemiesListArea' set!");
        if (enemyOptionCost == null) Debug.LogError("No 'enemyOptionCost' set!");
        if (nextRoundHeightmapImage == null) Debug.LogError("No 'nextRoundHeightmapImage' set!");
        if (nextRoundPOIMapImage == null) Debug.LogError("No 'nextRoundPOIMapImage' set!");
        if (totalPlayersText == null) Debug.LogError("No 'totalPlayersText' set!");
        if (readyPlayerText == null) Debug.LogError("No 'readyPlayerText' set!");

        if (timerTexts == null) Debug.LogError("No 'timerText' set!");

        if (resultsUI == null) Debug.LogError("No 'resultsUI' set!");
        if (prevRoundNum == null) Debug.LogError("No 'prevRoundNum' set!");
        if (prevRoundTime == null) Debug.LogError("No 'prevRoundTime' set!");
        if (prevRoundPoints == null) Debug.LogError("No 'prevRoundPoints' set!");
        if (prevRoundPointsUsed == null) Debug.LogError("No 'prevRoundPointsUsed' set!");
        if (prevRoundEnemiesListArea == null) Debug.LogError("No 'prevRoundEnemiesListArea' set!");
        if (enemyOptionCostCount == null) Debug.LogError("No 'enemyOptionCostCount' set!");
        if (prevRoundHeightmapImage == null) Debug.LogError("No 'prevRoundHeightmapImage; set!");
        if (prevRoundPOIMapImage == null) Debug.LogError("No 'prevRoundPOIMapImage; set!");

        inactiveUIObjs = new GameObject("INACTIVE_UI_OBJS").transform;
        inactiveUIObjs.SetParent(transform);

        HideResultsUI();
    }
    #endregion
    #endregion

    #region Unity Functions
    private void Awake()
    {
        Setup();
    }
    #endregion
}

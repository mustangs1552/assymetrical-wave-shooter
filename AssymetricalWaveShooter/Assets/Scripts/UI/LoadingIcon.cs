﻿using UnityEngine;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// The main loading icon for the scene.
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    class LoadingIcon : MonoBehaviour
    {
        public static LoadingIcon SINGLETON = null;

        [SerializeField] private float spinSpeed = 1;

        private bool animate = false;
        private RectTransform trans = null;

        /// <summary>
        /// Show and position the loading icon and start animating.
        /// </summary>
        /// <param name="pos">The position of the icon.</param>
        /// <param name="scale">The scale of the icon.</param>
        public void StartLoading(Vector2 pos, float scale)
        {
            trans.position = pos;
            trans.sizeDelta = new Vector2(scale, scale);

            gameObject.SetActive(true);
            animate = true;
        }
        /// <summary>
        /// Hide and stop animating the loading icon.
        /// </summary>
        public void StopLoading()
        {
            gameObject.SetActive(false);
            animate = false;
        }

        /// <summary>
        /// Spin the icon.
        /// </summary>
        private void Animate()
        {
            trans.Rotate(Vector3.forward * spinSpeed * Time.deltaTime);
        }

        private void Awake()
        {
            if (LoadingIcon.SINGLETON == null) SINGLETON = this;
            else
            {
                Debug.LogError("More than one 'LoadingIcon' singleton detected!");
                Destroy(this);
            }

            trans = GetComponent<RectTransform>();
        }

        private void Update()
        {
            if (animate) Animate();
        }
    }
}

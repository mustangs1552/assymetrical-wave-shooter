﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Assets.Scripts.Controllers;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// The terminal used to exit to the main menu.
    /// </summary>
    public class ExitToMainMenuTerminal : InteractiveTerminal
    {
        [Tooltip("The name of the main menu scene.")]
        [SerializeField] private string mainMenuSceneName = "";

        /// <summary>
        /// Load to the main menu.
        /// </summary>
        public void LoadMainMenu()
        {
            UnityAnalyticsController.GameModeEnded(false);
            if (mainMenuSceneName != null && mainMenuSceneName.Length > 0) SceneManager.LoadSceneAsync(mainMenuSceneName);
        }
    }
}

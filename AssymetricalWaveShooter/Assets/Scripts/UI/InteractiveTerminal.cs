﻿using UnityEngine;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// A general interactive terminal that players can interact with in world.
    /// It will be used as a base for other terminals.
    /// </summary>
    public abstract class InteractiveTerminal : MonoBehaviour
    {
        [Tooltip("The UI object of this terminal.")]
        [SerializeField] private GameObject ui = null;

        /// <summary>
        /// Turn the UI display off.
        /// </summary>
        public void TurnOff()
        {
            ui.SetActive(false);
        }
        /// <summary>
        /// Turn the UI display on.
        /// </summary>
        public void TurnOn()
        {
            ui.SetActive(true);
        }

        /// <summary>
        /// Initial setup.
        /// </summary>
        private void Setup()
        {
            if (ui == null) Debug.LogError("No 'ui' object assigned!");
        }

        protected void Awake()
        {
            Setup();
        }
    }

}

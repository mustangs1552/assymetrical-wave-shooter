﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Items;
using Assets.Scripts.Utilities;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// The UI of the Currency Terminal.
    /// This handles the controls for the UI display and has the list of items available for sale.
    /// Handles the button event for buying an item but, passes the process of making the actual purchase off to the main terminal script.
    /// </summary>
    public class CurrencyTerminalUI : MonoBehaviour
    {
        #region Variables
        [Header("Shop Settings")]
        [Tooltip("The name of this shop for the header.")]
        [SerializeField] private string shopName = "Shop";
        [Tooltip("The text object for the shop name header.")]
        [SerializeField] private Text shopNameTextObj = null;

        [Tooltip("The raw image object that shows the icons of the selected item.")]
        [SerializeField] private RawImage iconObj = null;
        [Tooltip("The text object prefab to use for the item names and descriptions.")]
        [SerializeField] private GameObject textObjPrefab = null;
        [Tooltip("The items that are available for sale at this terminal.")]
        [SerializeField] private List<GameObject> availableItems = new List<GameObject>();
        [Tooltip("The main terminal script.")]
        [SerializeField] private CurrencyTerminal mainScript = null;

        [Header("UITextMatcher Settings")]
        [Tooltip("The max attempts the 'UITextMatcher' components will have.")]
        [SerializeField] private int textMatcherMaxAttempts = 5;
        [Tooltip("The size difference desired for the header text objects vs the rest of the text objects.")]
        [SerializeField] private int headerFontSizeDifference = 5;
        [Tooltip("Size up the headers from the rest of the text objects or size down the non-header text objects.")]
        [SerializeField] private bool sizeUpHeaders = false;

        private int CurrItem
        {
            get
            {
                return currItem;
            }
            set
            {
                if (value >= addedItems.Count) currItem = 0;
                else if (value < 0) currItem = addedItems.Count - 1;
                else currItem = value;
            }
        }

        private Transform namesAnchor = null;
        private Transform descriptionsAnchor = null;
        private UITextMatcher itemsTextMatcher = null;
        private List<GameObject> addedItems = new List<GameObject>();
        private int currItem;
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Hide this display to look off.
        /// </summary>
        public void TurnOff()
        {
            gameObject.SetActive(false);
        }
        /// <summary>
        /// Show this display UI.
        /// </summary>
        public void TurnOn()
        {
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Select the next item in the list.
        /// </summary>
        public void SelectNextItem()
        {
            CurrItem++;
            SelectItem(CurrItem);
        }
        /// <summary>
        /// Select the previous item in the list.
        /// </summary>
        public void SelectPrevItem()
        {
            CurrItem--;
            SelectItem(CurrItem);
        }
        /// <summary>
        /// Select the item at the given index.
        /// </summary>
        /// <param name="index">The index of the item to be selected.</param>
        public void SelectItem(int index)
        {
            HideItems();

            iconObj.texture = addedItems[index].GetComponent<IUIItem>().Icon;
            iconObj.gameObject.SetActive(true);
            namesAnchor.GetChild(index).gameObject.SetActive(true);
            descriptionsAnchor.GetChild(index).gameObject.SetActive(true);
        }
        /// <summary>
        /// Hide all the UI elements so nothing is shown on the display.
        /// </summary>
        public void HideItems()
        {
            iconObj.texture = null;
            iconObj.gameObject.SetActive(false);

            for (int i = 0; i < namesAnchor.childCount; i++) namesAnchor.GetChild(i).gameObject.SetActive(false);
            for (int i = 0; i < descriptionsAnchor.childCount; i++) descriptionsAnchor.GetChild(i).gameObject.SetActive(false);
        }

        /// <summary>
        /// Call the main terminal to make the purchase of the selected item.
        /// </summary>
        public void StartPurchase()
        {
            mainScript.MakePurchase(addedItems[currItem].GetComponent<IUIItem>());
        }
        #endregion

        #region Private
        /// <summary>
        /// Destroy all the UI elements.
        /// </summary>
        private void DestroyUIItems()
        {
            for(int i = 0; i < namesAnchor.childCount; i++) ObjectPoolManagerBase.SINGLETON.Destroy(namesAnchor.GetChild(i).gameObject);
            for (int i = 0; i < descriptionsAnchor.childCount; i++) ObjectPoolManagerBase.SINGLETON.Destroy(descriptionsAnchor.GetChild(i).gameObject);
        }
        /// <summary>
        /// Create a new UI element for the given item.
        /// </summary>
        /// <param name="item">The item the UI element is for.</param>
        private void AddItemUI(IUIItem item)
        {
            Text text = ObjectPoolManagerBase.SINGLETON.Instantiate(textObjPrefab, namesAnchor, true, false).GetComponent<Text>();
            text.text = item.Name;
            text.fontStyle = FontStyle.Bold;

            text = ObjectPoolManagerBase.SINGLETON.Instantiate(textObjPrefab, descriptionsAnchor, true, false).GetComponent<Text>();
            text.text = item.Cost + " Point(s)\n\n" + item.Description;
        }
        /// <summary>
        /// Reset and populate the UI with UI elements for each of the available items.
        /// </summary>
        private void PopulateItems()
        {
            DestroyUIItems();
            iconObj.texture = null;
            addedItems = new List<GameObject>();

            foreach(GameObject item in availableItems)
            {
                IUIItem uiItem = item.GetComponent<IUIItem>();
                if(uiItem != null)
                {
                    AddItemUI(uiItem);
                    addedItems.Add(item);
                }
            }

            itemsTextMatcher.StartUpdateFontSizes(true);
        }

        /// <summary>
        /// Finish the setup of the items.
        /// </summary>
        private void FinishSetup()
        {
            HideItems();
            SelectItem(currItem);
        }

        /// <summary>
        /// Initial setup.
        /// </summary>
        private void Setup()
        {
            if (shopNameTextObj == null) Debug.LogError("No 'shopNameTextObj' assigned!");
            if (iconObj == null) Debug.LogError("No 'iconObj' assigned!");
            if (textObjPrefab == null) Debug.LogError("No 'textObjPrefab' assigned!");

            gameObject.name = shopName + "-" + gameObject.name;
            namesAnchor = GameObject.Find(gameObject.name + "/Panel/Items/Info/Names").transform;
            if (namesAnchor == null) Debug.LogError("No 'Names' object found at '" + gameObject.name + "/Panel/Items/Info/'!");
            else
            {
                itemsTextMatcher = namesAnchor.parent.GetComponent<UITextMatcher>();
                if (itemsTextMatcher == null) itemsTextMatcher = namesAnchor.parent.gameObject.AddComponent<UITextMatcher>();
            }
            descriptionsAnchor = GameObject.Find(gameObject.name + "/Panel/Items/Info/Descriptions").transform;
            if (descriptionsAnchor == null) Debug.LogError("No 'Descriptions' object found at '" + gameObject.name + "/Panel/Items/Info/'!");
            else
            {
                if(itemsTextMatcher == null)
                {
                    itemsTextMatcher = descriptionsAnchor.parent.GetComponent<UITextMatcher>();
                    if (itemsTextMatcher == null) itemsTextMatcher = descriptionsAnchor.parent.gameObject.AddComponent<UITextMatcher>();
                }
            }

            if (mainScript == null) Debug.LogError("No 'mainScript' assigned!");

            itemsTextMatcher.OnFinishFontUpdate += FinishSetup;
            itemsTextMatcher.MaxAttempts = textMatcherMaxAttempts;
            itemsTextMatcher.HeaderTextParent = (namesAnchor != null) ? namesAnchor.gameObject : descriptionsAnchor.gameObject;
            itemsTextMatcher.HeaderFontSizeDifference = headerFontSizeDifference;
            itemsTextMatcher.SizeUpHeader = sizeUpHeaders;
        }
        #endregion
        #endregion

        #region Unity Methods
        private void Awake()
        {
            Setup();
        }
        private void Start()
        {
            shopNameTextObj.text = shopName;

            PopulateItems();
        }
        #endregion
    }
}

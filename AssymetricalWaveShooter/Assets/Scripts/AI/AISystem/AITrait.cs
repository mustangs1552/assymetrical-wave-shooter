﻿using UnityEngine;

/// <summary>
/// The AI Traits are the main Traits of the AI.
/// These require attention from the AI because they cannot be performed alongside other AI Traits.
/// They would typically use Passive Traits to get values and/or perform their actions.
/// For example: Various AI Traits may need to use the Movement Passive Trait to move to a target to be able to perform their action.
/// </summary>
public abstract class AITrait : Trait
{
    #region Variables
    #region Public
    [Tooltip("If isDefault is true then priority will be ignored.")]
    [SerializeField] private bool isDefault = false;
    [Tooltip("The priority of this module compared to other modules.")]
    [SerializeField] private int priority = 10;
    #endregion

    #region Properties
    /// <summary>
    /// Is this the default module of the AIBrain?
    /// </summary>
    public bool IsDefault
    {
        get
        {
            return isDefault;
        }
    }
    /// <summary>
    /// This module's priority the AIBrain uses to determine which module can perform their needed actions.
    /// </summary>
    public int Priority
    {
        get
        {
            return currPriority;
        }
    }
    #endregion

    #region Private
    protected int currPriority = 0;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Called by AIBrain before the abstract setups.
    /// </summary>
    public override void InitialTraitSetup()
    {
        base.InitialTraitSetup();
        Setup();
    }

    /// <summary>
    /// Called from the AIBrain when it is trying to see if this Trait needs attention.
    /// </summary>
    /// <returns>True if this Trait does need attention.</returns>
    public abstract bool Checking();
    /// <summary>
    /// Called from the AIBrain when it has chosen this Trait to perform what it needs after Checking() returned true meaning this Trait needed attention.
    /// </summary>
    /// <returns>True when attention is still needed.</returns>
    public abstract bool Performing();
    /// <summary>
    /// Called from the AIBrain when it decides that this Trait no longer has priority.
    /// Cancel whatever needs to be canceled.
    /// AIBrain will re-call Checking() to make sure that this Trait still needs attention once this Trait has priority again.
    /// If not possible to be canceled then AIBrain will re-check the same Trait that needed attendtion until cancelation is possible.
    /// </summary>
    /// <returns>True if successfully canceled.</returns>
    public abstract bool CancelPerform();
    #endregion

    #region Private
    /// <summary>
    /// Initial setup.
    /// </summary>
    private void Setup()
    {
        currPriority = (priority >= 0) ? priority : 0;
    }
    #endregion
    #endregion

    #region Unity Functions
    
    #endregion
}

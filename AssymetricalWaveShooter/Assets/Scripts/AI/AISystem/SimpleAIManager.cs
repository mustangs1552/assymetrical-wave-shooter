﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// An AI manager that simply goes throught the list of AI and tells them to perform their needed actions.
/// </summary>
public class SimpleAIManager : MonoBehaviour
{
    #region Variables
    #region Public
    [Tooltip("The frequency that the AI are performed.")]
    [SerializeField] private float frequency = 1;
    #endregion

    #region Properties

    #endregion

    #region Private
    private float lastPass = 0;
    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// Go through all the AI and perform them once.
    /// </summary>
    private void DoPass()
    {
        List<AIBrain> brains = ObjectPoolManagerBase.SINGLETON.GetActiveObjectsComponents<AIBrain>();
        foreach(AIBrain brain in brains)
        {
            if (brain.gameObject.activeInHierarchy) brain.RunAIOnce();
        }
    }
    #endregion

    #region Unity Functions
    private void Update()
    {
        if(Time.time - lastPass >= frequency)
        {
            DoPass();
            lastPass = Time.time;
        }
    }
    #endregion
}

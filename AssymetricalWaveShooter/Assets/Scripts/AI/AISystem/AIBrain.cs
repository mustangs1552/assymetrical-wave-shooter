﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Utilities.ObjectPooling.Interfaces;

/// <summary>
/// The main class for a creature.
/// The main job of this class is to make decissions on what the creature will do.
/// The various traits that this class manages are what "does" things.
/// Uses the AITraits on this creature to determine what the creature does.
/// The AITraits can't perform thier actions on at the same time so therefore, this class determines which ones can perform thier actions first.
/// They know what they need to do.
/// </summary>
public class AIBrain : MonoBehaviour, IObjectPoolEvents
{
    #region Variables
    #region Public
    
    #endregion

    #region Properties
    /// <summary>
    /// The current AI trait that this class chosen to do its thing.
    /// </summary>
    public AITrait CurrAITrait
    {
        get
        {
            return currAITrait;
        }
    }

    /// <summary>
    /// The default AI trait that this class is using.
    /// </summary>
    public AITrait DefaultAITrait
    {
        get
        {
            return defaultAITrait;
        }
    }
    #endregion

    #region Private
    private PassiveTrait[] passiveTraits = null;

    private AITrait[] aiTraits = null;
    private AITrait defaultAITrait = null;
    private Dictionary<Type, Trait> traitAccess = new Dictionary<Type, Trait>();

    private int currAITraitI = 0;
    private AITrait currAITrait = null;

    private bool setupRan = false;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Perform everything this AI needs to do.
    /// </summary>
    /// <param name="managerLoopNum">Current AI manager loop number.</param>
    public void RunAIOnce()
    {
        AITraitLoop();
        PassiveTraitLoop();
    }

    /// <summary>
    /// Reset the AIBrain and the AITraits.
    /// </summary>
    public void ResetAI()
    {
        if (setupRan)
        {
            foreach (Trait trait in aiTraits) trait.TraitReset();
            foreach (Trait trait in passiveTraits) trait.TraitReset();
        }
    }

    /// <summary>
    /// Checks to see if this AI is performing a trait other than the default trait.
    /// </summary>
    /// <returns>True if it is performing a trait that isn't the default trait.</returns>
    public bool IsPerformingTrait()
    {
        return currAITrait != null && defaultAITrait != null && currAITrait != defaultAITrait;
    }
    /// <summary>
    /// Checks to see if it is performing the given trait.
    /// Can also check the opposite, if it is performing any other trait that isn't the default trait or the given trait.
    /// </summary>
    /// <param name="trait">The trait to check.</param>
    /// <param name="invertCheck">If true than check the opposite.</param>
    /// <returns>True if given trait is performing or the any other trait is performing when invertCheck is true.</returns>
    public bool IsPerformingTrait(AITrait trait, bool invertCheck = false)
    {
        if (!invertCheck) return currAITrait != null && currAITrait == trait;
        else return currAITrait != null && defaultAITrait != null && currAITrait != defaultAITrait && currAITrait != trait;
    }

    /// <summary>
    /// Get a trait that the AI brain is using.
    /// </summary>
    /// <param name="type">The class type of the desired trait.</param>
    /// <returns>The trait desired.</returns>
    public Trait GetTrait(Type type)
    {
        if (traitAccess.ContainsKey(type)) return traitAccess[type];

        return null;
    }

    /// <summary>
    /// Run the setup.
    /// </summary>
    public void RunSetup()
    {
        Setup();
    }

    /// <summary>
    /// Reset the AI when potentially re-spawned by object pool system.
    /// </summary>
    public void OnInstantiatedByObjectPool()
    {
        ResetAI();
    }
    public void OnDestroyedByObjectPool()
    {
        
    }
    #endregion

    #region Private
    /// <summary>
    /// Checks the current AITrait to see if it needs attention.
    /// If it does need attention and one is already being performed and this new AITrait has higher priority then it will cancel what its doing and this one will replace it.
    /// If it doesn't then the default module will become active if it isn't already.
    /// </summary>
    private void CheckAITrait()
    {
        if (aiTraits != null && aiTraits.Length > 0)
        {
            if (aiTraits[currAITraitI].Checking())
            {
                if (currAITrait == null)
                {
                    if (defaultAITrait != null && currAITrait == defaultAITrait)
                    {
                        if (currAITrait.CancelPerform()) currAITrait = aiTraits[currAITraitI];
                        else currAITraitI--;
                    }
                    else currAITrait = aiTraits[currAITraitI];
                }
                else if (currAITrait.Priority < aiTraits[currAITraitI].Priority)
                {
                    if (currAITrait.CancelPerform()) currAITrait = aiTraits[currAITraitI];
                    else currAITraitI--;
                }
            }
            else if (aiTraits[currAITraitI] == currAITrait) currAITrait = null;

            currAITraitI++;
            if (currAITraitI >= aiTraits.Length) currAITraitI = 0;
        }

        if (currAITrait == null && defaultAITrait != null)
        {
            if (defaultAITrait.Checking()) currAITrait = defaultAITrait;
        }
    }
    /// <summary>
    /// Performs the current AITrait's actions if one is active.
    /// </summary>
    private void PerformAITrait()
    {
        if (currAITrait != null && !currAITrait.Performing()) currAITrait = null;
    }
    /// <summary>
    /// The main loop that checks the AITraits anf performs the AITrait that currently needs the most attention.
    /// </summary>
    private void AITraitLoop()
    {
        CheckAITrait();
        PerformAITrait();
    }

    /// <summary>
    /// Run all the system and passive traits.
    /// </summary>
    private void PassiveTraitLoop()
    {
        if (passiveTraits != null && passiveTraits.Length > 0)
        {
            foreach (PassiveTrait trait in passiveTraits) trait.UpdateTrait();
        }
    }

    /// <summary>
    /// Run the normal setup methods on the traits and modules.
    /// </summary>
    private void RunSetups()
    {
        foreach (PassiveTrait trait in passiveTraits) trait.TraitSetup();

        if (defaultAITrait != null) defaultAITrait.TraitSetup();
        if (aiTraits != null && aiTraits.Length > 0)
        {
            foreach (AITrait mod in aiTraits) mod.TraitSetup();
        }
    }
    /// <summary>
    /// Run the Post setup methods on the traits and modules.
    /// </summary>
    private void RunPostSetups()
    {
        foreach (PassiveTrait trait in passiveTraits) trait.PostTraitSetup();

        if (defaultAITrait != null) defaultAITrait.PostTraitSetup();
        if (aiTraits != null && aiTraits.Length > 0)
        {
            foreach (AITrait mod in aiTraits) mod.PostTraitSetup();
        }
    }
    /// <summary>
    /// Run all other setups.
    /// </summary>
    private void RunOtherSetups()
    {
        RunSetups();
        RunPostSetups();
    }

    /// <summary>
    /// Find all the PassiveTrait objects on this GameObject and tell them to perform their initial setup.
    /// </summary>
    private void FindPassiveTraits()
    {
        passiveTraits = GetComponents<PassiveTrait>();
        if (passiveTraits.Length > 0)
        {
            foreach (PassiveTrait trait in passiveTraits)
            {
                trait.InitialTraitSetup();
                trait.PreTraitSetup();
                traitAccess.Add(trait.GetType(), trait);
            }
        }
    }
    /// <summary>
    /// Find all the AITrait objects on this GameObject and tell them to perform their initial setup.
    /// </summary>
    private void FindAITraits()
    {
        aiTraits = GetComponents<AITrait>();
        if (aiTraits.Length > 0)
        {
            foreach (AITrait trait in aiTraits)
            {
                if (!trait.IsDefault)
                {
                    trait.InitialTraitSetup();
                    trait.PreTraitSetup();
                    traitAccess.Add(trait.GetType(), trait);
                }
                else if (defaultAITrait == null)
                {
                    defaultAITrait = trait;
                    trait.InitialTraitSetup();
                    trait.PreTraitSetup();
                    traitAccess.Add(trait.GetType(), trait);
                }
                else Debug.LogWarning("There can only be one default trait!");
            }

            if (defaultAITrait != null)
            {
                List<AITrait> trait = new List<AITrait>(aiTraits);
                trait.Remove(defaultAITrait);
                aiTraits = trait.ToArray();
            }
        }
    }
    /// <summary>
    /// Checls to see if any AITraits' priorities are the same.
    /// For intended behaviour, priorities must be different.
    /// Returns after the first match.
    /// </summary>
    /// <returns>True if two AITraits' priorities match.</returns>
    private bool CheckAITraitsPriorities()
    {
        if (aiTraits.Length > 1)
        {
            foreach (AITrait modOne in aiTraits)
            {
                foreach (AITrait modTwo in aiTraits)
                {
                    if (modTwo != modOne && modTwo.Priority == modOne.Priority)
                    {
                        Debug.LogWarning(modTwo.GetType() + " and " + modOne.GetType() + " have the same priority! Behaviour may not be as intended.");
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /// <summary>
    /// Initial setup.
    /// </summary>
    private void Setup()
    {
        if (!setupRan)
        {
            FindPassiveTraits();
            FindAITraits();
            RunOtherSetups();

            CheckAITraitsPriorities();

            setupRan = true;
        }
    }
    #endregion
    #endregion

    #region Unity Functions
    private void Start()
    {
        Invoke("Setup", .25f);
    }
    #endregion
}

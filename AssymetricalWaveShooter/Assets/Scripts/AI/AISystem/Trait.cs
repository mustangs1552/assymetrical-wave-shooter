﻿using UnityEngine;

[RequireComponent(typeof(AIBrain))]
/// <summary>
/// Traits are the different modules that an AI has.
/// They are designed to simply just drop on a new AI and that AI will now have access to that trait without any changes beyond options for that Trait.
/// The Traits will all be found by the AIBrain which is what manages and calls all of them, traits should not use the default Unity Start() and Update() type methods.
/// </summary>
public abstract class Trait : MonoBehaviour
{
    #region Variables
    #region Public
    
    #endregion

    #region Properties
    /// <summary>
    /// The AIBrain that manages this Trait.
    /// </summary>
    public AIBrain Brain
    {
        get
        {
            return aiBrain;
        }
    }
    #endregion

    #region Private
    private AIBrain aiBrain = null;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Called by AIBrain before the abstract setups.
    /// </summary>
    public virtual void InitialTraitSetup()
    {
        Setup();
    }

    /// <summary>
    /// Called from the AIBrain as soon as this trait is found.
    /// </summary>
    public abstract void PreTraitSetup();
    /// <summary>
    /// Called from the AIBrain after it finds all the traits.
    /// </summary>
    public abstract void TraitSetup();
    /// <summary>
    /// Called from the AIBrain after it calls PreTraitSetup() and TraitSetup() on all traits.
    /// </summary>
    public abstract void PostTraitSetup();

    /// <summary>
    /// Called from the AIBrain when it needs to reset everything.
    /// </summary>
    public abstract void TraitReset();
    #endregion

    #region Private
    /// <summary>
    /// Inital setup.
    /// </summary>
    private void Setup()
    {
        aiBrain = GetComponent<AIBrain>();
    }
    #endregion
    #endregion

    #region Unity Functions
    
    #endregion
}

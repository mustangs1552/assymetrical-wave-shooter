﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Manages all the AI in the scene by different distances from the player.
/// Can set a pattern that the manager will follow and run the AI that falls in that current distance for the current frame.
/// Can also set a max amount of AI to run for that pass and a frame gap between passes.
/// </summary>
public class AIManagerDistPattern : MonoBehaviour
{
    private enum AIFramePatternTypes
    {
        Nearby,
        Medium,
        Far,
    }

    #region Variables
    #region Public
    [Tooltip("Frames between each call.")]
    [SerializeField] private int frameGap = 0;
    [Tooltip("Max amount of AI for medium and far AI each frame.")]
    [SerializeField] private int maxDistantAIPerFrame = 5;
    [Tooltip("Max distance for nearby AI.")]
    [SerializeField] private float nearbyAIMaxDist = 20;
    [Tooltip("Max distance for medium AI.")]
    [SerializeField] private float mediumAIMaxDist = 40;
    [Tooltip("AI pattern order. Each is ran in one frame and frame gap is between each then returns to first after the last.")]
    [SerializeField] private AIFramePatternTypes[] aiPattern = { AIFramePatternTypes.Nearby, AIFramePatternTypes.Medium, AIFramePatternTypes.Far };
    #endregion

    #region Properties

    #endregion

    #region Private
    private int frameGapI = 0;
    private int aiFramePatternI = 0;
    private int currLoopNum = 0;
    private Dictionary<int, int> aiLoopNumbers = new Dictionary<int, int>();
    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// Performs all AI that are within the nearby range of the player.
    /// </summary>
    private void DoNearbyAILoop()
    {
        List<AIBrain> objectManagerObjs = ObjectPoolManagerBase.SINGLETON.GetActiveObjectsComponents<AIBrain>();
        float closest = -1;
        foreach (AIBrain aiBrain in objectManagerObjs)
        {
            AIBrain currAIBrain = aiBrain.GetComponent<AIBrain>();
            if (currAIBrain != null)
            {
                if (!aiLoopNumbers.ContainsKey(currAIBrain.GetInstanceID())) aiLoopNumbers.Add(currAIBrain.GetInstanceID(), -1);

                float dist = Vector3.Distance(GameObject.FindGameObjectWithTag("Player").transform.position, aiBrain.transform.position);
                if (closest == -1 || closest > dist) closest = dist;
                if (dist <= nearbyAIMaxDist)
                {
                    currAIBrain.RunAIOnce();
                    aiLoopNumbers[currAIBrain.GetInstanceID()] = currLoopNum;
                }
            }
        }
    }
    /// <summary>
    /// Performs up to max distant AI that are within medium distance of player if doFar is false else will perform up to max distant AI for any AI beyond medium range.
    /// </summary>
    /// <param name="doFar">Target AI in the far range instead of medium.</param>
    private void DoDistantAILoop(bool doFar)
    {
        List<AIBrain> objectManagerObjs = ObjectPoolManagerBase.SINGLETON.GetActiveObjectsComponents<AIBrain>();
        List<AIBrain> topChosen = new List<AIBrain>();
        int leastRecentRanAI = -1;
        foreach (AIBrain aiBrain in objectManagerObjs)
        {
            AIBrain currAIBrain = aiBrain.GetComponent<AIBrain>();
            if (currAIBrain != null)
            {
                if (!aiLoopNumbers.ContainsKey(currAIBrain.GetInstanceID())) aiLoopNumbers.Add(currAIBrain.GetInstanceID(), -1);

                float dist = Vector3.Distance(GameObject.FindGameObjectWithTag("Player").transform.position, aiBrain.transform.position);
                bool inRange = dist > mediumAIMaxDist;
                if (!doFar) inRange = dist > nearbyAIMaxDist && dist <= mediumAIMaxDist;

                if (inRange)
                {
                    // List not full yet
                    if (topChosen.Count < maxDistantAIPerFrame)
                    {
                        topChosen.Add(currAIBrain);
                        if (aiLoopNumbers[currAIBrain.GetInstanceID()] == -1 || leastRecentRanAI > aiLoopNumbers[currAIBrain.GetInstanceID()]) leastRecentRanAI = aiLoopNumbers[currAIBrain.GetInstanceID()];
                    }
                    // List full, replace most recent in list with current if current isn't as recent
                    else if (leastRecentRanAI >= aiLoopNumbers[currAIBrain.GetInstanceID()])
                    {
                        int mostRecentI = 0;
                        int mostRecent = -1;
                        for (int i = 0; i < topChosen.Count; i++)
                        {
                            if (mostRecent == -1 || mostRecent < aiLoopNumbers[topChosen[i].GetInstanceID()])
                            {
                                mostRecentI = i;
                                mostRecent = aiLoopNumbers[topChosen[i].GetInstanceID()];
                            }
                        }

                        topChosen.RemoveAt(mostRecentI);
                        topChosen.Add(currAIBrain);
                        leastRecentRanAI = aiLoopNumbers[currAIBrain.GetInstanceID()];
                    }
                }
            }
        }

        foreach (AIBrain chosen in topChosen)
        {
            chosen.RunAIOnce();
            aiLoopNumbers[chosen.GetInstanceID()] = currLoopNum;
        }
    }
    /// <summary>
    /// Main AI loop. Determine if nearby, medium, or far AI should be performed this frame using AI pattern.
    /// </summary>
    private void AILoop()
    {
        if (aiPattern != null && aiPattern.Length > 0)
        {
            if (aiPattern[aiFramePatternI] == AIFramePatternTypes.Nearby) DoNearbyAILoop();
            else if (aiPattern[aiFramePatternI] == AIFramePatternTypes.Medium) DoDistantAILoop(false);
            else if (aiPattern[aiFramePatternI] == AIFramePatternTypes.Far) DoDistantAILoop(true);

            aiFramePatternI++;
            if (aiFramePatternI >= aiPattern.Length) aiFramePatternI = 0;

            currLoopNum++;
        }
    }
    #endregion

    #region Unity Functions
    private void Update()
    {
        if (frameGapI == frameGap) AILoop();
        frameGapI++;
        if (frameGapI > frameGap) frameGapI = 0;
    }
    #endregion
}

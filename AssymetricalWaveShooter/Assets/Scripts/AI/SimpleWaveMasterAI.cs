﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A simple wave master that has very little inteligence and relies mainly on random choices.
/// </summary>
public class SimpleWaveMasterAI : MonoBehaviour, IWaveMaster
{
    #region Variables
    #region Public
    
    #endregion

    #region Properties
    /// <summary>
    /// Are there any enemies left to spawn and alive?
    /// </summary>
    public bool EnemiesRemaining
    {
        get
        {
            foreach(EnemyCount enemyCount in chosenEnemies)
            {
                if (enemyCount.Count > 0) return true;
            }

            return false;
        }
    }
    #endregion

    #region Private
    private List<EnemyCount> chosenEnemies = new List<EnemyCount>();
    private List<SpawnSphere> spawnSpheres = new List<SpawnSphere>();

    private EndRoundWaveMasterInfo endRoundInfo = new EndRoundWaveMasterInfo();
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Called from the round controller when the prepare state started.
    /// </summary>
    /// <param name="totalPoints">The total points available for this round.</param>
    /// <param name="enemyOptions">The enemy options available for this round/</param>
    /// <param name="heightmapOptions">The heightmap options available for this round.</param>
    public void PrepareState(int totalPoints, List<EnemyOption> enemyOptions, List<PMHeightmap> heightmapOptions)
    {
        ChooseEnemies(totalPoints, enemyOptions);
        ChooseMap(heightmapOptions);

        RoundControllerBase.SINGLETON.WaveMasterReady = true;
    }
    /// <summary>
    /// Called from the round controller when the starting state started.
    /// </summary>
    public void StartingState()
    {

    }
    /// <summary>
    /// Called from the roudn controller when the playing state started.
    /// </summary>
    public void PlayingState()
    {
        spawnSpheres = ObjectPoolManagerBase.SINGLETON.GetActiveObjectsComponents<SpawnSphere>();
        QueueEnemies();
    }
    /// <summary>
    /// Called from the round controller when the post-play state started.
    /// </summary>
    /// <returns>The information from the round that just finished.</returns>
    public EndRoundWaveMasterInfo PostPlayState()
    {
        chosenEnemies = new List<EnemyCount>();
        KillAllSpawnedEnemies();

        return endRoundInfo;
    }
    #endregion

    #region Private
    /// <summary>
    /// Randomly chooses the enemies out of the given options and tries to use up as much as all the points it can.
    /// </summary>
    /// <param name="totalPoints">The total points available.</param>
    /// <param name="enemyOptions">The enemy options available.</param>
    private void ChooseEnemies(int totalPoints, List<EnemyOption> enemyOptions)
    {
        if (enemyOptions != null && enemyOptions.Count > 0)
        {
            // Find most expensive and cheapest.
            EnemyOption mostExpensive = enemyOptions[0];
            EnemyOption cheapest = enemyOptions[0];
            for (int i = 1; i < enemyOptions.Count; i++)
            {
                EnemyOption curr = enemyOptions[i];
                if (curr.PointCost > mostExpensive.PointCost) mostExpensive = curr;
                else if (curr.PointCost < cheapest.PointCost) cheapest = curr;
            }

            // Randomly pick enemies.
            List<EnemyOption> currChosenEnemies = new List<EnemyOption>();
            int currPointsTotal = 0;
            int randNum = 0;
            while (totalPoints - currPointsTotal >= mostExpensive.PointCost)
            {
                randNum = Random.Range(0, enemyOptions.Count);
                currChosenEnemies.Add(enemyOptions[randNum]);
                currPointsTotal += enemyOptions[randNum].PointCost;
            }

            // Use up remaining points with cheapest.
            if (totalPoints - currPointsTotal > 0)
            {
                while (totalPoints - currPointsTotal >= cheapest.PointCost)
                {
                    currChosenEnemies.Add(cheapest);
                    currPointsTotal += cheapest.PointCost;
                }
            }
            
            // Save selected enemies without repeats to end round info.
            endRoundInfo.SelectedEnemyOptions = new List<EnemyOption>();
            foreach(EnemyOption enemyOption in currChosenEnemies)
            {
                bool isFound = false;
                foreach(EnemyOption savedOption in endRoundInfo.SelectedEnemyOptions)
                {
                    if (savedOption.Enemy == enemyOption.Enemy)
                    {
                        isFound = true;
                        break;
                    }
                }

                if (!isFound) endRoundInfo.SelectedEnemyOptions.Add(enemyOption);
            }

            // Transfer chosen to list with counts.
            bool found = false;
            foreach (EnemyOption enemy in currChosenEnemies)
            {
                for(int i = 0; i < chosenEnemies.Count; i++)
                {
                    if(chosenEnemies[i].Enemy == enemy.Enemy.gameObject)
                    {
                        chosenEnemies[i].Count++;
                        found = true;
                        break;
                    }
                }

                if (!found) chosenEnemies.Add(new EnemyCount(enemy.Enemy.gameObject, 1));
                found = false;
            }

            List<EnemyCount> endRoundSpecificCounts = new List<EnemyCount>();
            foreach(EnemyCount enemyCount in chosenEnemies) endRoundSpecificCounts.Add(new EnemyCount(enemyCount.Enemy, enemyCount.Count));
            endRoundInfo.SelectedEnemyCounts = new List<EnemyCount>(endRoundSpecificCounts);
        }
    }
    /// <summary>
    /// Chooses a random heightmap out of the ones given.
    /// </summary>
    /// <param name="heightmapOptions">The heightmap options available.</param>
    private void ChooseMap(List<PMHeightmap> heightmapOptions)
    {
        if (heightmapOptions != null && heightmapOptions.Count > 0)
        {
            int randNum = Random.Range(0, heightmapOptions.Count);
            PMHeightmap chosenHeightmap = heightmapOptions[randNum];
            endRoundInfo.SelectedHeightmap = chosenHeightmap;

            RoundControllerBase.SINGLETON.SetNextHeightmap(chosenHeightmap);
        }
    }

    /// <summary>
    /// Chooses a random spawn sphere to add the each selected enemy to the queue to spawn.
    /// </summary>
    private void QueueEnemies()
    {
        if (spawnSpheres != null && spawnSpheres.Count > 0)
        {
            int currI = 0;
            int randNum = 0;
            while(EnemiesRemaining)
            {
                if(chosenEnemies[currI].Count > 0)
                {
                    randNum = Random.Range(0, spawnSpheres.Count);
                    spawnSpheres[randNum].AddToQueue(chosenEnemies[currI].Enemy);
                    chosenEnemies[currI].Count--;
                }

                currI++;
                if (currI >= chosenEnemies.Count) currI = 0;
            }
        }
    }

    /// <summary>
    /// Kill all the enemies that are on the map.
    /// </summary>
    private void KillAllSpawnedEnemies()
    {
        List<AIBrain> spawnedBrains = ObjectPoolManagerBase.SINGLETON.GetActiveObjectsComponents<AIBrain>();
        foreach(AIBrain brain in spawnedBrains)
        {
            HealthTrait healthTrait = (HealthTrait)brain.GetTrait(typeof(HealthTrait));
            if(healthTrait == null)
            {
                Invoke("KillAllSpawnedEnemies", .1f);
                break;
            }
            else healthTrait.AdjustHealth(-1000000);
        }
    }
    #endregion
    #endregion
}
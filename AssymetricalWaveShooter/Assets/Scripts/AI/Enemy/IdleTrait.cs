﻿using UnityEngine;

[RequireComponent(typeof(IAIMovementTrait))]
/// <summary>
/// This trait simply has the AI move towards the player.
/// </summary>
public class IdleTrait : AITrait
{
    #region Variables
    #region Public
    [Tooltip("The frequency that the player's position is updated.")]
    [SerializeField] private float updatePlayerPosFrequency = 1;
    #endregion

    #region Properties

    #endregion

    #region Private
    IAIMovementTrait aiMovement = null;

    private float lastPosUpdate = 0;
    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// Can always be canceled.
    /// Reset cooldown time.
    /// </summary>
    /// <returns>True when canceled.</returns>
    public override bool CancelPerform()
    {
        lastPosUpdate = 0;
        aiMovement.CancelTarget();
        return true;
    }
    /// <summary>
    /// Checks if the target position update cooldown has past.
    /// </summary>
    /// <returns>True if cooldown has passed.</returns>
    public override bool Checking()
    {
        return Time.time - lastPosUpdate >= updatePlayerPosFrequency;
    }
    /// <summary>
    /// If the player exists then update the position to move towards them.
    /// </summary>
    /// <returns>false and will never need additional attention.</returns>
    public override bool Performing()
    {
        if (Time.time - lastPosUpdate >= updatePlayerPosFrequency)
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            if (player != null) aiMovement.AssignTarget(player.transform.position, true);

            lastPosUpdate = Time.time;
        }

        return false;
    }

    /// <summary>
    /// Reset target position update cooldown to not trigger instantly.
    /// </summary>
    public override void PreTraitSetup()
    {
        lastPosUpdate = Time.time - updatePlayerPosFrequency;
    }
    /// <summary>
    /// Get the refrences needed.
    /// </summary>
    public override void TraitSetup()
    {
        aiMovement = (IAIMovementTrait)Brain.GetTrait(typeof(UnityMovementTrait));
    }
    public override void PostTraitSetup()
    {

    }

    public override void TraitReset()
    {
        
    }
    #endregion

    #region Unity Functions

    #endregion
}

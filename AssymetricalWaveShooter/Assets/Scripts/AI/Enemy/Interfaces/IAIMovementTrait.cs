﻿using UnityEngine;

public interface IAIMovementTrait
{
    void AssignTarget(Vector3 targetPos, bool run = false);
    void CancelTarget();

    bool CheckTargetReached();
    float CheckTargetDist();
}

﻿using UnityEngine;

namespace Assets.Scripts.AI.Enemy
{
    /// <summary>
    /// This trait handles moving to a target position.
    /// Can allow or not allow attacking while moving.
    /// </summary>
    [RequireComponent(typeof(IAIMovementTrait))]
    [RequireComponent(typeof(AttackTrait))]
    public class MoveToTrait : AITrait
    {
        #region Variables
        private IAIMovementTrait movementTrait = null;
        private AttackTrait attackTrait = null;
        private Vector3 currTargetPos = Vector3.zero;
        private bool moveToSet = false;
        #endregion

        #region Methods
        /// <summary>
        /// Move to the target position.
        /// </summary>
        /// <param name="targetPos">The target position.</param>
        public void MoveTo(Vector3 targetPos)
        {
            currTargetPos = targetPos;
            moveToSet = false;
        }

        #region AI Trait
        /// <summary>
        /// If a target position is set then needs attention.
        /// </summary>
        /// <returns>True if has a target position.</returns>
        public override bool Checking()
        {
            return currTargetPos != Vector3.zero;
        }
        /// <summary>
        /// Monitor progress to current target position.
        /// </summary>
        /// <returns>True if target position isn't yet reached.</returns>
        public override bool Performing()
        {
            if (!moveToSet)
            {
                movementTrait.AssignTarget(currTargetPos, true);
                moveToSet = true;
            }
            else if (currTargetPos == Vector3.zero || movementTrait.CheckTargetReached())
            {
                CancelPerform();
                return false;
            }

            return true;
        }

        public override void PreTraitSetup()
        {
            
        }
        public override void TraitSetup()
        {
            movementTrait = (IAIMovementTrait)Brain.GetTrait(typeof(UnityMovementTrait));
            attackTrait = (AttackTrait)Brain.GetTrait(typeof(AttackTrait));
        }
        public override void PostTraitSetup()
        {
            
        }

        public override bool CancelPerform()
        {
            currTargetPos = Vector3.zero;
            moveToSet = false;
            attackTrait.AttackDisabled = false;

            return true;
        }
        public override void TraitReset()
        {
            CancelPerform();
        }        
        #endregion
        #endregion
    }
}

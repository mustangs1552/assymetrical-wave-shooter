﻿using UnityEngine;
using Assets.Scripts.Controllers;
using MattRGeorge.Utilities;

/// <summary>
/// The health for the AI.
/// </summary>
public class HealthTrait : AITrait, IHasHealth, ITakesDamage
{
    #region Variables
    #region Public
    [Tooltip("The maximum health.")]
    [SerializeField] private float maxHealth = 100;
    [Tooltip("The starting damage multiplier.")]
    [SerializeField] private float damageMultiplier = 1;
    [Tooltip("The frequency of back-to-back health updates when dealing/healing damage.")]
    [SerializeField] private float healthUpdateCooldown = .1f;
    #endregion

    #region Properties
    /// <summary>
    /// The current amount of health.
    /// </summary>
    public float CurrHealth
    {
        get
        {
            return currHealth;
        }
    }
    /// <summary>
    /// The current percentage of remaining health.
    /// </summary>
    public float CurrHealthPerc
    {
        get
        {
            return currHealth / currMaxHealth;
        }
    }

    /// <summary>
    /// Is this object still alive?
    /// </summary>
    public bool IsAlive
    {
        get
        {
            return currHealth > 0;
        }
    }
    #endregion

    #region Private
    private float currHealth = 0;
    private float currMaxHealth = 0;
    private float currDamageMultiplier = 0;
    private float lastHealthUpdate = 0;
    #endregion
    #endregion

    #region Functions
    public override bool Checking()
    {
        return false;
    }
    public override bool Performing()
    {
        return false;
    }
    public override bool CancelPerform()
    {
        return true;
    }

    /// <summary>
    /// Set all the values.
    /// </summary>
    public override void PreTraitSetup()
    {
        currHealth = maxHealth;
        currMaxHealth = maxHealth;
        currDamageMultiplier = damageMultiplier;
    }
    public override void TraitSetup()
    {
        
    }
    public override void PostTraitSetup()
    {
        
    }

    /// <summary>
    /// Reset all the values.
    /// </summary>
    public override void TraitReset()
    {
        ResetStats();
    }

    /// <summary>
    /// Deal the amount of damage given to health.
    /// </summary>
    /// <param name="amount">The amount of damage to deal.</param>
    /// <returns>True if successful.</returns>
    public bool TakeDamage(float amount, GameObject source)
    {
        if (amount > 0 && Time.time - lastHealthUpdate >= healthUpdateCooldown && currHealth > 0)
        {
            bool successful = AdjustHealth(-amount * currDamageMultiplier);
            if (successful)
            {
                lastHealthUpdate = Time.time;
                if (source != null && !IsAlive) UnityAnalyticsController.ItemKilledEnemy(GameObjectUtilities.GetActualName(source), GameObjectUtilities.GetActualName(gameObject));

                return true;
            }
        }

        return false;
    }
    /// <summary>
    /// Heal the amount of health given.
    /// </summary>
    /// <param name="amount">The amount to heal by.</param>
    /// <returns>True if successful.</returns>
    public bool Heal(float amount, GameObject source)
    {
        if (amount > 0 && currHealth < currMaxHealth && Time.time - lastHealthUpdate >= healthUpdateCooldown && currHealth < currMaxHealth)
        {
            bool successful = AdjustHealth(amount);
            if (successful)
            {
                lastHealthUpdate = Time.time;
                return true;
            }
        }

        return false;
    }
    public bool AdjustHealth(float amount)
    {
        if (amount != 0)
        {
            currHealth += amount;
            CheckValueBoundries();
            return true;
        }

        return false;
    }

    /// <summary>
    /// Reset the current health to full.
    /// </summary>
    public void ResetCurrHealth()
    {
        currHealth = currMaxHealth;
    }
    /// <summary>
    /// Reset the max health amount.
    /// </summary>
    public void ResetCurrMaxHealth()
    {
        currMaxHealth = maxHealth;
    }
    /// <summary>
    /// Reset the current damage multiplier.
    /// </summary>
    public void ResetCurrDamageMultiplier()
    {
        currDamageMultiplier = damageMultiplier;
    }
    /// <summary>
    /// Reset all stats.
    /// </summary>
    public void ResetStats()
    {
        ResetCurrHealth();
        ResetCurrMaxHealth();
        ResetCurrDamageMultiplier();
    }
    /// <summary>
    /// Reset the health to full and optionally the rest of the stats.
    /// </summary>
    /// <param name="resetAllStats">Reset all stats?</param>
    public void Revive(bool resetAllStats)
    {
        if (resetAllStats) ResetStats();
        else ResetCurrHealth();
    }
    /// <summary>
    /// Set the current health to 0.
    /// </summary>
    public void Kill()
    {
        currHealth = 0;
        CheckValueBoundries();
    }

    /// <summary>
    /// Health reached 0.
    /// </summary>
    private void Die()
    {
        ObjectPoolManagerBase.SINGLETON.Destroy(gameObject);
    }
    /// <summary>
    /// Check the bounds of stats values.
    /// </summary>
    private void CheckValueBoundries()
    {
        if (currHealth <= 0)
        {
            currHealth = 0;
            Die();
        }
        else if (currHealth > currMaxHealth) currHealth = currMaxHealth;
    }
    #endregion

    #region Unity Functions

    #endregion
}

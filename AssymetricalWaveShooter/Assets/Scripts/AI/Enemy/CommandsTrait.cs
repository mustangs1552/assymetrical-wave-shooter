﻿using UnityEngine;

namespace Assets.Scripts.AI.Enemy
{
    /// <summary>
    /// This trait listens for commands and performs them by informing the corresponding AI traits.
    /// </summary>
    [RequireComponent(typeof(MoveToTrait))]
    [RequireComponent(typeof(AttackTrait))]
    public class CommandsTrait : PassiveTrait
    {
        #region Variables
        private MoveToTrait moveTrait = null;
        private AttackTrait attackTrait = null;
        #endregion

        #region Methods
        /// <summary>
        /// Move to a target position while allowing or not allowing attacking while moving to target position.
        /// </summary>
        /// <param name="targetPos">The target position.</param>
        /// <param name="allowAttack">Can attack while moving?</param>
        public void MoveTo(Vector3 targetPos, bool allowAttack)
        {
            TraitReset();
            moveTrait.MoveTo(targetPos);
            attackTrait.AttackDisabled = !allowAttack;
        }

        /// <summary>
        /// Attack a target and ignore all other targets until otherwise stated or this target is removed.
        /// </summary>
        /// <param name="target">Target to be assigned.</param>
        public void AttackTarget(Transform target)
        {
            TraitReset();
            attackTrait.AssignTarget(target);
        }

        #region Passive Trait Methods
        public override void UpdateTrait()
        {
            
        }

        public override void PreTraitSetup()
        {
            
        }
        public override void TraitSetup()
        {
            moveTrait = (MoveToTrait)Brain.GetTrait(typeof(MoveToTrait));
            attackTrait = (AttackTrait)Brain.GetTrait(typeof(AttackTrait));
        }
        public override void PostTraitSetup()
        {
            
        }

        /// <summary>
        /// Cancel any active command.
        /// </summary>
        public override void TraitReset()
        {
            moveTrait.CancelPerform();
            attackTrait.CancelPerform();
        }
        #endregion
        #endregion
    }
}

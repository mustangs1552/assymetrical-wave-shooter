﻿using System;
using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Utilities;

[RequireComponent(typeof(SensesTrait))]
[RequireComponent(typeof(IAIMovementTrait))]
/// <summary>
/// Once a player is within range then this trait will take over and move towards the target and start shooting once within range of the equiped weapon.
/// </summary>
public class AttackTrait : AITrait, IShooterControls, IWeaponManager
{
    #region Variables
    #region Public
    [Tooltip("Enable debugging?")]
    [SerializeField] private bool isDebug = false;

    [Tooltip("The minnimum distance from the player to move to.")]
    [SerializeField] private float minDistToPlayer = 5;
    [Tooltip("The starting weapon this AI will have.")]
    [SerializeField] private GameObject startingWeapon = null;
    #endregion

    #region Properties
    public Action ActiveWeaponFireStart
    {
        get
        {
            return StartFiringWeapon;
        }
    }
    public Action ActiveWeaponFiring
    {
        get
        {
            return FiringWeapon;
        }
    }
    public Action ActiveWeaponFireEnd
    {
        get
        {
            return StopFiringWeapon;
        }
    }
    public Action ActiveWeaponReload
    {
        get
        {
            return ReloadWeapon;
        }
    }

    public bool AttackDisabled { get; set; }
    #endregion

    #region Private
    private SensesTrait sensesTrait = null;
    private IAIMovementTrait movementTrait = null;

    private float currMinDistToPlayer = 0;
    private IWeapon equipedWeapon = null;
    private Transform currTarget = null;
    private IWeapon[] weapons = null;
    private bool isFiring = false;
    private bool reachedMinDist = false;

    private bool currTargetDemanded = false;

    private Action StartFiringWeapon = null;
    private Action FiringWeapon = null;
    private Action StopFiringWeapon = null;
    private Action ReloadWeapon = null;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Forcebly assign a new target and ignore all others until this target is removed.
    /// </summary>
    /// <param name="target">Target to be assigned.</param>
    public void AssignTarget(Transform target)
    {
        currTarget = target;
        currTargetDemanded = true;
    }

    /// <summary>
    /// Always able to cancel.
    /// </summary>
    /// <returns>True when canceled.</returns>
    public override bool CancelPerform()
    {
        currTarget = null;
        currTargetDemanded = false;

        return true;
    }
    /// <summary>
    /// Check if we can see a player.
    /// Also, make sure our weapon is topped off here.
    /// </summary>
    /// <returns>True if a player is spotted.</returns>
    public override bool Checking()
    {
        if (equipedWeapon == null) return false;
        
        if(!currTargetDemanded) DetectPlayer();
        if (currTarget == null && equipedWeapon.IsNotFull) equipedWeapon.Reload();

        return !AttackDisabled && currTarget != null;
    }
    /// <summary>
    /// Move towards target if not within range.
    /// Start firing when within range.
    /// Make sure weapon is reloaded while attacking.
    /// </summary>
    /// <returns>True if if still has a target.</returns>
    public override bool Performing()
    {
        if (AttackDisabled || currTarget == null)
        {
            currTargetDemanded = false;
            return false;
        }

        if (equipedWeapon.IsEmpty) equipedWeapon.Reload();

        float distToPlayer = Vector3.Distance(transform.position, currTarget.position);
        if (distToPlayer > equipedWeapon.Range)
        {
            UpdateTargetPos(true);
            reachedMinDist = false;

            if (isFiring)
            {
                isFiring = false;
                StopFiringAtTarget();
            }
        }
        else
        {
            if (distToPlayer <= currMinDistToPlayer)
            {
                movementTrait.CancelTarget();
                reachedMinDist = true;
            }
            else if(reachedMinDist) UpdateTargetPos(false);
            transform.LookAt(currTarget.position);

            RaycastHit hit;
            Physics.Raycast(new Ray(transform.position, transform.forward), out hit, equipedWeapon.Range);
            if (hit.transform != null && hit.transform.tag == "Player")
            {
                if (equipedWeapon.WeaponFiringMode == FiringMode.Auto)
                {
                    if (!isFiring)
                    {
                        isFiring = true;
                        StartFiringAtTarget();
                    }
                    else FiringAtTarget();
                }
                else
                {
                    StartFiringAtTarget();
                    StopFiringAtTarget();
                }
            }
        }

        return true;
    }

    /// <summary>
    /// Find and prepare available weapons.
    /// </summary>
    public override void PreTraitSetup()
    {
        weapons = GetComponentsInChildren<IWeapon>();
        MonoBehaviour weaponMB = null;
        foreach(IWeapon w in weapons)
        {
            weaponMB = (MonoBehaviour)w;
            weaponMB.gameObject.SetActive(false);
            weaponMB.GetComponent<Rigidbody>().isKinematic = true;
            weaponMB.GetComponent<Collider>().enabled = false;
        }

        if (startingWeapon != null)
        {
            IWeapon weapon = startingWeapon.GetComponent<IWeapon>();
            if (weapon != null) PickUpWeapon(weapon);
        }
    }
    /// <summary>
    /// Get needed refrences.
    /// </summary>
    public override void TraitSetup()
    {
        sensesTrait = (SensesTrait)Brain.GetTrait(typeof(SensesTrait));
        movementTrait = (IAIMovementTrait)Brain.GetTrait(typeof(UnityMovementTrait));
    }
    public override void PostTraitSetup()
    {

    }

    /// <summary>
    /// Unequip equiped weapon.
    /// </summary>
    public override void TraitReset()
    {
        CancelPerform();

        equipedWeapon = null;
        StartFiringWeapon = null;
        FiringWeapon = null;
        StopFiringWeapon = null;
        ReloadWeapon = null;
        MonoBehaviour weaponMB = (MonoBehaviour)equipedWeapon;
        if(weaponMB != null) weaponMB.gameObject.SetActive(false);

        if (startingWeapon != null)
        {
            IWeapon weapon = startingWeapon.GetComponent<IWeapon>();
            if (weapon != null) PickUpWeapon(weapon);
        }
    }

    /// <summary>
    /// Replaces the current weapon with the given weapon if it is one of the ones that this AI has available.
    /// </summary>
    /// <param name="weapon">New weapon.</param>
    /// <returns>True if successfully equiped.</returns>
    public bool PickUpWeapon(IWeapon weapon)
    {
        if (weapon == null) return false;

        bool success = false;
        MonoBehaviour weaponMB = null;
        foreach (IWeapon w in weapons)
        {
            if (w.WeaponName == weapon.WeaponName)
            {
                equipedWeapon = w;
                StartFiringWeapon = equipedWeapon.FireStart;
                FiringWeapon = equipedWeapon.Firing;
                StopFiringWeapon = equipedWeapon.FireEnd;
                ReloadWeapon = equipedWeapon.Reload;
                currMinDistToPlayer = equipedWeapon.Range < minDistToPlayer ? equipedWeapon.Range : minDistToPlayer;
                equipedWeapon.PickedUp(gameObject);

                weaponMB = (MonoBehaviour)equipedWeapon;
                weaponMB.gameObject.SetActive(true);

                success = true;
            }
            else
            {
                weaponMB = (MonoBehaviour)w;
                weaponMB.gameObject.SetActive(false);
            }
        }

        return success;
    }

    #region Unused
    public void PauseMovement()
    {

    }
    public void ResumeMovement()
    {

    }

    public void DropWeapon()
    {

    }
    public void DropWeapon(WeaponTypes slot)
    {

    }
    public void ToggleLastWeapon()
    {
        
    }
    public void ChangeWeaponUp()
    {
        
    }
    public void ChangeWeaponDown()
    {
        
    }
    public void ChangeWeapon(WeaponTypes slot)
    {
        
    }
    public void ChangeWeapon(int slotNum)
    {
        
    }
    #endregion
    #endregion

    #region Private
    /// <summary>
    /// Find the closest object to us from the list.
    /// </summary>
    /// <param name="objs">The list of objects.</param>
    /// <returns>The closest object to us.</returns>
    private Transform FindClosest(List<Transform> objs)
    {
        if (objs != null && objs.Count > 0)
        {
            float closestDist = Vector3.Distance(transform.position, objs[0].position);
            Transform closest = objs[0];
            float currClosestDist = 0;
            for (int i = 1; i < objs.Count; i++)
            {
                currClosestDist = Vector3.Distance(transform.position, objs[0].position);
                if (currClosestDist < closestDist)
                {
                    closestDist = currClosestDist;
                    closest = objs[i];
                }
            }

            return closest;
        }

        return null;
    }
    /// <summary>
    /// Use our senses trait to find to get what objects we sense and check for a player to target.
    /// </summary>
    private void DetectPlayer()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        List<Transform> playersTrans = new List<Transform>();
        foreach (GameObject player in players) playersTrans.Add(player.transform);
        List<Transform> sensedPlayers = sensesTrait.CheckSenses(playersTrans);

        if (sensedPlayers.Count > 0) currTarget = FindClosest(sensedPlayers);
        else if(currTarget != null) currTarget = null;
    }

    /// <summary>
    /// Update the position of the current target.
    /// </summary>
    /// <param name="run">Move at running speed?</param>
    private void UpdateTargetPos(bool run)
    {
        if (currTarget != null) movementTrait.AssignTarget(currTarget.position, run);
    }

    /// <summary>
    /// Aim and start firing at the current target using the equiped weapon.
    /// </summary>
    private void StartFiringAtTarget()
    {
        equipedWeapon.FireStart();
    }
    /// <summary>
    /// Aim and continue firing at the current target using the equiped weapon.
    /// </summary>
    private void FiringAtTarget()
    {
        equipedWeapon.Firing();
    }
    /// <summary>
    /// Stop firing the equiped weapon.
    /// </summary>
    private void StopFiringAtTarget()
    {
        equipedWeapon.FireEnd();
    }
    #endregion
    #endregion

    #region Unity Functions
    private void OnDrawGizmos()
    {
        if (isDebug && currTarget != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, currTarget.position);
        }
    }
    #endregion
}

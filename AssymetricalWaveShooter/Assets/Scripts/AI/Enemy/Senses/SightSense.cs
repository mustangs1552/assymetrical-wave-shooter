﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(SensesTrait))]
/// <summary>
/// This sense is the AI's ability to see other objects.
/// It uses a FOV and range to check what is sensed.
/// </summary>
public class SightSense : MonoBehaviour, ISense
{
    #region Variables
    #region Public
    [Tooltip("Enable debugging?")]
    [SerializeField] private bool isDebug = false;

    [Tooltip("The distance this sense can detect an object.")]
    [SerializeField] private float range = 10;
    [Tooltip("The horizontal FOV of this creature.")]
    [SerializeField] private float horizontalFOV = 180;
    [Tooltip("The vertical FOV of this creature.")]
    [SerializeField] private float verticalFOV = 90;
    #endregion

    #region Properties

    #endregion

    #region Private
    private float verticalFOVCenter = 0;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Check to see what this sense currently sees.
    /// </summary>
    /// <returns>The transforms that this sense can see.</returns>
    public List<Transform> CheckSense()
    {
        return CheckSight(FilterObjects());
    }
    /// <summary>
    /// Check to see if this sense can see any of the given objects.
    /// </summary>
    /// <param name="objs">The objects to check.</param>
    /// <returns>The transforms that this sense can see.</returns>
    public List<Transform> CheckSense(List<Transform> objs)
    {
        return CheckSight(objs);
    }

    /// <summary>
    /// Setup this sense.
    /// </summary>
    public void SenseSetup()
    {
        Setup();
    }

    public void SenseReset()
    {

    }
    #endregion

    #region private
    /// <summary>
    /// Initial setup.
    /// </summary>
    private void Setup()
    {
        verticalFOVCenter = Vector3.Angle(Vector3.forward, Vector3.up);
    }

    /// <summary>
    /// Get and filter through all the active objects for valid sensable objects.
    /// </summary>
    /// <returns>Valid sensable objects.</returns>
    private List<Transform> FilterObjects()
    {
        List<Transform> objManagerObjs = new List<Transform>();

        if (ObjectPoolManagerBase.SINGLETON != null)
        {
            objManagerObjs = ObjectPoolManagerBase.SINGLETON.AllTransforms;
            objManagerObjs.Remove(gameObject.transform);
        }

        return objManagerObjs;
    }

    /// <summary>
    /// Check the list of valid objects to see which are actually sensed.
    /// </summary>
    /// <param name="validObjs">The list of valid objects.</param>
    /// <returns>The list of sensed objects.</returns>
    private List<Transform> CheckSight(List<Transform> validObjs)
    {
        List<Transform> detectedObjs = new List<Transform>();

        foreach (Transform obj in validObjs)
        {
            // Within range and FOV
            float objDist = Vector3.Distance(transform.position, obj.position);
            if (objDist <= range && CheckFOV(obj))
            {
                // LOS
                RaycastHit hit = new RaycastHit();
                if(Physics.Raycast(new Ray(transform.position, obj.position - transform.position), out hit, objDist))
                {
                    if (hit.transform == obj) detectedObjs.Add(obj);
                }
            }
        }

        return detectedObjs;
    }
    /// <summary>
    /// Check to see if the given object is within the FOV range.
    /// </summary>
    /// <param name="obj">The object to check.</param>
    /// <returns>True if within FOV range.</returns>
    private bool CheckFOV(Transform obj)
    {
        Vector3 targetDir = obj.position - transform.position;
        float horizontalAngle = Vector3.Angle(targetDir, transform.forward);
        float verticalAngle = Vector3.Angle(targetDir, transform.up);

        if (horizontalAngle <= horizontalFOV / 2)
        {
            if (verticalAngle <= verticalFOVCenter + verticalFOV / 2 && verticalAngle >= verticalFOVCenter - verticalFOV / 2) return true;
        }

        return false;
    }

    /// <summary>
    /// Runs a test that gets all sensed objects and shows the results via debug lines connecting those objects.
    /// Green: Did sense
    ///   Red: Did not sense
    /// </summary>
    private void SenseTest()
    {
        List<Transform> startingObjs = FilterObjects();
        List<Transform> endingObjs = CheckSight(startingObjs);

        foreach (Transform obj in startingObjs)
        {
            if(obj.gameObject.layer != (int)LayerMask.NameToLayer("Ignore Raycast")) Debug.DrawLine(transform.position, obj.position, (endingObjs.Contains(obj)) ? Color.green : Color.red);
        }
    }
    #endregion
    #endregion

    #region Unity Functions
    private void OnDrawGizmos()
    {
        if (isDebug)
        {
            SenseTest();

            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, range);
        }
    }
    #endregion
}

﻿using UnityEngine;
using System.Collections.Generic;

public interface ISense
{
    List<Transform> CheckSense();
    List<Transform> CheckSense(List<Transform> objs);

    void SenseSetup();

    void SenseReset();
}

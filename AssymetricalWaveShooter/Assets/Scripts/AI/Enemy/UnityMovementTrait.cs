﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
/// <summary>
/// Uses the Unity navigation system to move.
/// </summary>
public class UnityMovementTrait : PassiveTrait, IAIMovementTrait
{
    #region Variables
    #region Public
    [Tooltip("Enable debugging?")]
    [SerializeField] private bool isDebug = false;

    [Tooltip("How far from the target destination for the agent consider reached?")]
    [SerializeField] private float stopDist = 1;
    [Tooltip("The speed this creature runs above default walking speed.")]
    [SerializeField] private float runningSpeedIncrease = 2;
    [Tooltip("The speed of the extra rotation that is applied due to Unity's NavMeshAgent's rotation being slow and strange to control.")]
    [SerializeField] private float extraRotationSpeed = 1;
    #endregion

    #region Properties

    #endregion

    #region Private
    private NavMeshAgent navAgent = null;

    private Vector3 currTarget = Vector3.zero;
    private float walkingSpeed = 0;
    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// Get the refrences needed and the walking speed from the nav mesh agent.
    /// </summary>
    public override void PreTraitSetup()
    {
        navAgent = GetComponent<NavMeshAgent>();
        walkingSpeed = navAgent.speed;
    }
    public override void TraitSetup()
    {
        
    }
    public override void PostTraitSetup()
    {

    }

    /// <summary>
    /// Update the target position if not reached.
    /// Otherwise cancel the target position.
    /// </summary>
    public override void UpdateTrait()
    {
        if (CheckTargetReached()) CancelTarget();
        else UpdateRotation();
    }

    /// <summary>
    /// Cancel the current target position.
    /// </summary>
    public override void TraitReset()
    {
        CancelTarget();
    }

    /// <summary>
    /// Set the destination for the NavMeshAgent.
    /// </summary>
    /// <param name="targetPos">The destination.</param>
    public void AssignTarget(Vector3 targetPos, bool run = false)
    {
        if (navAgent != null && navAgent.enabled)
        {
            navAgent.SetDestination(targetPos);
            currTarget = targetPos;

            if (!run) navAgent.speed = walkingSpeed;
            else navAgent.speed = walkingSpeed + runningSpeedIncrease;
        }
    }

    /// <summary>
    /// Cancel the path on the NavMeshAgent.
    /// </summary>
    public void CancelTarget()
    {
        if (navAgent != null)
        {
            navAgent.ResetPath();
            currTarget = Vector3.zero;
        }
    }

    /// <summary>
    /// Check if destination was reached via distance.
    /// </summary>
    /// <returns>True if reached.</returns>
    public bool CheckTargetReached()
    {
        if (CheckTargetDist() <= stopDist) return true;
        else return false;
    }
    /// <summary>
    /// Check the distance from the destination.
    /// </summary>
    /// <returns>The distance.</returns>
    public float CheckTargetDist()
    {
        if (navAgent != null)
        {
            float dist = Vector3.Distance(transform.position, navAgent.destination) - transform.localScale.z / 2;
            return (dist >= 0) ? dist : 0;
        }

        return -1;
    }

    /// <summary>
    /// Add an extra rotation because of Unity's nav mesh agent having a strange way to handle rotations and were very slow.
    /// </summary>
    private void UpdateRotation()
    {
        Vector3 lookDir = navAgent.steeringTarget - transform.position;
        if (lookDir != Vector3.zero)
        {
            Quaternion lookRot = Quaternion.LookRotation(lookDir);
            Quaternion lookRotY = Quaternion.Euler(transform.rotation.eulerAngles.x, lookRot.eulerAngles.y, transform.rotation.eulerAngles.z);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotY, extraRotationSpeed * Time.deltaTime);
        }
    }
    #endregion

    #region Unity Functions
    private void OnDrawGizmos()
    {
        if (isDebug && currTarget != Vector3.zero)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, currTarget);
        }
    }
    #endregion
}

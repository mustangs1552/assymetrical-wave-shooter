﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Assets.Scripts.Controllers;
using MattRGeorge.Utilities;

/// <summary>
/// An object that has a queue of objects that need to be spawned.
/// </summary>
public class SpawnSphere : MonoBehaviour, IQueueSpawner, ISpawnSphere
{
    #region Variables
    #region Public
    [Tooltip("Enable spawning?")]
    [SerializeField] private bool spawingEnabled = false;
    [Tooltip("Distance to spawn objects on the y-axis of this spawn sphere.")]
    [SerializeField] private float spawnDist = 1;
    [Tooltip("The rate at which this spawn sphere spawns the objects in its queue.")]
    [SerializeField] private float spawnRate = 3;

    [Tooltip("The starting position on the y-axis when spawned.")]
    [SerializeField] private float startY = 10;
    [Tooltip("The ending position on the y-axis when the moving to its active position.")]
    [SerializeField] private float activeY = 3;
    [Tooltip("The speed that the sphere moves.")]
    [SerializeField] private float speed = 1;
    [Tooltip("The frequency that the height is updated as the ground moves.")]
    [SerializeField] private float updateHeightCooldown = .1f;
    #endregion

    #region Properties
    /// <summary>
    /// Spawning enabled?
    /// </summary>
    public bool SpawningEnabled
    {
        get
        {
            return spawingEnabled;
        }
        set
        {
            spawingEnabled = value;
        }
    }

    /// <summary>
    /// The current queue of objects.
    /// </summary>
    public List<GameObject> SpawnQueue
    {
        get
        {
            return spawnQueue;
        }
    }

    /// <summary>
    /// Is this spawn sphere still moving while spawning or de-spawning.
    /// </summary>
    public bool IsMoving
    {
        get
        {
            return isSpawning || isDespawning;
        }
    }
    #endregion

    #region Private
    private List<GameObject> spawnQueue = new List<GameObject>();
    private float lastSpawn = 0;
    private NavMeshAgent lastSpawnedAgent = null;
    private bool isSpawning = false;
    private bool isDespawning = false;
    private bool despawned = false;
    private Vector3 startingPos = Vector3.zero;
    private Vector3 activePos = Vector3.zero;
    private Pillar pillarPOI = null;
    private float lastHeightUpdate = 0;
    #endregion
    #endregion

    #region Functions
    /// <summary>
    /// Add the given object to the queue of objects to be spawned.
    /// </summary>
    /// <param name="spawnObj">The object to be spawned.</param>
    public void AddToQueue(GameObject spawnObj)
    {
        spawnQueue.Add(spawnObj);
    }

    /// <summary>
    /// Remove the last object from this object's queue of objects to be spawned.
    /// </summary>
    public void RemoveFromBackOfQueue()
    {
        RemoveFromQueue(spawnQueue.Count - 1);
    }
    /// <summary>
    /// Remove the first object from this object's queue of objects to be spawned.
    /// </summary>
    public void RemoveFromFrontOfQueue()
    {
        RemoveFromQueue(0);
    }
    /// <summary>
    /// Remove the object at the given index from this object's queue of objects to be spawned.
    /// </summary>
    /// <param name="index">The index of the object to be removed.</param>
    public void RemoveFromQueue(int index)
    {
        if (spawnQueue != null && spawnQueue.Count > 0) spawnQueue.RemoveAt(index);
    }

    /// <summary>
    /// Spawn the spawn sphere.
    /// </summary>
    /// <param name="pillar">The pillar that this spawn sphere is spawning on.</param>
    public void SpawnSpawnSphere(Pillar pillar)
    {
        startingPos = pillar.TopSurface + Vector3.up * startY;
        activePos = pillar.TopSurface + Vector3.up * activeY;
        pillarPOI = pillar;
        transform.position = startingPos;

        despawned = false;
        isSpawning = true;
    }
    /// <summary>
    /// Despawn the spawn sphere.
    /// </summary>
    public void DespawnSpawnSphere()
    {
        isDespawning = true;
        spawingEnabled = false;
        spawnQueue = new List<GameObject>();
    }

    /// <summary>
    /// Enables the last spawned nav mesh agent.
    /// Having this disabled on spawn fixes a bug that makes the agent jump to a completly different spot after spawned.
    /// </summary>
    private void EnableNavMeshAgent()
    {
        if (lastSpawnedAgent != null) lastSpawnedAgent.enabled = true;
    }
    /// <summary>
    /// Spawn the given object.
    /// </summary>
    /// <param name="spawnObj">The object to be spawned.</param>
    private void Spawn(GameObject spawnObj)
    {
        GameObject spawnedObj = ObjectPoolManagerBase.SINGLETON.Instantiate(spawnObj, transform.position + Vector3.down * spawnDist, Quaternion.identity);
        UnityAnalyticsController.EnemySpawned(GameObjectUtilities.GetActualName(spawnedObj));

        lastSpawnedAgent = spawnedObj.GetComponent<NavMeshAgent>();
        if (lastSpawnedAgent != null)
        {
            lastSpawnedAgent.enabled = false;
            Invoke("EnableNavMeshAgent", .5f);
        }
    }
    /// <summary>
    /// Go through queue of objects to be spawned.
    /// </summary>
    private void SpawnLoop()
    {
        if (spawingEnabled && Time.time - lastSpawn >= spawnRate && spawnQueue.Count > 0)
        {
            Spawn(spawnQueue[0]);
            spawnQueue.RemoveAt(0);
            lastSpawn = Time.time;
        }
    }

    /// <summary>
    /// Move the sphere down from the ceiling.
    /// </summary>
    private void SpawningSphere()
    {
        transform.Translate(Vector3.down * speed * Time.deltaTime);
        if (transform.position.y <= activePos.y)
        {
            isSpawning = false;
            transform.position = activePos;
        }
    }
    /// <summary>
    /// Move the sphere up into the ceiling.
    /// </summary>
    private void DespawningSphere()
    {
        transform.Translate(Vector3.up * speed * Time.deltaTime);
        if (transform.position.y >= startingPos.y)
        {
            isDespawning = false;
            despawned = true;
            transform.position = startingPos;
        }
    }
    /// <summary>
    /// Move the sphere when spawning in/out.
    /// </summary>
    private void MoveSphere()
    {
        if (isSpawning) SpawningSphere();
        else if (isDespawning) DespawningSphere();
        else if (!despawned && Time.time - lastHeightUpdate > updateHeightCooldown)
        {
            UpdateHeight();
            lastHeightUpdate = Time.time;
        }
    }

    /// <summary>
    /// Updates the height of the spawn sphere if the pillar that this spawn sphere has spawned on moved.
    /// </summary>
    private void UpdateHeight()
    {
        if(pillarPOI != null && pillarPOI.TopSurface + Vector3.up * activeY != activePos)
        {
            activePos = pillarPOI.TopSurface + Vector3.up * activeY;
            transform.position = activePos;
        }
    }

    /// <summary>
    /// Initial setup.
    /// </summary>
    private void Setup()
    {
        if (spawnRate < .6f) spawnRate = .6f;
        lastSpawn = -spawnRate;
    }
    #endregion

    #region Unity Functions
    private void Awake()
    {
        Setup();
    }
    private void Update()
    {
        MoveSphere();
        SpawnLoop();
    }

    private void OnEnable()
    {
        spawnQueue = new List<GameObject>();
    }
    #endregion
}
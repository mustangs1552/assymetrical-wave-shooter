﻿interface IRoundController
{
    RoundState State { get; }
    bool WaveMasterReady { get; set; }
    bool IsPlayerCounterReached { get; }

    bool StartRound(int playerCount, int startRoundNum);

    void ShooterPlayerReady();
    void ShooterPlayerUnReady();
    void ShooterPlayerInPosition();
    void ShooterPlayerDefeated();

    bool SetNextHeightmap(PMHeightmap heightmap);
}
